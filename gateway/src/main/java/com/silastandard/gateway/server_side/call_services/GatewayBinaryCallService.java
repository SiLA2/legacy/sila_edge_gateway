package com.silastandard.gateway.server_side.call_services;

import com.silastandard.gateway.server_side.server_management.Connection;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;

import java.util.List;

/**
 * Executes binary calls to SiLA Server.
 */
public class GatewayBinaryCallService {
    private final Connection connection;

    public GatewayBinaryCallService(@NonNull final Connection connection) {
        this.connection = connection;
    }

    /**
     * Binary Upload Call: CreateBinaryRequest
     *
     * A message to allocate storage space on the size of the binary data and on how many Binary Chunks it will be divided in.
     **/
    public SiLABinaryTransfer.CreateBinaryResponse createBinaryRequest(@NonNull SiLABinaryTransfer.CreateBinaryRequest createBinaryRequest, List<SiLACloudConnector.Metadata> metadataList) {
        BinaryUploadGrpc.BinaryUploadBlockingStub uploadBlockingStub = BinaryUploadGrpc.newBlockingStub(connection.getChannelWithMetadata(metadataList));
        return uploadBlockingStub.createBinary(
                SiLABinaryTransfer.CreateBinaryRequest
                        .newBuilder()
                        .setBinarySize(createBinaryRequest.getBinarySize())
                        .setChunkCount(createBinaryRequest.getChunkCount())
                        .build()
        );
    }

    /**
     * Binary Upload Call: Upload Chunk
     *
     * A message to upload the Binary Chunk.
     **/
    public StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunk(@NonNull StreamObserver<SiLABinaryTransfer.UploadChunkResponse> streamObserver) {
        BinaryUploadGrpc.BinaryUploadStub uploadStreamStub = BinaryUploadGrpc.newStub(this.connection.getManagedChannel());
        return uploadStreamStub.uploadChunk(streamObserver);
    }

    /**
     * Binary Upload Call: DeleteUploadedBinaryRequest
     *
     * A message to delete the Binary after use.
     **/
    public SiLABinaryTransfer.DeleteBinaryResponse deleteUploadedBinaryUpload(@NonNull SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        BinaryUploadGrpc.BinaryUploadBlockingStub uploadBlockingStub = BinaryUploadGrpc.newBlockingStub(this.connection.getManagedChannel());
        return uploadBlockingStub.deleteBinary(deleteBinaryRequest);
    }

    /**
     * Binary Download Call: DeleteDownloadedBinaryRequest
     *
     * A message to delete the Binary after use.
     **/
    public SiLABinaryTransfer.DeleteBinaryResponse deleteDownloadedBinaryUpload(@NonNull SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        BinaryDownloadGrpc.BinaryDownloadBlockingStub downloadBlockingStub = BinaryDownloadGrpc.newBlockingStub(this.connection.getManagedChannel());
        return downloadBlockingStub.deleteBinary(deleteBinaryRequest);
    }

    /**
     * Binary Download Call: GetBinaryInfoRequest
     *
     * A message to inspect the Binary.
     **/
    public SiLABinaryTransfer.GetBinaryInfoResponse getBinaryInfo(@NonNull SiLABinaryTransfer.GetBinaryInfoRequest binaryInfoRequest) {
        BinaryDownloadGrpc.BinaryDownloadBlockingStub downloadBlockingStub = BinaryDownloadGrpc.newBlockingStub(this.connection.getManagedChannel());
        SiLABinaryTransfer.GetBinaryInfoRequest getBinaryInfoRequest = SiLABinaryTransfer.GetBinaryInfoRequest.newBuilder()
                .setBinaryTransferUUID(binaryInfoRequest.getBinaryTransferUUID())
                .build();
        return downloadBlockingStub.getBinaryInfo(getBinaryInfoRequest);
    }

    /**
     * Binary Download Call: GetChunk
     *
     * A message to retrieve the binary data.
     **/
    public StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunk(@NonNull StreamObserver<SiLABinaryTransfer.GetChunkResponse> streamObserver) {

        BinaryDownloadGrpc.BinaryDownloadStub binaryDownloadStub = BinaryDownloadGrpc.newStub(this.connection.getManagedChannel());
        return binaryDownloadStub.getChunk(streamObserver);
    }

    /**
     * Binary Download Call: DeleteBinaryDownload
     *
     * A message to delete the Binary after use.
     **/
    public SiLABinaryTransfer.DeleteBinaryResponse deleteBinaryDownload(@NonNull SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        BinaryDownloadGrpc.BinaryDownloadBlockingStub downloadBlockingStub = BinaryDownloadGrpc.newBlockingStub(this.connection.getManagedChannel());
        return downloadBlockingStub.deleteBinary(deleteBinaryRequest);
    }
}
