package com.silastandard.gateway.server_side.models;

import com.google.protobuf.DynamicMessage;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;

/**
 * Observers a stream and notifies the StreamCallback
 */
public class SubscriptionStreamObserver  implements StreamObserver<Object> {
    private final SubscriptionStreamCallback subscriptionStreamCallback;

    public SubscriptionStreamObserver(@NonNull SubscriptionStreamCallback subscriptionStreamCallback) {
        this.subscriptionStreamCallback = subscriptionStreamCallback;
    }

    @Override
    public void onNext(Object message) {
        DynamicMessage dynamicMessage = (DynamicMessage)message;
        this.subscriptionStreamCallback.onNext(dynamicMessage);
    }

    @Override
    public void onError(Throwable throwable) {
        this.subscriptionStreamCallback.onError(throwable);
    }

    @Override
    public void onCompleted() {
        this.subscriptionStreamCallback.onComplete();
    }

    public interface SubscriptionStreamCallback {
        boolean onNext(DynamicMessage dynamicMessage);
        void onComplete();
        void onError(Throwable throwable);
    }
}