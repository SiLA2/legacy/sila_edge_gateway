package com.silastandard.gateway.server_side.server_management;

import com.google.protobuf.Descriptors;
import com.silastandard.gateway.utils.FeatureIdConverter;
import io.grpc.*;
import io.grpc.stub.MetadataUtils;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.manager.models.Server;

import java.security.KeyException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Represent a SiLA compliant connection with a server that exposes features callable through a channel
 */
@Slf4j
public class Connection implements AutoCloseable {
    @Getter
    private final UUID serverId;
    @Getter
    private final Map<String, Descriptors.ServiceDescriptor> featureMap = new HashMap<>();
    @Getter
    private final ManagedChannel managedChannel;

    public interface ConnectionListener {
        void connectionChanged(UUID id, ManagedChannel managedChannel);
    }

    /**
     * Create a new SiLA Connection
     *
     * @param server         Server Description
     * @param managedChannel gRPC Channel to Server
     * @throws MalformedSiLAFeature if one of the features is invalid
     */
    public Connection(
            @NonNull final Server server,
            @NonNull final ManagedChannel managedChannel
    ) throws MalformedSiLAFeature {
        this.serverId = server.getConfiguration().getUuid();
        this.managedChannel = managedChannel;

        // Load gRPC Mappings
        for (val feature : server.getFeatures()) {
            String fullyQualifiedIdentifier = FeatureGenerator.generateFullyQualifiedIdentifier(feature);

            final Descriptors.FileDescriptor protoFile = ProtoMapper
                    .usingFeature(feature)
                    .generateProto();
            // changed from featureIdentifier to fullyQualifiedIdentifier
            this.featureMap.put(fullyQualifiedIdentifier, protoFile.getServices().get(0));
        }
    }

    /**
     * Returns a channel with an Interceptor for adding metadata
     *
     * @param metadataList a list of metadata defined in SiLACloudConnector
     */
    public Channel getChannelWithMetadata(List<SiLACloudConnector.Metadata> metadataList) {
        Metadata metadata = new Metadata();
        metadataList.forEach(meta -> {
            String keyIdentifier = FeatureIdConverter.convertMetaDataId(meta.getFullyQualifiedMetadataId());
            Metadata.Key<byte[]> key =
                    Metadata.Key.of(keyIdentifier, Metadata.BINARY_BYTE_MARSHALLER);
            metadata.put(key, meta.getValue().toByteArray());
        });
        ClientInterceptor interceptor = MetadataUtils.newAttachHeadersInterceptor(metadata);
        return ClientInterceptors.intercept(this.managedChannel, interceptor);
    }

    /**
     * Attaches and triggers a Listener
     *
     * @param connectionListener Listeners to changes in the connection
     */
    public void attachAndTriggerListener(@NonNull final ConnectionListener connectionListener) {
        // Subscribe to channel changes
        this.managedChannel.notifyWhenStateChanged(managedChannel.getState(false), new Runnable() {
            @Override
            public void run() {
                final ConnectivityState state = managedChannel.getState(false);
                log.debug("state of server with id `{}` is `{}`", serverId, state.name());
                connectionListener.connectionChanged(serverId, managedChannel);
                // Re-subscribe to the one-off callback with this Runnable anonymous class instance
                managedChannel.notifyWhenStateChanged(state, this);
            }
        });

        // We trigger the change once at subscription to receive the correct initial connection state
        connectionListener.connectionChanged(serverId, this.managedChannel);
    }

    /**
     * Get a feature service
     *
     * @param fullyQualifiedIdentifier The feature identifier
     * @return The matching feature service
     * @throws KeyException when no identifier does not match any feature
     */
    public Descriptors.ServiceDescriptor getFeatureService(@NonNull final String fullyQualifiedIdentifier) throws KeyException {
        if (!featureMap.containsKey(fullyQualifiedIdentifier)) {
            throw new KeyException("Cannot find fullyQualifiedIdentifier: " + fullyQualifiedIdentifier);
        } else {
            return featureMap.get(fullyQualifiedIdentifier);
        }
    }

    /**
     * Shutdown the channel
     */
    @Override
    public void close() {
        if (managedChannel != null) {
            try {
                managedChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                log.warn(e.getMessage());
            }
        }
    }
}
