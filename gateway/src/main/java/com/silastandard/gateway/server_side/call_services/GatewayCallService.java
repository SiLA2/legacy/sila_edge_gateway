package com.silastandard.gateway.server_side.call_services;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.server_management.Connection;
import com.silastandard.gateway.server_side.models.SubscriptionStreamObserver;
import io.grpc.*;
import io.grpc.stub.ClientCalls;
import lombok.NonNull;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.cloud.client.standard_features.core.silaservice.SILAServiceFeature;
import sila_java.library.core.sila.mapping.grpc.GrpcNameMapper;
import sila_java.library.manager.grpc.DynamicMessageMarshaller;

import java.security.KeyException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * Executes gRPC calls for the different message types.
 */
public class GatewayCallService {
    private static final Duration DEFAULT_CALL_TIMEOUT = Duration.ofMillis(Long.MAX_VALUE); // Infinite timeout
    private final Connection connection;
    private final Descriptors.ServiceDescriptor feature;
    private final Duration timeout;

    private final String callId;
    private final ByteString requestPayload;

    public static class Builder {
        private final Connection connection;
        private final Descriptors.ServiceDescriptor feature;

        private final String callId;
        private final ByteString payload;

        private Duration timeout = DEFAULT_CALL_TIMEOUT;

        private Builder(@NonNull final Connection connection, @NonNull final SiLAGatewayCall siLACall) {
            this.connection = connection;
            this.callId = siLACall.getCallId();
            this.payload = siLACall.getPayload();

            final String fullyQualifiedFeatureId = siLACall.getFullyQualifiedFeatureId();

            try {
                if (fullyQualifiedFeatureId.equals(SILAServiceFeature.ID)) {
                    this.feature = SiLAServiceOuterClass.getDescriptor().getServices().get(0);
                } else {
                    this.feature = this.connection.getFeatureService(fullyQualifiedFeatureId);
                }
            } catch (KeyException e) {
                throw new RuntimeException(e);
            }
        }

        public GatewayCallService build() {
            return new GatewayCallService(this);
        }

        /**
         * Set the timeout when performing a call.
         * Default value is {@link GatewayCallService#DEFAULT_CALL_TIMEOUT}
         *
         * @param timeout The timeout to set
         * @return the builder
         */
        public GatewayCallService.Builder withTimeout(@NonNull final Duration timeout) {
            this.timeout = timeout;
            return this;
        }
    }

    public static GatewayCallService.Builder newBuilder(@NonNull final Connection connection, @NonNull final SiLAGatewayCall siLACall) {
        return new GatewayCallService.Builder(connection, siLACall);
    }

    private GatewayCallService(@NonNull final GatewayCallService.Builder builder) {
        this.connection = builder.connection;
        this.feature = builder.feature;
        this.timeout = builder.timeout;
        this.callId = builder.callId;
        this.requestPayload = builder.payload;

        if (timeout.isZero() || timeout.isNegative()) {
            throw new IllegalArgumentException("Timeout must be greater than 0");
        }
    }

    /**
     * Executes an Unobservable Command.
     **/
    public DynamicMessage executeUnobservableCommand(@NonNull DynamicMessage requestMessage, @NonNull List<SiLACloudConnector.Metadata> metadataList) {
        return executeGatewayCall(getChannelOnMetadata(metadataList), requestMessage, callId);
    }

    /**
     * Executes an Unobservable Property.
     **/
    public DynamicMessage executeGetUnobservableProperty(@NonNull List<SiLACloudConnector.Metadata> metadataList) {
        String requestMessageId = GrpcNameMapper.getUnobservableProperty(callId);
        return executeGatewayCall(getChannelOnMetadata(metadataList), getRequestMessage(requestMessageId),requestMessageId);
    }

    /**
     * Initiates an Observable Command Execution.
     **/
    public DynamicMessage executeObservableCommandInitiation(@NonNull DynamicMessage requestMessage, @NonNull List<SiLACloudConnector.Metadata> metadataList) {
        return executeGatewayCall(getChannelOnMetadata(metadataList), requestMessage, callId);
    }

    /**
     * Requests the result of an Observable Command.
     **/
    public DynamicMessage executeObservableCommandGetResponse() {
        String requestMessageId = GrpcNameMapper.getResult(callId);
        return executeGatewayCall(connection.getManagedChannel(), getRequestMessage(requestMessageId), requestMessageId);
    }

    /**
     * Retrieves the list of fully Qualified Identifiers that are affected by the SiLA Client Metadata.
     **/
    public DynamicMessage executeGetFCPAffectedByMetadata() {
        String requestMessageId = GrpcNameMapper.getMetadataRPC(callId);
        return executeGatewayCall(connection.getManagedChannel(), getRequestMessage(requestMessageId), requestMessageId);
    }

    /**
     * Executes an unary gRPC-Call
     **/
    private DynamicMessage executeGatewayCall(@NonNull Channel channel, @NonNull DynamicMessage requestMessage, @NonNull String messageCallID) {
        final Descriptors.MethodDescriptor methodDescriptor = this.feature.findMethodByName(messageCallID);
        if (methodDescriptor == null) {
            throw new RuntimeException("Server " + connection.getServerId() + " doesn't expose call to " + messageCallID);
        }

        final MethodDescriptor<Object, Object> newMethodDescriptor = getMethodDescriptor(methodDescriptor);

        return (DynamicMessage) ClientCalls.blockingUnaryCall(
                channel.newCall(
                        newMethodDescriptor,
                        CallOptions.DEFAULT.withDeadlineAfter(timeout.getSeconds(), TimeUnit.SECONDS)
                ),
                requestMessage
        );
    }

    /**
     * Subscribes to an Command Execution Info.
     **/
    public void executeObservableCommandInfoSubscription(@NonNull Context.CancellableContext contextInfoSubscription, @NonNull SubscriptionStreamObserver.SubscriptionStreamCallback subscriptionStreamCallback) {
        String requestMessageId = GrpcNameMapper.getStateCommand(callId);
        executeSubscriptionStream(connection.getManagedChannel(), getRequestMessage(requestMessageId), requestMessageId, subscriptionStreamCallback, contextInfoSubscription);
    }

    /**
     * Subscribes to an Observable Command Intermediate Response.
     **/
    public void executeIntermediateCommandSubscription(@NonNull Context.CancellableContext contextInfoSubscription, @NonNull SubscriptionStreamObserver.SubscriptionStreamCallback subscriptionStreamCallback) {
        String requestMessageId = GrpcNameMapper.getIntermediateCommand(callId);
        executeSubscriptionStream(connection.getManagedChannel(), getRequestMessage(requestMessageId), requestMessageId, subscriptionStreamCallback, contextInfoSubscription);
    }

    /**
     * Subscribes to an Observable Command Intermediate Response.
     **/
    public void executePropertySubscription(@NonNull List<SiLACloudConnector.Metadata> metadataList, @NonNull Context.CancellableContext contextInfoSubscription, @NonNull SubscriptionStreamObserver.SubscriptionStreamCallback subscriptionStreamCallback) {
        String requestMessageId = GrpcNameMapper.getObservableProperty(callId);
        executeSubscriptionStream(getChannelOnMetadata(metadataList), getRequestMessage(requestMessageId), requestMessageId, subscriptionStreamCallback, contextInfoSubscription);
    }

    /**
     * Executes an async server streaming gRPC-Call.
     **/
    private void executeSubscriptionStream(@NonNull final Channel channel,
                                           @NonNull final DynamicMessage requestMessage,
                                           @NonNull final String messageCallID,
                                           @NonNull final SubscriptionStreamObserver.SubscriptionStreamCallback subscriptionStreamCallback,
                                           @NonNull final Context.CancellableContext contextInfoSubscription) {
        final Descriptors.MethodDescriptor method = this.feature.findMethodByName(messageCallID);

        final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
        // https://github.com/grpc/grpc-java/issues/3095 use  io.grpc.Context - CancellableContext
        contextInfoSubscription.run(() -> {
            final ClientCall<Object, Object> clientCall = channel.newCall(
                    methodDescriptor,
                    CallOptions.DEFAULT.withDeadlineAfter(timeout.getSeconds(), TimeUnit.SECONDS)
            );
            final SubscriptionStreamObserver streamObserver = new SubscriptionStreamObserver(subscriptionStreamCallback);
            ClientCalls.asyncServerStreamingCall(clientCall, requestMessage, streamObserver);
        });
    }

    /**
     * Gets the Request Message.
     **/
    public DynamicMessage getRequestMessage(String messageCallID) {
        // feature, messageCallID
        final Descriptors.MethodDescriptor methodDescriptor = this.feature.findMethodByName(messageCallID);
        if (methodDescriptor == null) {
            throw new RuntimeException("Server " + connection.getServerId() + " doesn't expose call to " + messageCallID);
        }
        try {
            return DynamicMessage.parseFrom(methodDescriptor.getInputType(), requestPayload);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private Channel getChannelOnMetadata(@NonNull List<SiLACloudConnector.Metadata> metadataList) {
        if(!metadataList.isEmpty()) {
            return connection.getChannelWithMetadata(metadataList);
        } else {
            return connection.getManagedChannel();
        }
    }

    // Private Helpers to retrieve dynamic protobuf constructs
    private static MethodDescriptor<Object, Object> getMethodDescriptor(
            @NonNull final Descriptors.MethodDescriptor method
    ) {
        return (MethodDescriptor
                .newBuilder()
                .setType(MethodDescriptor.MethodType.UNARY)
                .setFullMethodName(getFullMethodName(method))
                .setRequestMarshaller(new DynamicMessageMarshaller(method.getInputType()))
                .setResponseMarshaller(new DynamicMessageMarshaller(method.getOutputType()))
                .build()
        );
    }

    private static String getFullMethodName(@NonNull final Descriptors.MethodDescriptor method) {
        return (MethodDescriptor.generateFullMethodName(
                method.getService().getFullName(),
                method.getName())
        );
    }
}