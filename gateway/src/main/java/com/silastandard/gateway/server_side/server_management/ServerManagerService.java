package com.silastandard.gateway.server_side.server_management;

import com.silastandard.gateway.server_side.call_services.GatewayBinaryCallService;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.call_services.GatewayCallService;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.discovery.SiLADiscovery;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.ServerAdditionException;
import sila_java.library.manager.ServerListener;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.server_management.ServerConnectionException;
import sila_java.library.manager.server_management.ServerLoading;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import static  sila_java.library.manager.server_management.ServerLoading.*;

/**
 * Manager service to manage SiLA Servers
 *
 * This Manager will keep a cache of all servers added either through discovery or via invoking
 * the add function. It will also keep track of their Status with a Heartbeat.
 */
@Slf4j
public class ServerManagerService implements AutoCloseable {
    private static final int MAX_SERVICE_TIMEOUT = 3; // [s]

    private final SiLADiscovery discovery = new SiLADiscovery();
    private final Map<UUID, Connection> connections = new ConcurrentHashMap<>();
    private final Map<UUID, Server> servers = new ConcurrentHashMap<>();
    private final List<ServerListener> serverListenerList = new CopyOnWriteArrayList<>();

    private final sila_java.library.core.discovery.ServerListener serverDiscoveryListener;

    /**
     * Constructor to instantiate the Server Discovery Listener.
     * Triggers the addServer function when discovery finds a server.
     */
    public ServerManagerService () {
        this.serverDiscoveryListener = (instanceId, host, port) -> {
            try {
                final Server server = getServer(instanceId);
                if (server != null && (server.getStatus() == Server.Status.ONLINE)) {
                    log.debug("Discovery found server with uuid: " + instanceId);
                } else {
                    addServer(host, port);
                }
            } catch (final ServerAdditionException e) {
                log.warn(
                        "Server with instance id: '{}' was not added because the following error occurred: '{}'",
                        instanceId,
                        e.getMessage()
                );
            }
        };
    }

    /**
     * Adds the Listener to get notified when server status changed.
     */
    public void addServerListener(@NonNull ServerListener serverListener) {
        serverListenerList.add(serverListener);
    }

    /**
     * Removes the Listener.
     */
    public void removeServerListener(@NonNull ServerListener serverListener) {
        serverListenerList.remove(serverListener);
    }

    /**
     * Clear any servers from manager.
     */
    public void clear() {
        this.servers.keySet().forEach(this::removeServer);
        this.discovery.clearCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        this.discovery.close();
        this.connections.values().forEach(Connection::close);
        this.connections.clear();
        this.servers.clear();
        this.serverListenerList.clear();
    }

    /**
     * Removes the Server from the manager.
     *
     * @param id Server Id used for referencing to the Server
     */
    public synchronized void removeServer(@NonNull final UUID id) {
        final Connection connection = connections.get(id);
        final Server server = servers.get(id);

        if (connection == null || server == null) {
            log.warn("Server with id: " + id + " already removed from cache.");
            return;
        }

        // Inform listeners of server removal
        serverListenerList.forEach(listener -> listener.onServerRemoved(id, server));

        // First close connection to stop channel updates
        connections.remove(id);
        connection.close();
        servers.remove(id);

        log.info("[removeServer] removed serverId={}", id);
    }

    /**
     * Adds a Server to the manager.
     *
     * @param host Host on which SiLA Server is exposed
     * @param port Port on which SiLA Server is exposed
     * @throws ServerAdditionException if unable to add the server
     */
    public synchronized void addServer(@NonNull final String host, final int port) throws ServerAdditionException {
        // Create Server
        final Server server = new Server();
        server.setJoined(new Date()); // change to OffsetDateTime.now() -> Date is depricated
        server.setHost(host);
        server.setPort(port);
        server.setStatus(Server.Status.ONLINE);

        try {
            addServer(server);
        } catch (final ServerAdditionException e) {
            serverListenerList.forEach(listener -> listener.onServerAdditionFail(host, port, e.getMessage()));
            throw e;
        }
    }

    /**
     * Adds a Server to the manager
     *
     * @param server the server to add
     * @throws ServerAdditionException if unable to add the server
     */
    private void addServer(@NonNull final Server server) throws ServerAdditionException {
        // Establish Connection
        final ManagedChannel managedChannel;
        // Load SiLA Server
        try {
            managedChannel = ServerLoading.attemptConnectionWithServer(server);
        } catch (final ServerConnectionException e) {
            throw new ServerAdditionException(server, e.getMessage());
        }
        try {
            ServerLoading.loadServer(server, managedChannel);
        } catch (final ServerLoadingException e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        log.info(
                "[addServer] Resolved SiLA Server serverName={} on {}:{}",
                server.getConfiguration().getName(),
                server.getHost(),
                server.getPort()
        );

        // Protect against re-adding when Online
        final UUID uuid = server.getConfiguration().getUuid();
        final Server serverInManager = servers.get(uuid);
        final boolean isNewServer = (serverInManager == null);
        if (!isNewServer && (serverInManager.getStatus() == Server.Status.ONLINE)) {
            log.warn("Server with id `{}` is already present in the manager and online! ", uuid);
            managedChannel.shutdownNow();
            throw new ServerAdditionException(
                    server,
                    "Server with id [" + uuid + "] is already in the manager and online"
            );
        }

        // Add Server and notify when successful resolving
        final Connection connection;
        try {
            connection = new Connection(server, managedChannel);
        } catch (final MalformedSiLAFeature e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        servers.put(uuid, server);
        final Connection previousConnection = connections.put(uuid, connection);
        if (previousConnection != null) {
            previousConnection.close();
        }
        if (isNewServer) {
            serverListenerList.forEach(listener -> listener.onServerAdded(uuid, server));
        } else {
            serverListenerList.forEach(listener -> listener.onServerChange(uuid, server));
        }
        connection.attachAndTriggerListener(this::updateServerState);
    }

    public Server getServer(@NonNull final UUID serverId) {
        return servers.get(serverId);
    }

    /**
     * Private Utility to check and update the state of the server.
     *
     * Attempt to execute a call to retrieve the UUID of the server.
     * If the call succeed and the UUID match, the status of server will be set to online, offline otherwise.
     *
     * @param serverId The server unique identifier
     * @param channel The server channel
     */
    private void updateServerState(@NonNull final UUID serverId, @NonNull final ManagedChannel channel) {
        final Server server = this.servers.get(serverId);
        if (server == null) {
            log.warn("Unable to update server state because server {} does not exist", serverId);
            return;
        }
        final ConnectivityState state = channel.getState(false);
        if (state == ConnectivityState.IDLE) {
            CompletableFuture.runAsync(() -> {
                Server.Status status = Server.Status.OFFLINE;
                try {
                    final UUID uuid = getServerId(SiLAServiceGrpc.newBlockingStub(channel));
                    if (server.getConfiguration().getUuid().equals(uuid)) {
                        status = Server.Status.ONLINE;
                    } else {
                        log.debug(
                                "GetServerId returned by server UUID: {} differ from the one in manager {}",
                                uuid,
                                server.getConfiguration().getUuid()
                        );
                    }
                } catch (final Exception e) {
                    log.debug("Failed UUID Retrieval {}", e.getMessage());
                }
                if(status.equals(Server.Status.OFFLINE)) {
                    serverListenerList.forEach(listener -> listener.onServerRemoved(serverId, server));
                }
                this.setServerStatus(serverId, status);
            });
        }
    }

    /**
     * Sets the Status of a Server.
     *
     * @param id Server Id
     * @param status Server Status
     */
    private void setServerStatus(@NonNull final UUID id, @NonNull final Server.Status status) {
        final Connection siLAConnection = this.connections.get(id);

        if (siLAConnection == null) {
            log.warn("Server with id " + id + " doesn't exist.");
            return;
        }

        final Server server = this.servers.get(id);

        // Only Notify Change, if Change happened
        if (!server.getStatus().equals(status)) {
            server.setStatus(status);
            log.info("[changeServerStatus] change {} status to {}", id, status.toString());
            // also update listeners
            serverListenerList.forEach(listener -> listener.onServerChange(id, server));
        }
    }

    /**
     * Sets the Status of a Server.
     *
     * @implNote serverNameResponse has no response at the moment, as such
     * we proceed assuming the setServerName was successful and catching RcpException at application level
     * alternatively, wrap this function call with try {} catch(RpcException e) {}
     *
     * @param id Server Id
     * @param newServerName String Server Name
     */
    public void setServerName(@NonNull final UUID id, @NonNull final String newServerName) {
        final Connection connection = this.connections.get(id);

        if (connection == null) {
            log.warn("Server with id " + id + " doesn't exit.");
            return;
        }

        final Server server = this.servers.get(id);
        if (!server.getConfiguration().getName().equals(newServerName)) {
            log.info("[setServerName] server name changed to {}", newServerName);
            @NonNull final ManagedChannel managedChannel = connection.getManagedChannel();
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAServiceBlockingStub = SiLAServiceGrpc
                    .newBlockingStub(managedChannel);

            siLAServiceBlockingStub
                    .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .setServerName(SiLAServiceOuterClass.SetServerName_Parameters.newBuilder().setServerName(
                            SiLAString.from(newServerName)).build()
                    );
            // check for updated name on server
            final String serverName = getServerName(siLAServiceBlockingStub);
            if (serverName.equals(newServerName)) {
                log.info("[setServerName] server name changed to {}", newServerName);
                // update server prior to updating through the listener
                server.setConfiguration(server.getConfiguration().withName(newServerName));
                serverListenerList.forEach(listener -> listener.onServerChange(id, server));
            } else {
                throw new IllegalStateException("Server Name was not updated on SiLA Server.");
            }
        }
    }

    /**
     * Gets the map of servers.
     *
     * @implNote Synchronised call because servers might be in removal state
     *
     * @return A read only map of servers
     */
    public synchronized Map<UUID, Server> getServers() {
        return Collections.unmodifiableMap(this.servers);
    }

    public void scanNetwork(int iteration) {
        addListenerToDiscovery();
        discovery.scanNetwork(iteration);
        removeListenerFromDiscovery();
    }

    /**
     * Creates a Server Call Service.
     */
    public GatewayCallService newCallService(@NonNull SiLAGatewayCall siLACall) {
        return GatewayCallService.newBuilder(connections.get(siLACall.getServerUUID()), siLACall).build();
    }

    /**
     * Creates a Server Binary Call Service.
     */
    public GatewayBinaryCallService newBinaryCallService(UUID serverUUID ) {
        return new GatewayBinaryCallService(connections.get(serverUUID));
    }

    private void addListenerToDiscovery() {
        log.info("ServerManagerService: add listener to discovery");
        this.discovery.addListener(serverDiscoveryListener);
    }

    private void removeListenerFromDiscovery() {
        log.info("ServerManagerService: remove listener from discovery");
        this.discovery.removeListener(serverDiscoveryListener);
    }
}
