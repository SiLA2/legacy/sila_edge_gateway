package com.silastandard.gateway;

import com.silastandard.gateway.monitoring_configuration.server.ConfigurationAndMonitoringServer;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.MonitoringSubscriptionService;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.server_base.utils.ArgumentHelper;


import static sila_java.library.core.utils.Utils.blockUntilStop;

@Slf4j
public class Gateway {
    public static final String SERVER_TYPE = "Monitoring and Configuration Server";
    /**
     * Main function to initiate and setup all necessary services and servers to run the gateway
     */
    public static void main(String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        final GatewayServices gatewayServices = new GatewayServices(
                new MonitoringSubscriptionService(),
                new ServerManagerService());

        try (final ConfigurationAndMonitoringServer configurationAndMonitoringServer = new ConfigurationAndMonitoringServer(argumentHelper, gatewayServices)) {
            Runtime.getRuntime().addShutdownHook(new Thread(configurationAndMonitoringServer::close));
            blockUntilStop();
        }
        log.info("termination complete.");
    }
}
