package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class CommandResponse {
    public static final String NAME = "CommandResponse";
    public static final String TYPE = "<DataType><Structure><Element><Identifier>CommandIdentifier</Identifier> <DisplayName>Command Identifier</DisplayName> <Description>The fully qualified command identifier</Description> <DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><FullyQualifiedIdentifier>CommandIdentifier</FullyQualifiedIdentifier> </Constraints></Constrained></DataType></Element><Element><Identifier>CommandResponse</Identifier> <DisplayName>Command Response</DisplayName> <Description>The response of an Unobservable Command.</Description> <DataType><Basic>Any</Basic></DataType></Element></Structure></DataType>";
}
