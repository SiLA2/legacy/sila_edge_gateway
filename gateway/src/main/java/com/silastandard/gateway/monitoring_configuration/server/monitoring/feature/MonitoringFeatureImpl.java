package com.silastandard.gateway.monitoring_configuration.server.monitoring.feature;

import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.MonitoringSubscriptionService;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceGrpc;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Implementation of the GatewayMonitoringService feature for the monitoring functionalities of the Gateway.
 */
@Slf4j
public class MonitoringFeatureImpl extends GatewayMonitoringServiceGrpc.GatewayMonitoringServiceImplBase {

    private static final int EXECUTION_LIFETIME = 60; // [s] LifeTime of the command after CommandExecution is done
    private final GatewayServices gatewayServices;


    public MonitoringFeatureImpl(GatewayServices gatewayServices) {
        this.gatewayServices = gatewayServices;
    }

    ObservableCommandManager<
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters,
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses
                    > monitoringCommandManager = new ObservableCommandManager<>(
                        new ObservableCommandTaskRunner(1,1),
                        this::runCommandTask,
                        Duration.ofSeconds(EXECUTION_LIFETIME)
                    );

    /**
     * The command execution task.
     * Subscribes to messages addressed to the server on the parameter list.
     * If the observation of the intermediate messages is not executed before the execution time has expired, the command ends.
     * Otherwise it runs as long as the observation is going on.
     */
    private GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses runCommandTask(
            @NonNull final ObservableCommandWrapper<
                            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters,
                            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses> commandWrapper
            ) {
        List<SiLAFramework.String> serverUUIDList = commandWrapper.getParameter().getServerUUIDListList();
        List<String> uuidList = serverUUIDList.stream().map(SiLAFramework.String::getValue).collect(Collectors.toList());

        MonitoringSubscriptionService.MonitoringMessageListener listener = commandWrapper::notifyIntermediateResponse;
        gatewayServices.getMonitoringSubscriptionService().addListener(uuidList, listener);
        log.info("[subscribeObserveCurrentMessages] Monitoring subscription on Intermediate Response started for servers - " + uuidList);

        CountDownLatch doneSignal = new CountDownLatch(1);
        new Thread(new WaitForIntermediateResponseSub(doneSignal, commandWrapper)).start();
        try {
            // Wait for Intermediate Response Subscription with timeout
            doneSignal.await(EXECUTION_LIFETIME, TimeUnit.SECONDS);

            // Executes Command until StreamObserver for intermediate messages is cancelled.
            while (!commandWrapper.getIntermediateResponseObservers().isEmpty()) {
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            log.error("Command with ExecutionID {} failed due to timeout.",  commandWrapper.getExecutionId());
        }

        log.info("[subscribeObserveCurrentMessages] Subscription ended for servers - " + uuidList);
        gatewayServices.getMonitoringSubscriptionService().removeListener(listener);
        return GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses.newBuilder().build();
    }

    /**
     * Implementation of the ObserveCurrentMessages Command.
     */
    @Override
    public void observeCurrentMessages(
            @NonNull GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters request,
            @NonNull StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.monitoringCommandManager.addCommand(request, responseObserver);
    }

    /**
     * Implementation to observe command execution.
     */
    @Override
    public void observeCurrentMessagesInfo(
            @NonNull final SiLAFramework.CommandExecutionUUID commandExecutionUUID,
            @NonNull StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.monitoringCommandManager.get(commandExecutionUUID).addStateObserver(responseObserver);
    }

    /**
     * Implementation to get the result of the command execution.
     */
    @Override
    public void observeCurrentMessagesResult(
            @NonNull final SiLAFramework.CommandExecutionUUID commandExecutionUUID,
            @NonNull StreamObserver<GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses> responseObserver) {
        this.monitoringCommandManager.get(commandExecutionUUID).sendResult(responseObserver);
    }

    /**
     * Implementation to observer the intermediate messages of the command.
     */
    @Override
    public void observeCurrentMessagesIntermediate(
            @NonNull final SiLAFramework.CommandExecutionUUID commandExecutionUUID,
            @NonNull StreamObserver<GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses> responseObserver) {
        this.monitoringCommandManager.get(commandExecutionUUID).addIntermediateResponseObserver(responseObserver);
    }

    /**
     * Waits for observation of the intermediate messages of the command.
     */
    class WaitForIntermediateResponseSub implements Runnable {
        private final CountDownLatch doneSignal;
        private final ObservableCommandWrapper<
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters,
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses> commandWrapper;

        WaitForIntermediateResponseSub(CountDownLatch doneSignal, ObservableCommandWrapper<
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters,
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses> commandWrapper) {
            this.doneSignal = doneSignal;
            this.commandWrapper = commandWrapper;
        }
        public void run() {
            try {
                // Wait until Intermediate Response Subscription
                while (commandWrapper.getIntermediateResponseObservers().isEmpty()) {
                    TimeUnit.SECONDS.sleep(1);
                }
                doneSignal.countDown();
            } catch (InterruptedException ex) {log.error(ex.getMessage());} // return;
        }
    }
}