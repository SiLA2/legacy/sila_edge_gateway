package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class MetaDataRequest {
    public static final String NAME = "MetaDataRequest";
    public static final String TYPE = "<DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><FullyQualifiedIdentifier>MetadataIdentifier</FullyQualifiedIdentifier></Constraints></Constrained></DataType>";
}
