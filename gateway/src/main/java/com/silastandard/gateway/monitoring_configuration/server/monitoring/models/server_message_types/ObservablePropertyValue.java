package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class ObservablePropertyValue {
    public static final String NAME = "ObservablePropertyValue";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>PropertyIdentifier</Identifier> <DisplayName>Property Identifier</DisplayName> <Description>The fully qualified property identifier.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <FullyQualifiedIdentifier>PropertyIdentifier</FullyQualifiedIdentifier> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>PropertyValue</Identifier> <DisplayName>Property Value</DisplayName> <Description> The response of an Observable Property read. Any Type corresponds to the original Unobservable Property DataType described in the feature. </Description> <DataType> <Basic>Any</Basic> </DataType> </Element> </Structure> </DataType>";
}