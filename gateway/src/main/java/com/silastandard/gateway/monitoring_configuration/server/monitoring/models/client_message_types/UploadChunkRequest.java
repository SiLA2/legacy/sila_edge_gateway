package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class UploadChunkRequest {
    public static final String NAME = "UploadChunkRequest";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>BinaryTransferUUID</Identifier> <DisplayName>Binary Transfer UUID</DisplayName> <Description>Binary Transfer UUID refers to the specific binary data.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}</Pattern> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>ChunkIndex</Identifier> <DisplayName>Chunk Index</DisplayName> <Description>The Binary Chunk Index.</Description> <DataType> <Basic>Integer</Basic> </DataType> </Element> <Element> <Identifier>Payload</Identifier> <DisplayName>Payload</DisplayName> <Description>The Payload</Description> <DataType> <Basic>Binary</Basic> </DataType> </Element> </Structure> </DataType>";
}
