package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class ErrorResponse {
    public static final String VALIDATION_ERROR_TYPE = "<DataType><Structure><Element><Identifier>Parameter</Identifier><DisplayName>Parameter</DisplayName><Description>Theparameterforwhichthevalidationfailed.</Description><DataType><Basic>String</Basic></DataType></Element><Element><Identifier>Message</Identifier><DisplayName>Message</DisplayName><Description>TheErrorMessage.</Description><DataType><Basic>String</Basic></DataType></Element></Structure></DataType>";
    public static final String VALIDATION_ERROR_NAME = "ValidationError";

    public static final String UNDEFINED_ERROR_TYPE = "<DataType> <Basic>String</Basic> </DataType>";
    public static final String UNDEFINED_ERROR_NAME = "UndefinedExecutionError";

    public static final String DEFINED_ERROR_TYPE =  "<DataType> <Structure> <Element> <Identifier>ErrorIdentifier</Identifier> <DisplayName>Error Identifier</DisplayName> <Description>Unique Identifier of the Defined Execution Error.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <FullyQualifiedIdentifier>DefinedExecutionErrorIdentifier</FullyQualifiedIdentifier> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>Message</Identifier> <DisplayName>Message</DisplayName> <Description>The Error Message.</Description> <DataType> <Basic>String</Basic> </DataType> </Element> </Structure> </DataType>";
    public static final String DEFINED_ERROR_NAME = "DefinedExecutionError";

    public static final String FRAMEWORK_ERROR_TYPE = "<DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Set> <Value>Command Execution Not Accepted</Value> <Value>Invalid Command Execution UUID</Value> <Value>Command Execution Not Finished</Value> <Value>Invalid Metadata Error</Value> <Value>No Metadata Allowed Error</Value> </Set> </Constraints> </Constrained> </DataType>";
    public static final String FRAMEWORK_ERROR_NAME = "FrameworkError";

    public static final String CONNECTION_ERROR_TYPE = "<DataType> <Basic>String</Basic> </DataType>";
    public static final String CONNECTION_ERROR_NAME = "ConnectionError";
}