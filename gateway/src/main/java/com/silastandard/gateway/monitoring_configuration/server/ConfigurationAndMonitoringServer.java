package com.silastandard.gateway.monitoring_configuration.server;

import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client.ClientConnectionConfigurationFeature;
import com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client.ConnectionException;
import com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client.ConnectionManager;
import com.silastandard.gateway.monitoring_configuration.server.connection_configuration_server.ServerConfigurationFeature;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.feature.MonitoringFeatureImpl;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.connection_configuration.SiLAClientDAO;
import sila_java.library.cloud.connection_configuration.model.ConnectionConfiguration;
import sila_java.library.cloud.connection_configuration.model.SiLAClientInformation;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.utils.FileUtils;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * This server implements the ConnectionConfigurationService feature for the client connection,
 * the GatewayConfigurationService feature which provides the server-side configuration functionalities
 * and the GatewayMonitoringService feature for the monitoring functionalities of the Gateway.
 */
@Slf4j
public class ConfigurationAndMonitoringServer implements AutoCloseable {
    public static final String SERVER_TYPE = "Gateway Monitoring and Configuration Server";

    private final SiLAServer server;
    private final ConnectionManager connectionManager;
    final GatewayServices gatewayService;


    public ConfigurationAndMonitoringServer(@NonNull final ArgumentHelper argumentHelper, @NonNull final GatewayServices gatewayServices) {
        this.connectionManager = new ConnectionManager(gatewayServices);
        this.gatewayService = gatewayServices;

        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "This server implements the GatewayConfigurationService feature which provides " +
                        "the server-side configuration functionalities, the ConnectionConfigurationService for client connection " +
                        "and the GatewayMonitoringService feature for the monitoring functionalities of the Gateway.",
                "www.sila-standard.org",
                "v0.0"
        );
        try {
            final SiLAServer.Builder builder;
            Optional<Path> configFile = argumentHelper.getConfigFile();
            if (configFile.isPresent()) {
                builder = SiLAServer.Builder.withConfig(configFile.get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            builder.addFeature(
                    FileUtils.getResourceContent("GatewayConfigurationService.sila.xml"),
                    new ServerConfigurationFeature(gatewayServices));
            builder.addFeature(
                    FileUtils.getResourceContent("GatewayMonitoringService.sila.xml"),
                    new MonitoringFeatureImpl(gatewayServices));
            builder.addFeature(
                    getResourceContent("sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService.sila.xml"),
                    new ClientConnectionConfigurationFeature(connectionManager));

            this.server = builder.start();

            SiLAClientDAO siLAClientDAO = new SiLAClientDAO();
            ConnectionConfiguration configuration = siLAClientDAO.getConfiguration();

            if (configuration.isServerInitiatedModeEnabled()) {
                for (SiLAClientInformation clientInformation : configuration.getClientInformation().values()) {
                    String clientName = clientInformation.getClientName();
                    log.info("connecting to SiLA Client {}", clientName);
                    try {
                        connectionManager.connect(clientInformation);
                        log.info("Connected successfully to " + clientName);
                    } catch(ConnectionException e) {
                        log.error(e.getMessage());
                    }
                }
            }

        } catch(IOException | SelfSignedCertificate.CertificateGenerationException e) {
            log.error("Something went wrong when building / starting server", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.gatewayService.clear();
        this.server.close();
        connectionManager.closeAndRemoveGatewayServers();
    }
}
