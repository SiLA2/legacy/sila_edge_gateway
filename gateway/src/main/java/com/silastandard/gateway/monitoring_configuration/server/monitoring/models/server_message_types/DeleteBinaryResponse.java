package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class DeleteBinaryResponse {
    public static final String NAME = "DeleteBinaryResponse";
    public static final String TYPE = "<DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>^$</Pattern> </Constraints> </Constrained> </DataType>";
}
