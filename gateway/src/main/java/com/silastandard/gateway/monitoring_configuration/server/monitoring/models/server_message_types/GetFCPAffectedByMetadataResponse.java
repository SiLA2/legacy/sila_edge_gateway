package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class GetFCPAffectedByMetadataResponse {
    public static final String NAME = "GetFCPAffectedByMetadataResponse";
    public static final String TYPE = "<DataType><List><DataType><Basic>String</Basic></DataType></List></DataType>";
}
