package com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceGrpc;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;
import sila_java.library.cloud.connection_configuration.model.SiLAClientInformation;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.util.List;

/**
 * Implementation of the ConnectionConfigurationService feature for the client connection configuration
 */
@Slf4j
public class ClientConnectionConfigurationFeature extends ConnectionConfigurationServiceGrpc.ConnectionConfigurationServiceImplBase {

    private final ConnectionManager connectionManager;
    private final Object lock = new Object();

    public ClientConnectionConfigurationFeature(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Implementation of the EnableServerInitiatedConnectionMode Command to enable the server initiated connection mode.
     */
    @Override
    public void enableServerInitiatedConnectionMode(ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses> responseObserver) {
        connectionManager.enableServerInitiatedConnectionMode(true);
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses.getDefaultInstance());
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the DisableServerInitiatedConnectionMode Command to disable the server initiated connection mode.
     */
    @Override
    public void disableServerInitiatedConnectionMode(ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses> responseObserver) {
        connectionManager.enableServerInitiatedConnectionMode(false);
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses.getDefaultInstance());
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the ConnectSiLAClient Command to connect a new SiLA Client.
     */
    @Override
    public void connectSiLAClient(ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses> responseObserver) {
        synchronized(lock) {
            if (!request.hasClientName()) {
                responseObserver.onError(SiLAErrors.generateValidationError("ClientName", "Client name required"));
                return;
            }
            if(!request.hasSiLAClientHost()) {
                responseObserver.onError(SiLAErrors.generateValidationError("SiLAClientHost", "SiLAClient Host required"));
                return;
            }
            if (!request.hasSiLAClientPort()) {
                responseObserver.onError(SiLAErrors.generateValidationError("SiLAClientPort", "SiLAClient Port required"));
                return;
            }
            if(!request.hasPersist()) {
                responseObserver.onError(SiLAErrors.generateValidationError("Persist", "Persist value required"));
                return;
            }

            SiLAClientInformation clientInformation = SiLAClientInformation.builder()
                    .clientName(request.getClientName().getValue())
                    .siLAClientHost(request.getSiLAClientHost().getValue())
                    .siLAClientPort((int) request.getSiLAClientPort().getValue())
                    .persist(request.getPersist().getValue())
                    .build();
            try {
                connectionManager.connect(clientInformation);
                responseObserver.onNext(ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses.newBuilder().build());
                responseObserver.onCompleted();
            } catch (ConnectionException e) {
                responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(e.getMessage()));
            }
        }
    }

    /**
     * Implementation of the DisconnectSiLAClient Command to disconnect a configured SiLA Client.
     */
    @Override
    public void disconnectSiLAClient(ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses> responseObserver) {
        synchronized(lock) {
            if (!request.hasClientName()) {
                responseObserver.onError(SiLAErrors.generateValidationError("ClientName", "Client name required"));
                return;
            }
            if(!request.hasRemove()) {
                responseObserver.onError(SiLAErrors.generateValidationError("Remove", "Remove boolean value required"));
                return;
            }
            SiLAClientInformation clientInformation = SiLAClientInformation.builder()
                    .clientName(request.getClientName().getValue())
                    .persist(!request.getRemove().getValue())
                    .build();

            try {
                connectionManager.disconnect(clientInformation);
                responseObserver.onNext(ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses.getDefaultInstance());
                responseObserver.onCompleted();
            } catch (ConnectionException e) {
                responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(e.getMessage()));
            }
        }
    }

    /**
     * Implementation of the ServerInitiatedConnectionModeStatus Property. It returns whether or not the server initiated connection mode is active.
     */
    @Override
    public void getServerInitiatedConnectionModeStatus(ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Responses> responseObserver) {
        boolean serverInitiatedModeActive = connectionManager.isServerInitiatedModeActive();
        ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Responses response = ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Responses.newBuilder()
                .setServerInitiatedConnectionModeStatus(SiLABoolean.from(serverInitiatedModeActive))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the ConfiguredSiLAClients Property. Gets the configured SiLA Clients used for server-initiated connections.
     */
    @Override
    public void getConfiguredSiLAClients(ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses> responseObserver) {
        List<SiLAClientInformation> clientInformationList = connectionManager.getSiLAClientList();
        ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.Builder responseBuilder = ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.newBuilder();

        for (SiLAClientInformation siLAClientInformation : clientInformationList) {
            ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct.Builder builder = ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct.newBuilder();

            builder.setClientName(SiLAString.from(siLAClientInformation.getClientName()));
            builder.setSiLAClientHost(SiLAString.from(siLAClientInformation.getSiLAClientHost()));
            builder.setSiLAClientPort(SiLAInteger.from(siLAClientInformation.getSiLAClientPort()));

            responseBuilder.addConfiguredSiLAClients(builder);

        }

        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();
    }
}
