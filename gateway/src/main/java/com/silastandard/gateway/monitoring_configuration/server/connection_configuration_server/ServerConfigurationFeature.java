package com.silastandard.gateway.monitoring_configuration.server.connection_configuration_server;

import com.silastandard.gateway.GatewayServices;
import io.grpc.stub.StreamObserver;
import sila2.org.gateway.gateway.gatewayconfigurationservice.v1.GatewayConfigurationServiceGrpc;
import sila2.org.gateway.gateway.gatewayconfigurationservice.v1.GatewayConfigurationServiceOuterClass;
import sila_java.library.core.models.Feature;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLADate;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.utils.XMLMarshaller;
import sila_java.library.manager.ServerAdditionException;
import sila_java.library.manager.models.Server;

import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Implementation of the GatewayConfigurationService feature which provides the server-side configuration functionalities
 */
public class ServerConfigurationFeature extends GatewayConfigurationServiceGrpc.GatewayConfigurationServiceImplBase {

    private final GatewayServices gatewayServices;

    public ServerConfigurationFeature(GatewayServices gatewayServices) {
        this.gatewayServices = gatewayServices;
    }

    /**
     * Implementation of the TriggerServerDiscovery Command.
     * The Server Discovery gets triggered and the found servers get added to the gateway.
     * Features of the servers are now made available to a SiLA client connected to the gateway.
     */
    @Override
    public synchronized void triggerServerDiscovery(GatewayConfigurationServiceOuterClass.TriggerServerDiscovery_Parameters request,
                                StreamObserver<GatewayConfigurationServiceOuterClass.TriggerServerDiscovery_Responses> responseObserver) {
        GatewayConfigurationServiceOuterClass.TriggerServerDiscovery_Responses response = GatewayConfigurationServiceOuterClass.TriggerServerDiscovery_Responses.newBuilder().build();

        gatewayServices.getServerManagerService().scanNetwork(4);
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the Servers Property.
     * Gets a list of UUIDS from servers that have been added and are still known to the gateway.
     */
    @Override
    public void getServers(GatewayConfigurationServiceOuterClass.Get_Servers_Parameters request,
                           StreamObserver<GatewayConfigurationServiceOuterClass.Get_Servers_Responses> responseObserver) {

        GatewayConfigurationServiceOuterClass.Get_Servers_Responses.Builder responses = GatewayConfigurationServiceOuterClass.Get_Servers_Responses.newBuilder();

        Collection<UUID> servers = gatewayServices.getServerManagerService().getServers().keySet();
        servers.forEach(server -> responses.addServers(SiLAString.from(server.toString())));

        responseObserver.onNext(responses.build());
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the GetServerInformation Command.
     * Gets information from a server that has been added and is still known to the gateway.
     */
    @Override
    public void getServerInformation(GatewayConfigurationServiceOuterClass.GetServerInformation_Parameters request,
                                     StreamObserver<GatewayConfigurationServiceOuterClass.GetServerInformation_Responses> responseObserver) {
        if(!request.hasServerUUID()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID parameter was not set. Specify a correct UUID."
            ));
            return;
        }

        UUID serverUUID;
        try {
            serverUUID = UUID.fromString(request.getServerUUID().getValue());
        } catch (IllegalArgumentException e) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID is not a valid UUID. Specify a correct UUID"
            ));
            return;
        }
        if(!gatewayServices.getServerManagerService().getServers().containsKey(serverUUID)) {
            responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(
                    "No server with UUID " + serverUUID + " found."
            ));
            return;
        }

        Server server = gatewayServices.getServerManagerService().getServers().get(serverUUID);

        GatewayConfigurationServiceOuterClass.GetServerInformation_Responses response = GatewayConfigurationServiceOuterClass.GetServerInformation_Responses.newBuilder()
                .setServerInformation(
                        GatewayConfigurationServiceOuterClass.GetServerInformation_Responses.ServerInformation_Struct.newBuilder()
                                .setServerName(SiLAString.from(server.getConfiguration().getName()))
                                .setServerType(SiLAString.from(server.getInformation().getType()))
                                .setServerDescription(SiLAString.from(server.getInformation().getDescription()))
                                .setServerVersion(SiLAString.from(server.getInformation().getVersion()))
                                .setServerVendorURL(SiLAString.from(server.getInformation().getVendorURL()))
                                .setHost(SiLAString.from(server.getHost()))
                                .setPort(SiLAString.from(server.getPort().toString()))
                                .setStatus(SiLAString.from(server.getStatus().toString()))
                                .setDateJoined(SiLADate.from(server.getJoined().toInstant().atOffset(ZoneOffset.UTC))))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();

    }

    /**
     * Implementation of the AddServer Command.
     * Adds a server to the gateway. Features of the server are now made available to a client connected to the gateway.
     */
    @Override
    public void addServer(GatewayConfigurationServiceOuterClass.AddServer_Parameters request,
                          StreamObserver<GatewayConfigurationServiceOuterClass.AddServer_Responses> responseObserver) {
        if(!request.hasHost()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "Host",
                    "Host parameter was not set."
            ));
            return;
        } if(!request.hasPort()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "Port",
                    "Port parameter was not set."
            ));
            return;
        }
        int port = 0;
        try {
            port = Integer.parseInt(request.getPort().getValue());
        } catch (NumberFormatException e) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "Port",
                    "Port(as String) conversion to int failed."
            ));
        }

        try {
            gatewayServices.getServerManagerService().addServer(request.getHost().getValue(), port);
        } catch (ServerAdditionException e) {
            responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(e.getMessage()));
        }

        GatewayConfigurationServiceOuterClass.AddServer_Responses response = GatewayConfigurationServiceOuterClass.AddServer_Responses.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the RemoveServer Command.
     * Removes the server from the gateway so that the gateway no longer knows the server.
     * The server with its features can't bee seen and used anymore from clients which are connected to the gateway.
     */
    @Override
    public void removeServer(GatewayConfigurationServiceOuterClass.RemoveServer_Parameters request,
                             StreamObserver<GatewayConfigurationServiceOuterClass.RemoveServer_Responses> responseObserver) {
        if(!request.hasServerUUID()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID parameter was not set. Specify a correct UUID."
            ));
            return;
        }
        UUID serverUUID;
        try {
            serverUUID = UUID.fromString(request.getServerUUID().getValue());
        } catch (IllegalArgumentException e) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID is not a valid UUID. Specify a correct UUID"
            ));
            return;
        }
        gatewayServices.getServerManagerService().removeServer(serverUUID);

        GatewayConfigurationServiceOuterClass.RemoveServer_Responses response = GatewayConfigurationServiceOuterClass.RemoveServer_Responses.newBuilder().build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the TriggerServerDiscovery Command.
     * Gets a list of fully qualified Feature identifiers of all implemented Features of the SiLA Server.
     * The SiLA server must have been added to the gateway.
     */
    @Override
    public void getImplementedFeatures(GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Parameters request,
                                      StreamObserver<GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Responses> responseObserver) {

        UUID serverUUID = checkServerUUID(request, responseObserver);
        if(serverUUID == null) { return; }
        GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Responses.Builder response = GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Responses.newBuilder();

        gatewayServices.getServerManagerService().getServers().get(serverUUID).getFeatures().
                forEach(feature -> response.addImplementedFeatures(SiLAString.from(feature.getIdentifier())));

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    /**
     * Implementation of the GetFeatureDefinition Command.
     * Fet the Feature Definition of an implemented Feature by its fully qualified Feature Identifier.
     * Only Feature Definitions from servers that have been added to the gateway can be retrieved.
     */
    @Override
    public void getFeatureDefinition(GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Parameters request,
                                     StreamObserver<GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Responses> responseObserver) {

        if(!request.hasFeatureIdentifier()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "FeatureIdentifier",
                    "FeatureIdentifier parameter was not set."
            ));
            return;
        }
        String featureIdentifier = request.getFeatureIdentifier().getValue();
        Optional<Feature> foundFeature = gatewayServices.getServerManagerService().getServers().values().stream()
                .flatMap(server -> server.getFeatures().stream())
                .filter(feature -> feature.getIdentifier().equals(featureIdentifier))
                .findFirst();

        if(!foundFeature.isPresent()) {
            responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(
                    "No feature found with featureIdentifier " + featureIdentifier
            ));
            return;
        }

        GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Responses response = GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Responses
                .newBuilder()
                .setFeatureDefinition(SiLAString.from(XMLMarshaller.convertToXML(foundFeature.get())))
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    private UUID checkServerUUID(GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Parameters request,
                                 StreamObserver<GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Responses> responseObserver) {
        UUID serverUUID = null;
        if(!request.hasServerUUID()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID parameter was not set. Specify a correct UUID."
            ));
        }
        try {
            serverUUID = UUID.fromString(request.getServerUUID().getValue());
        } catch (IllegalArgumentException e) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "ServerUUID",
                    "ServerUUID is not a valid UUID. Specify a correct UUID"
            ));
        }
        return serverUUID;
    }
}
