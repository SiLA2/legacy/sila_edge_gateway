package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class CancelPropertySubscription {
    public static final String NAME = "CancelPropertySubscription";
    public static final String TYPE = "<DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><Length>36</Length><Pattern>^$</Pattern></Constraints></Constrained></DataType>";
}