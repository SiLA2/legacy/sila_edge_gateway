package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class BinaryTransferError {
    public static final String NAME = "BinaryTransferError";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>ErrorType</Identifier> <DisplayName>Error Type</DisplayName> <Description>The Error can occur as different Binary Transfer Error Types.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Set> <Value>INVALID_BINARY_TRANSFER_UUID</Value> <Value>BINARY_UPLOAD_FAILED</Value> <Value>BINARY_DOWNLOAD_FAILED</Value> </Set> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>Message</Identifier> <DisplayName>Message</DisplayName> <Description>The Error Message.</Description> <DataType> <Basic>String</Basic> </DataType> </Element> </Structure> </DataType>";
}
