package com.silastandard.gateway.monitoring_configuration.server.monitoring;

import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.MonitoringMessage;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.*;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import com.silastandard.gateway.utils.FeatureIdConverter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.Feature;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides the monitoring messages for the different types of client messages sent through the gateway
 * and notifies the MonitoringSubscriptionService.
 */
@Slf4j
public class MonitoringClientMessageProvider {
    private final MonitoringSubscriptionService monitoringSubscriptionService;
    private final ServerManagerService serverManagerService;

    public MonitoringClientMessageProvider(@NonNull MonitoringSubscriptionService monitoringSubscriptionService, @NonNull ServerManagerService serverManagerService) {
        this.monitoringSubscriptionService = monitoringSubscriptionService;
        this.serverManagerService = serverManagerService;
    }

    public void onCommandExecutionMessage(SiLAGatewayCall gatewayCall, DynamicMessage requestMessage) {
        final Optional<Feature.Command> optionalCommand = getFeatureCommand(gatewayCall);

        if(optionalCommand.isPresent()) {
            GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.Builder commandExecutionStruct =
                    GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct
                            .newBuilder()
                            .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()));

            List<SiLAElement> parameterList = optionalCommand.get().getParameter();
            final Map<String, Descriptors.FieldDescriptor> requestParams = requestMessage
                    .getAllFields()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap((k) -> k.getKey().getName(), Map.Entry::getKey));

                for (final Map.Entry<String, Descriptors.FieldDescriptor> e : requestParams.entrySet()) {
                    String requestParamIdentifier = e.getKey();
                    Optional<SiLAElement> optionalParam = parameterList.stream().filter(p -> p.getIdentifier().equals(requestParamIdentifier)).findAny();
                    if (optionalParam.isPresent()) {
                        SiLAElement foundParam = optionalParam.get();
                        GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct.Builder paramBuilder =
                                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct.newBuilder();
                        final Descriptors.FieldDescriptor fd = e.getValue();
                        paramBuilder.setName(SiLAString.from(fd.getJsonName()));
                        try {
                            Object result = requestMessage.getField(fd);
                            DynamicMessage parameterValue = (DynamicMessage) result;
                            final SiLAFramework.Any anyMessage = SiLAAny.from(foundParam.getDataType(), parameterValue);
                            paramBuilder.setValue(anyMessage);
                        } catch(Exception exception) {
                            log.error("Couldn't cast: {}",  foundParam.getIdentifier());
                        }
                        commandExecutionStruct.addCommandParameters(paramBuilder.build());
                    }
                }

            SiLAFramework.Any message = SiLAAny.from(CommandExecution.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage
                            .newBuilder()
                            .setCommandExecutionMessage(commandExecutionStruct.build())
                            .build());
            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    CommandExecution.NAME,
                    message);
            notifyOnClientMessage(monitoringMessage);
        }
    }

    public void onCommandInitiationMessage(SiLAGatewayCall gatewayCall, DynamicMessage requestMessage) {
        final Optional<Feature.Command> optionalCommand = getFeatureCommand(gatewayCall);

        if(optionalCommand.isPresent()) {
            GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct.Builder commandInitiationStruct =
                    GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct
                            .newBuilder()
                            .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()));

            List<SiLAElement> parameterList = optionalCommand.get().getParameter();
            final Map<String, Descriptors.FieldDescriptor> requestParams = requestMessage
                    .getAllFields()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap((k) -> k.getKey().getName(), Map.Entry::getKey));

            for (final Map.Entry<String, Descriptors.FieldDescriptor> e : requestParams.entrySet()) {
                String requestParamIdentifier = e.getKey();
                Optional<SiLAElement> optionalParam = parameterList.stream().filter(p -> p.getIdentifier().equals(requestParamIdentifier)).findAny();
                if(optionalParam.isPresent()) {
                    SiLAElement foundParam = optionalParam.get();
                    final Descriptors.FieldDescriptor fd = e.getValue();
                    DynamicMessage parameterValue = (DynamicMessage) requestMessage.getField(fd);
                    GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct.CommandParameters_Struct.Builder commandBuilder =
                            GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct.CommandParameters_Struct.newBuilder();
                    commandBuilder.setName(SiLAString.from(fd.getJsonName()));
                    try {
                        final SiLAFramework.Any anyMessage = SiLAAny.from(foundParam.getDataType(), parameterValue) ;
                        commandBuilder.setValue(anyMessage);
                    } catch(Exception exception) {
                        log.error("Couldn't cast: {}",  foundParam.getIdentifier());
                    }
                    commandInitiationStruct.addCommandParameters(commandBuilder.build());
                }
            }
            SiLAFramework.Any message = SiLAAny.from(CommandInitiation.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage
                            .newBuilder()
                            .setCommandInitiationMessage(commandInitiationStruct.build())
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    CommandInitiation.NAME,
                    message);
            notifyOnClientMessage(monitoringMessage);
        }
    }


    public void onCommandExecutionInfoSubscriptionMessage(SiLAGatewayCall gatewayCall, String executionUUID) {
        GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage executionInfoSubscriptionMessage =
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage.newBuilder()
                        .setCommandExecutionInfoSubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage.CommandExecutionInfoSubscriptionMessage_Struct
                                .newBuilder()
                                .setCommandExecutionUUID(SiLAString.from(executionUUID))
                                .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()))
                                .build())
                .build();
        SiLAFramework.Any message = SiLAAny.from(CommandExecutionInfoSubscription.TYPE, executionInfoSubscriptionMessage);

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CommandExecutionInfoSubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }


    public void onCommandIntermediateResponseSubscriptionMessage(SiLAGatewayCall gatewayCall, String executionUUID) {
        GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage commandIntermediateResponseSubscriptionMessage =
                GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage.newBuilder()
                        .setCommandIntermediateResponseSubscriptionMessage(
                                GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage.CommandIntermediateResponseSubscriptionMessage_Struct
                                        .newBuilder()
                                        .setCommandExecutionUUID(SiLAString.from(executionUUID))
                                        .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()))
                                        .build())
                        .build();
        SiLAFramework.Any message = SiLAAny.from(CommandIntermediateResponseSubscription.TYPE, commandIntermediateResponseSubscriptionMessage);

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CommandIntermediateResponseSubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onCommandGetResponseMessage(SiLAGatewayCall gatewayCall, String executionUUID) {
        GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage commandGetResponseMessage =
                GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.newBuilder()
                        .setCommandGetResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.CommandGetResponseMessage_Struct
                                .newBuilder()
                                .setCommandExecutionUUID(SiLAString.from(executionUUID))
                                .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()))
                                .build())
                    .build();

        SiLAFramework.Any message = SiLAAny.from(CommandGetResponse.TYPE, commandGetResponseMessage);
        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CommandGetResponse.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public  void onMetaDataRequestMessage(SiLAGatewayCall gatewayCall, String metadataIdentifier) {

         SiLAFramework.Any message = SiLAAny.from(MetaDataRequest.TYPE, GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage.newBuilder()
                 .setGetFCPAffectedByMetadataRequestMessage(SiLAString.from(metadataIdentifier))
                 .build());

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                MetaDataRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onPropertyReadMessage(SiLAGatewayCall gatewayCall, String fullyQualifiedPropertyId) {
        SiLAFramework.Any message = SiLAAny.from(PropertyRead.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage
                        .newBuilder()
                        .setPropertyReadMessage(SiLAString.from(fullyQualifiedPropertyId))
                        .build());

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                PropertyRead.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onPropertySubscriptionMessage(SiLAGatewayCall gatewayCall, String fullyQualifiedPropertyId) {
        final SiLAFramework.Any message = SiLAAny.from(PropertySubscription.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage
                        .newBuilder()
                        .setPropertySubscriptionMessage(SiLAString.from(fullyQualifiedPropertyId))
                        .build());
        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                PropertySubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onCancelCommandExecutionInfoSubscriptionMessage(SiLAGatewayCall gatewayCall,  String requestUUID) {
        final SiLAFramework.Any message = SiLAAny.from(CancelCommandExecutionInfoSubscription.NAME,
                GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage
                        .newBuilder()
                        .setCancelCommandExecutionInfoSubscriptionMessage(SiLAString.from(requestUUID))
                        .build());

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CancelCommandExecutionInfoSubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onCancelCommandIntermediateResponseSubscriptionMessage(SiLAGatewayCall gatewayCall,  String requestUUID) {
        final SiLAFramework.Any message = SiLAAny.from(CancelCommandIntermediateResponseSubscription.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage
                        .newBuilder()
                        .setCancelCommandIntermediateResponseSubscriptionMessage(SiLAString.from(requestUUID))
                        .build());

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CancelCommandIntermediateResponseSubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onCancelPropertySubscriptionMessage(SiLAGatewayCall gatewayCall, String requestUUID) {
        final SiLAFramework.Any message = SiLAAny.from(CancelPropertySubscription.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage
                        .newBuilder()
                        .setCancelPropertySubscriptionMessage(SiLAString.from(requestUUID))
                        .build());

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CancelPropertySubscription.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onCreateBinaryUploadRequest(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.CreateBinaryRequest createBinaryRequest) {
        SiLAFramework.Any message = SiLAAny.from(CreateBinaryUploadRequest.TYPE,
            GatewayMonitoringServiceOuterClass.DataType_CreateBinaryUploadRequestMessage
                    .newBuilder()
                    .setCreateBinaryUploadRequestMessage(
                            GatewayMonitoringServiceOuterClass.DataType_CreateBinaryUploadRequestMessage.CreateBinaryUploadRequestMessage_Struct
                                    .newBuilder()
                                    .setBinarySize(SiLAInteger.from(createBinaryRequest.getBinarySize()))
                                    .setChunkCount(SiLAInteger.from(createBinaryRequest.getChunkCount()))
                                    .setParameterIdentifier(SiLAString.from(createBinaryRequest.getParameterIdentifier()))
                                    .build())
                    .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CreateBinaryUploadRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onDeleteUploadedBinaryRequest(SiLAGatewayCall gatewayCall,SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        SiLAFramework.Any message = SiLAAny.from(DeleteUploadedBinaryRequest.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryRequestMessage
                        .newBuilder()
                        .setDeleteBinaryRequestMessage(SiLAString.from(deleteBinaryRequest.getBinaryTransferUUID()))
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                DeleteUploadedBinaryRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onDeleteDownloadedBinaryRequest(SiLAGatewayCall gatewayCall,SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        SiLAFramework.Any message = SiLAAny.from(DeleteDownloadedBinaryRequest.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryRequestMessage
                        .newBuilder()
                        .setDeleteBinaryRequestMessage(SiLAString.from(deleteBinaryRequest.getBinaryTransferUUID()))
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                DeleteDownloadedBinaryRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onGetBinaryInfoRequest(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.GetBinaryInfoRequest getBinaryInfoRequest) {
        SiLAFramework.Any message = SiLAAny.from(GetBinaryInfoRequest.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoRequestMessage
                        .newBuilder()
                        .setGetBinaryInfoRequestMessage(SiLAString.from(getBinaryInfoRequest.getBinaryTransferUUID()))
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                GetBinaryInfoRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onGetChunkRequest( SiLAGatewayCall gatewayCall, SiLABinaryTransfer.GetChunkRequest getChunkRequest)  {
        SiLAFramework.Any message = SiLAAny.from(GetChunkRequest.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_GetChunkRequestMessage
                    .newBuilder()
                    .setGetChunkRequestMessage(GatewayMonitoringServiceOuterClass.DataType_GetChunkRequestMessage.GetChunkRequestMessage_Struct.newBuilder()
                            .setBinaryTransferUUID(SiLAString.from(getChunkRequest.getBinaryTransferUUID()))
                            .setLength(SiLAInteger.from(getChunkRequest.getLength()))
                            .setOffset(SiLAInteger.from(getChunkRequest.getOffset()))
                            .build())
                    .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                GetChunkRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    public void onUploadChunkRequest(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.UploadChunkRequest uploadChunkRequest) {
        SiLAFramework.Any message = SiLAAny.from(UploadChunkRequest.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_UploadChunkRequestMessage
                        .newBuilder()
                        .setUploadChunkRequestMessage(GatewayMonitoringServiceOuterClass.DataType_UploadChunkRequestMessage.UploadChunkRequestMessage_Struct.newBuilder()
                                .setBinaryTransferUUID(SiLAString.from(uploadChunkRequest.getBinaryTransferUUID()))
                                .setChunkIndex(SiLAInteger.from(uploadChunkRequest.getChunkIndex()))
                                .setPayload(SiLABinary.fromBytes(uploadChunkRequest.getPayload()))
                                .build())
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                UploadChunkRequest.NAME,
                message);
        notifyOnClientMessage(monitoringMessage);
    }

    private Optional<Feature.Command> getFeatureCommand(SiLAGatewayCall gatewayCall) {
        String featureId = FeatureIdConverter.getFeatureID(gatewayCall.getFullyQualifiedFeatureId());
        Optional<Feature> feature = serverManagerService.getServer(gatewayCall.getServerUUID()).getFeatures().stream()
                .filter(s -> s.getIdentifier().equals(featureId)).findAny();
        Optional<Feature.Command> optionalCommand = Optional.empty();
        if(feature.isPresent()) {
            optionalCommand = feature.get().getCommand()
                    .stream()
                    .filter(c -> gatewayCall.getCallId().equals(c.getIdentifier()))
                    .findAny();
        }
        return optionalCommand;
    }

    private void notifyOnClientMessage(MonitoringMessage monitoringMessage) {
        monitoringSubscriptionService.notifyListenerOnClientMessage(monitoringMessage);
    }
}
