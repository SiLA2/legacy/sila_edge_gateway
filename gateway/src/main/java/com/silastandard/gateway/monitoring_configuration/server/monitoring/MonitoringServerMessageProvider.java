package com.silastandard.gateway.monitoring_configuration.server.monitoring;

import com.google.protobuf.*;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.MonitoringMessage;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types.*;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import com.silastandard.gateway.utils.FeatureIdConverter;
import io.grpc.StatusRuntimeException;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.Feature;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.sila_base.EmptyClass;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Optional;

import static sila_java.library.core.utils.FileUtils.getFileContent;

/**
 * Provides the monitoring messages for the different types of server messages sent through the gateway
 * and notifies the MonitoringSubscriptionService.
 */
@Slf4j
public class MonitoringServerMessageProvider {
    private final MonitoringSubscriptionService monitoringSubscriptionService;
    private final ServerManagerService serverManagerService;

    public MonitoringServerMessageProvider(@NonNull MonitoringSubscriptionService monitoringSubscriptionService, @NonNull ServerManagerService serverManagerService) {
        this.monitoringSubscriptionService = monitoringSubscriptionService;
        this.serverManagerService = serverManagerService;
    }

    public void onCommandResponseMessage(SiLAGatewayCall gatewayCall, DynamicMessage responseMessage) {
        final Optional<Feature.Command> optionalCommand = getFeatureCommand(gatewayCall);
        if(optionalCommand.isPresent()) {
            List<SiLAElement> listResponses = optionalCommand.get().getResponse();
            GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct.Builder commandResponseStruct =
                    GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct.newBuilder()
                            .setCommandIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()));

            responseMessage.getAllFields().forEach((key, value) -> {
                Optional<SiLAElement> siLAElement = listResponses.stream().filter(r -> r.getIdentifier().equals(key.getJsonName())).findAny();
                if(siLAElement.isPresent()) {
                    DynamicMessage dynamicMessage = DynamicMessage.newBuilder(responseMessage.getDescriptorForType()).build();
                    try {
                        dynamicMessage = (DynamicMessage) responseMessage.getField(key);
                    } catch (Exception e) {
                        log.error("Couldn't retrieve command response for {}.", gatewayCall.getFullyQualifiedFeatureId());
                    }
                    commandResponseStruct.addCommandResponse(GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct.CommandResponse_Struct.newBuilder()
                            .setIdentifier(SiLAString.from(key.getJsonName()))
                            .setValue(SiLAAny.from(siLAElement.get().getDataType(), dynamicMessage))
                            .build());
                }
            });

            final SiLAFramework.Any message = SiLAAny.from(CommandResponse.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage
                            .newBuilder()
                            .setCommandResponseMessage(commandResponseStruct.build())
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    CommandResponse.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onObservableCommandConfirmationMessage(SiLAGatewayCall gatewayCall, ByteString byteResponse)  {
        try {
            Parser<SiLAFramework.CommandConfirmation> parser = SiLAFramework.CommandConfirmation.parser();
            SiLAFramework.CommandConfirmation commandConfirmation = parser.parseFrom(byteResponse);

            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage.ObservableCommandConfirmationMessage_Struct struct =
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage.ObservableCommandConfirmationMessage_Struct.newBuilder()
                    .setCommandExecutionUUID(SiLAString.from(commandConfirmation.getCommandExecutionUUID().getValue()))
                    .setLifetimeOfExecution(GatewayMonitoringServiceOuterClass.DataType_Duration
                            .newBuilder()
                            .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                    .setSeconds(SiLAInteger.from(commandConfirmation.getLifetimeOfExecution().getSeconds()))
                                    .setNanos(SiLAInteger.from(commandConfirmation.getLifetimeOfExecution().getNanos()))
                                    .build())
                            .build())
                .build();

            final SiLAFramework.Any message = SiLAAny.from(ObservableCommandConfirmation.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage
                            .newBuilder()
                            .setObservableCommandConfirmationMessage(struct)
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    ObservableCommandConfirmation.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        } catch (InvalidProtocolBufferException e) {
            log.error("Couldn't parse CommandConfirmation");
        }
    }

    public void onObservableCommandExecutionInfoMessage(SiLAGatewayCall gatewayCall, String executionUUID, DynamicMessage responseMessage) {
        try {
            Parser<SiLAFramework.ExecutionInfo> parser = SiLAFramework.ExecutionInfo.parser();
            SiLAFramework.ExecutionInfo executionInfo = parser.parseFrom(responseMessage.toByteString());

            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.ObservableCommandExecutionInfoMessage_Struct struct =
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.ObservableCommandExecutionInfoMessage_Struct.newBuilder()
                            .setCommandExecutionUUID(SiLAString.from(executionUUID))
                            .setExecutionInfo(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.ObservableCommandExecutionInfoMessage_Struct.ExecutionInfo_Struct.newBuilder()
                                    .setCommandExecutionStatus(SiLAString.from(executionInfo.getCommandStatus().name()))
                                    .setEstimatedRemainingTime(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder().
                                            setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                    .setSeconds(SiLAInteger.from(executionInfo.getEstimatedRemainingTime().getSeconds()))
                                                    .setNanos(SiLAInteger.from(executionInfo.getEstimatedRemainingTime().getNanos()))
                                            ).build())
                                    .setProgressInfo(executionInfo.getProgressInfo())
                                    .setUpdatedLifetimeOfExecution(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                            .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                    .setSeconds(SiLAInteger.from(executionInfo.getUpdatedLifetimeOfExecution().getSeconds()))
                                                    .setNanos(SiLAInteger.from(executionInfo.getUpdatedLifetimeOfExecution().getNanos()))
                                            .build())
                                    .build())
                            .build())
                    .build();

            final SiLAFramework.Any message = SiLAAny.from(ObservableCommandExecutionInfo.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage
                            .newBuilder()
                            .setObservableCommandExecutionInfoMessage(struct)
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    ObservableCommandExecutionInfo.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        } catch (InvalidProtocolBufferException e) {
            log.error("Couldn't parse CommandConfirmation");
        }
    }

    public void onObservableCommandIntermediateResponseMessage(SiLAGatewayCall gatewayCall, String executionUUID, DynamicMessage responseMessage) {
        GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.ObservableCommandIntermediateResponseMessage_Struct.Builder struct =
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.ObservableCommandIntermediateResponseMessage_Struct.newBuilder()
                        .setCommandExecutionUUID(SiLAString.from(executionUUID));
        final Optional<Feature.Command> optionalCommand = getFeatureCommand(gatewayCall);
        if(optionalCommand.isPresent()) {
            List<SiLAElement> intermediateResponse = optionalCommand.get().getIntermediateResponse();
            responseMessage.getAllFields().forEach((key, value) -> {
                Optional<SiLAElement> siLAElement = intermediateResponse.stream().filter(r -> r.getIdentifier().equals(key.getJsonName())).findAny();

                if(siLAElement.isPresent()) {
                    DynamicMessage dynamicMessage = DynamicMessage.newBuilder(responseMessage.getDescriptorForType()).build();
                    try {
                        dynamicMessage = (DynamicMessage) responseMessage.getField(key);
                    } catch (Exception e) {
                        log.error("Couldn't retrieve command response for {}.", gatewayCall.getFullyQualifiedFeatureId());
                    }

                    struct.addResult(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.ObservableCommandIntermediateResponseMessage_Struct.Result_Struct.newBuilder()
                            .setIdentifier(SiLAString.from(key.getJsonName()))
                            .setValue(SiLAAny.from(siLAElement.get().getDataType(), dynamicMessage))
                            .build());
                }
            });

            final SiLAFramework.Any message = SiLAAny.from(ObservableCommandIntermediateResponse.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage
                    .newBuilder()
                    .setObservableCommandIntermediateResponseMessage(struct.build())
                    .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    ObservableCommandIntermediateResponse.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onObservableCommandResponseMessage(SiLAGatewayCall gatewayCall, String executionUUID, DynamicMessage responseMessage) {
        GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.ObservableCommandResponseMessage_Struct.Builder struct =
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.ObservableCommandResponseMessage_Struct.newBuilder()
                    .setCommandExecutionUUID(SiLAString.from(executionUUID));

        final Optional<Feature.Command> optionalCommand = getFeatureCommand(gatewayCall);
        if(optionalCommand.isPresent()) {
            List<SiLAElement> responses = optionalCommand.get().getResponse();
            responseMessage.getAllFields().forEach((key, value) -> {
                Optional<SiLAElement> siLAElement = responses.stream().filter(r -> r.getIdentifier().equals(key.getJsonName())).findAny();

                if(siLAElement.isPresent()) {
                    DynamicMessage dynamicMessage = DynamicMessage.newBuilder(responseMessage.getDescriptorForType()).build();
                    try {
                        dynamicMessage = (DynamicMessage) responseMessage.getField(key);
                    } catch (Exception e) {
                        log.error("Couldn't retrieve command response for {}.", gatewayCall.getFullyQualifiedFeatureId());
                    }

                    struct.addResult(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.ObservableCommandResponseMessage_Struct.Result_Struct.newBuilder()
                            .setIdentifier(SiLAString.from(key.getJsonName()))
                            .setValue(SiLAAny.from(siLAElement.get().getDataType(), dynamicMessage))
                            .build());
                }
            });

            final SiLAFramework.Any message = SiLAAny.from(ObservableCommandIntermediateResponse.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage
                            .newBuilder()
                            .setObservableCommandResponseMessage(struct.build())
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    ObservableCommandResponse.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onMetaDataResponseMessage(SiLAGatewayCall gatewayCall, DynamicMessage responseMessage) {
        try {
            Parser<GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage> parser = GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage.parser();
            GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage response = parser.parseFrom(responseMessage.toByteString());
            SiLAFramework.Any message = SiLAAny.from(GetFCPAffectedByMetadataResponse.TYPE, response);

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    GetFCPAffectedByMetadataResponse.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        } catch (InvalidProtocolBufferException e) {
            log.info("Response couldn't be parsed into GetFCPAffectedByMetadataResponse");
        }
    }

    public void onPropertyValueMessage(SiLAGatewayCall gatewayCall, DynamicMessage propertyValueByteString) {
        final Optional<Feature.Property> featureProperty = getFeatureProperty(gatewayCall);

        if (featureProperty.isPresent()) {
            GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage.PropertyValueMessage_Struct.Builder struct =
                    GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage.PropertyValueMessage_Struct.newBuilder()
                            .setPropertyIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()));

            try {
                // PropertyValue with basic Datatype
                propertyValueByteString.getAllFields().forEach((key, value) -> {
                    struct.setPropertyValue(SiLAAny.from(featureProperty.get().getDataType(), (DynamicMessage) value));
                });
            } catch(Exception e) {
                // If fails: Complex Datatype
                struct.setPropertyValue(SiLAAny.from(featureProperty.get().getDataType(), propertyValueByteString));
            }

            SiLAFramework.Any message = SiLAAny.from(PropertyValue.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage
                            .newBuilder()
                            .setPropertyValueMessage(struct.build())
                            .build());
            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    PropertyValue.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onObservablePropertyValueMessage(SiLAGatewayCall gatewayCall, DynamicMessage propertyValueByteString) {
        final Optional<Feature.Property> featureProperty = getFeatureProperty(gatewayCall);
        if (featureProperty.isPresent()) {
            GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage.ObservablePropertyValueMessage_Struct.Builder struct =
                    GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage.ObservablePropertyValueMessage_Struct.newBuilder()
                        .setPropertyIdentifier(SiLAString.from(gatewayCall.getFullyQualifiedCallId()));

            try {
                // PropertyValue with basic Datatype
                propertyValueByteString.getAllFields().forEach((key, value) -> {
                    struct.setPropertyValue(SiLAAny.from(featureProperty.get().getDataType(), (DynamicMessage) propertyValueByteString.getField(key)));
            });
            } catch(Exception e) {
                // If fails: Complex Datatype
                struct.setPropertyValue(SiLAAny.from(featureProperty.get().getDataType(), propertyValueByteString));
            }
            SiLAFramework.Any message = SiLAAny.from(PropertyValue.TYPE,
                    GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage
                            .newBuilder()
                            .setObservablePropertyValueMessage(struct.build())
                            .build());

            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    ObservablePropertyValue.NAME,
                    message);
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onErrorMessage(SiLAGatewayCall gatewayCall, StatusRuntimeException exception) {
        Optional<SiLAFramework.SiLAError> optionalError = SiLAErrors.retrieveSiLAError(exception);

        Optional<SiLAFramework.Any> anyError = Optional.empty();
        String messageType = "";
        if (optionalError.isPresent()) {
            SiLAFramework.SiLAError error = optionalError.get();

            if (error.hasDefinedExecutionError()) {
                messageType = ErrorResponse.DEFINED_ERROR_NAME;
                SiLAFramework.DefinedExecutionError definedExecutionError = error.getDefinedExecutionError();
                GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage definedExecutionErrorMonitoring = GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.newBuilder()
                        .setDefinedExecutionErrorMessage(GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.DefinedExecutionErrorMessage_Struct.newBuilder()
                                .setErrorIdentifier(SiLAString.from(definedExecutionError.getErrorIdentifier()))
                                .setMessage(SiLAString.from(definedExecutionError.getMessage()))
                                .build())
                        .build();
                try {
                    DynamicMessage dm = DynamicMessage.parseFrom(GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.getDescriptor(), definedExecutionErrorMonitoring.toByteString());
                    anyError = Optional.of(SiLAAny.from(ErrorResponse.DEFINED_ERROR_TYPE, dm));
                } catch (InvalidProtocolBufferException e) {
                    log.error("DataType_DefinedExecutionError couldn't be parsed into DynamicMessage");
                }
            } else if (error.hasFrameworkError()) {
                messageType = ErrorResponse.FRAMEWORK_ERROR_NAME;
                SiLAFramework.FrameworkError frameworkError = error.getFrameworkError();
                GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage frameworkErrorMonitoring = GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage.newBuilder()
                        .setFrameworkErrorMessage(GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage.FrameworkErrorMessage_Struct.newBuilder()
                                .setErrorType(SiLAString.from(frameworkError.getErrorType().name()))
                                .setMessage(SiLAString.from(frameworkError.getMessage()))
                                .build())
                        .build();
                try {
                    DynamicMessage dm = DynamicMessage.parseFrom(GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage.getDescriptor(), frameworkErrorMonitoring.toByteString());
                    anyError = Optional.of(SiLAAny.from(ErrorResponse.FRAMEWORK_ERROR_TYPE, dm));
                } catch (InvalidProtocolBufferException e) {
                    log.error("DataType_FrameworkError couldn't be parsed into DynamicMessage");
                }
            } else if (error.hasUndefinedExecutionError()) {
                messageType = ErrorResponse.UNDEFINED_ERROR_NAME;
                SiLAFramework.UndefinedExecutionError undefinedExecutionError = error.getUndefinedExecutionError();
                GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage undefinedExecutionErrorMonitoring = GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage.newBuilder()
                        .setUndefinedExecutionErrorMessage(SiLAString.from(undefinedExecutionError.getMessage()))
                        .build();
                try {
                    DynamicMessage dm = DynamicMessage.parseFrom(GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage.getDescriptor(), undefinedExecutionErrorMonitoring.toByteString());
                    anyError = Optional.of(SiLAAny.from(ErrorResponse.UNDEFINED_ERROR_TYPE, dm));
                } catch (InvalidProtocolBufferException e) {
                    log.error("DataType_UndefinedExecutionError couldn't be parsed into DynamicMessage");
                }

            } else if (error.hasValidationError()) {
                messageType = ErrorResponse.VALIDATION_ERROR_NAME;
                SiLAFramework.ValidationError validationError = error.getValidationError();
                GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage validationErrorMonitoring = GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage.newBuilder()
                        .setValidationErrorMessage(GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage.ValidationErrorMessage_Struct.newBuilder()
                                .setParameter(SiLAString.from(validationError.getParameter()))
                                .setMessage(SiLAString.from(validationError.getMessage()))
                                .build())
                        .build();
                try {
                    DynamicMessage dm = DynamicMessage.parseFrom(GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage.getDescriptor(), validationErrorMonitoring.toByteString());
                    anyError = Optional.of(SiLAAny.from(ErrorResponse.VALIDATION_ERROR_TYPE, dm));
                } catch (InvalidProtocolBufferException e) {
                    log.info("DataType_ValidationError couldn't be parsed into DynamicMessage");
                }
            }
        } else {
            messageType = ErrorResponse.CONNECTION_ERROR_NAME;
            GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage connectionError = GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage.newBuilder()
                    .setConnectionErrorMessage(SiLAString.from(exception.getMessage()))
                    .build();
            try {
                DynamicMessage dm = DynamicMessage.parseFrom(GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage.getDescriptor(), connectionError.toByteString());
                anyError = Optional.of(SiLAAny.from(ErrorResponse.CONNECTION_ERROR_TYPE, dm));
            } catch (InvalidProtocolBufferException e) {
                log.info("DataType_ConnectionError couldn't be parsed into DynamicMessage");
            }
        }

        if (anyError.isPresent()) {
            MonitoringMessage monitoringMessage = new MonitoringMessage(
                    gatewayCall.getServerUUID().toString(),
                    gatewayCall.getClientNameId(),
                    messageType,
                    anyError.get());
            notifyOnServerMessage(monitoringMessage);
        }
    }

    public void onBinaryTransferError(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.BinaryTransferError binaryTransferError) {
        SiLAFramework.Any message = SiLAAny.from(BinaryTransferError.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage.newBuilder()
                        .setBinaryTransferErrorMessage(GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage.BinaryTransferErrorMessage_Struct.newBuilder()
                                .setErrorType(SiLAString.from(binaryTransferError.getErrorType().name()))
                                .setMessage(SiLAString.from(binaryTransferError.getMessage()))
                                .build())
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                BinaryTransferError.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    public void onCreateBinaryResponse(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse) {
        SiLAFramework.Any message = SiLAAny.from(CreateBinaryResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CreateBinaryResponseMessage
                        .newBuilder()
                        .setCreateBinaryResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CreateBinaryResponseMessage.CreateBinaryResponseMessage_Struct.newBuilder()
                                .setBinaryTransferUUID(SiLAString.from(createBinaryResponse.getBinaryTransferUUID()))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(createBinaryResponse.getLifetimeOfBinary().getSeconds()))
                                                .setNanos(SiLAInteger.from(createBinaryResponse.getLifetimeOfBinary().getNanos()))
                                                .build())
                                        .build())
                                .build())
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                CreateBinaryResponse.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    public void onDeleteBinaryResponse(SiLAGatewayCall gatewayCall) {
        SiLAFramework.Any message = SiLAAny.from(DeleteBinaryResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryResponseMessage
                        .newBuilder()
                        .setDeleteBinaryResponseMessage(SiLAString.from(""))
                        .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                DeleteBinaryResponse.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    public void onGetBinaryInfoResponse(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.GetBinaryInfoResponse getBinaryInfoResponse) {
        SiLAFramework.Any message = SiLAAny.from(GetBinaryInfoResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage.newBuilder()
                        .setGetBinaryInfoResponseMessage(GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage.GetBinaryInfoResponseMessage_Struct.newBuilder()
                                .setBinarySize(SiLAInteger.from(getBinaryInfoResponse.getBinarySize()))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(getBinaryInfoResponse.getLifetimeOfBinary().getSeconds()))
                                                .setNanos(SiLAInteger.from(getBinaryInfoResponse.getLifetimeOfBinary().getNanos()))
                                                .build())
                                        .build())
                                .build())
                .build()
        );

        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                GetBinaryInfoResponse.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    public void onGetChunkResponse(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.GetChunkResponse getChunkResponse) {
        SiLAFramework.Any message = SiLAAny.from(GetChunkResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage.newBuilder()
                        .setGetChunkResponseMessage(GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage.GetChunkResponseMessage_Struct.newBuilder()
                                .setBinaryTransferUUID(SiLAString.from(getChunkResponse.getBinaryTransferUUID()))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(getChunkResponse.getLifetimeOfBinary().getSeconds()))
                                                .setNanos(SiLAInteger.from(getChunkResponse.getLifetimeOfBinary().getNanos()))
                                                .build())
                                        .build())
                                .setOffset(SiLAInteger.from(getChunkResponse.getOffset()))
                                .setPayload(SiLABinary.fromBytes(getChunkResponse.getPayload()))
                                .build())
                .build()
        );
        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                GetChunkResponse.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    public void onUploadChunkResponse(SiLAGatewayCall gatewayCall, SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse) {
        SiLAFramework.Any message = SiLAAny.from(UploadChunkResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_UploadChunkResponseMessage.newBuilder()
                        .setUploadChunkResponseMessage(GatewayMonitoringServiceOuterClass.DataType_UploadChunkResponseMessage.UploadChunkResponseMessage_Struct.newBuilder()
                                .setBinaryTransferUUID(SiLAString.from(uploadChunkResponse.getBinaryTransferUUID()))
                                .setChunkIndex(SiLAInteger.from(uploadChunkResponse.getChunkIndex()))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(uploadChunkResponse.getLifetimeOfBinary().getSeconds()))
                                                .setNanos(SiLAInteger.from(uploadChunkResponse.getLifetimeOfBinary().getNanos()))
                                                .build())
                                        .build())
                                .build())
                        .build()
        );
        MonitoringMessage monitoringMessage = new MonitoringMessage(
                gatewayCall.getServerUUID().toString(),
                gatewayCall.getClientNameId(),
                UploadChunkResponse.NAME,
                message);
        notifyOnServerMessage(monitoringMessage);
    }

    private void notifyOnServerMessage(MonitoringMessage monitoringMessage) {
         monitoringSubscriptionService.notifyListenerOnServerMessage(monitoringMessage);
     }

    private Optional<Feature.Command> getFeatureCommand(SiLAGatewayCall gatewayCall) {
        Optional<Feature> feature = getFeature(gatewayCall);
        return feature.flatMap(value -> value.getCommand()
                .stream()
                .filter(c -> gatewayCall.getCallId().equals(c.getIdentifier()))
                .findAny());
    }

    private Optional<Feature.Property> getFeatureProperty(SiLAGatewayCall gatewayCall) {
        Optional<Feature> feature = getFeature(gatewayCall);

        return feature.flatMap(value -> value.getProperty()
                .stream()
                .filter(c -> gatewayCall.getCallId().equals(c.getIdentifier()))
                .findAny());
    }

    @SneakyThrows
    private Optional<Feature> getFeature(SiLAGatewayCall siLACall) {
        String featureId = FeatureIdConverter.getFeatureID(siLACall.getFullyQualifiedFeatureId());
        if(featureId.equals("SiLAService")) {
            final Feature feature = FeatureGenerator.generateFeature(getFileContent(
                    EmptyClass.class.getResourceAsStream(
                            "/sila_base/feature_definitions/org/silastandard/core/SiLAService.sila.xml")
            ));
            return Optional.of(feature);
        } else {
            return serverManagerService.getServer(siLACall.getServerUUID()).getFeatures().stream()
                    .filter(s -> s.getIdentifier().equals(featureId)).findAny();
        }
    }

    public static byte[] serialize(Object obj) throws IOException {
        try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
            try(ObjectOutputStream o = new ObjectOutputStream(b)){
                o.writeObject(obj);
            }
            return b.toByteArray();
        }
    }
}
