package com.silastandard.gateway.monitoring_configuration.server.monitoring.models;

import lombok.Getter;
import lombok.NonNull;
import sila2.org.silastandard.SiLAFramework;

/**
 * Represents a generic Monitoring Message.
 */
@Getter
public final class MonitoringMessage {
    private final String serverUUID;
    private final String clientName;
    private final String messageType;
    private final SiLAFramework.Any message;

    public MonitoringMessage(@NonNull String serverUUID, @NonNull String clientName, @NonNull String messageType, @NonNull SiLAFramework.Any message) {
        this.serverUUID = serverUUID;
        this.clientName = clientName;
        this.messageType = messageType;
        this.message = message;
    }
}
