package com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client;

import com.silastandard.gateway.client_side.server.GatewayServer;
import com.silastandard.gateway.GatewayServices;
import io.grpc.ConnectivityState;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.connection_configuration.SiLAClientDAO;
import sila_java.library.cloud.connection_configuration.model.ConnectionConfiguration;
import sila_java.library.cloud.connection_configuration.model.SiLAClientInformation;
import sila_java.library.manager.ServerListener;
import sila_java.library.manager.models.Server;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Manages the connection of the client.
 */
@Slf4j
public class ConnectionManager {
    private final SiLAClientDAO siLAClientDAO;
    private final GatewayServices gatewayServices;

    private final Map<String, GatewayServer> servers = new ConcurrentHashMap<>();
    private ServerListener serverListener;

    private final Object lock = new Object();

    public ConnectionManager(GatewayServices gatewayServices) {
        this.siLAClientDAO = new SiLAClientDAO();
        this.gatewayServices = gatewayServices;
    }

    /**
     * Connects a SiLA Client to the Gateway
     */
    public void connect(SiLAClientInformation clientInformation) throws ConnectionException {
        synchronized(lock) {
            if(!servers.isEmpty()) {
                throw new ConnectionException(ConnectionException.ExceptionStatus.CLIENT_ALREADY_CONNECTED,
                        "There is already a client connected to the gateway. First disconnect the connection to the current client.");
            }
            GatewayServer gatewayServer = new GatewayServer.Builder()
                    .withHost(clientInformation.getSiLAClientHost())
                    .withPort(clientInformation.getSiLAClientPort())
                    .build();
            ConnectivityState gatewayState = gatewayServer.getState();

            if (gatewayState == ConnectivityState.READY) {
                deletePersistedClients();
                siLAClientDAO.add(clientInformation);

                gatewayServer.addConnectionErrorObservers(() -> {
                    closeAndRemoveGatewayServers();
                    removeSiLAServerListener();
                });

                servers.put(clientInformation.getClientName(), gatewayServer);


                gatewayServices.getServerManagerService().getServers().forEach((key, value) -> {
                    log.info("ConnectionManager: add Endpoint - Server: " + key);
                    addCloudEndpointService(key, clientInformation.getClientName());
                });

                serverListener = newServerListener(clientInformation.getClientName());
                gatewayServices.getServerManagerService().addServerListener(serverListener);
                log.info("Connected Client: {}", clientInformation.getClientName());
            } else {
                gatewayServer.stop();
                throw new ConnectionException(ConnectionException.ExceptionStatus.CLIENT_OFFLINE,
                        "Couldn't connect to " + clientInformation.getClientName() + ". Client is Offline or does not use secure connection(TLS).");
            }
        }
    }

    /**
     * Disconnects a SiLA Client from the Gateway
     */
    public void disconnect(SiLAClientInformation clientInformation) throws ConnectionException {
        synchronized(lock) {
            GatewayServer server = servers.remove(clientInformation.getClientName());
            if(server == null) {
                throw new ConnectionException(ConnectionException.ExceptionStatus.CLIENT_NOT_CONNECTED,
                        clientInformation.getClientName() + " is currently not connected to the gateway.");
            }
            log.info("Disconnect Client: {}", clientInformation.getClientName());
            server.stop();
            closeAndRemoveGatewayServers();
            removeSiLAServerListener();

            if(!clientInformation.isPersist()) {
                siLAClientDAO.delete(clientInformation.getClientName());
            }
        }
    }

    /**
     * Enables/Disables the Server Initiated Connection Mode by controlling the process of saving the updated configuration.
     */
    public void enableServerInitiatedConnectionMode(boolean enable) {
        ConnectionConfiguration configuration = siLAClientDAO.getConfiguration();
        configuration.setServerInitiatedModeEnabled(enable);
        siLAClientDAO.save(configuration);
    }

    /**
     * Gets the configured SiLA Clients.
     */
    public List<SiLAClientInformation> getSiLAClientList() {
        return siLAClientDAO.get();
    }

    /**
     * Gets status of whether or not the server initiated connection mode is enabled.
     */
    public boolean isServerInitiatedModeActive() {
        return siLAClientDAO.getConfiguration().isServerInitiatedModeEnabled();
    }

    private void addCloudEndpointService(@NonNull UUID silaServerUUID, @NonNull String clientName) {
        GatewayServer gatewayServer = servers.get(clientName);
        if(gatewayServer != null) {
            gatewayServer.addAndStartCloudEndpointService(silaServerUUID, clientName, gatewayServices);
        } else {
            log.info("ConnectionManager: addCloudEndpointService - GatewayServer = null - not connected to client yet - server: " + silaServerUUID);
        }
    }

    private void removeCloudEndpointService(@NonNull UUID silaServerUUID) {
        servers.forEach((k,v) -> v.removeAndStopEndpointService(silaServerUUID));
    }

    private ServerListener newServerListener(String clientName) {
        return new ServerListener() {
            @Override
            public void onServerAdded(UUID uuid, Server server) {
                log.info("Listener: add Endpoint - Server: " + uuid);
                addCloudEndpointService(uuid, clientName);
            }

            @Override
            public void onServerRemoved(UUID uuid, Server server) {
                log.info("Listener: remove Endpoint - Server: " + uuid);
                removeCloudEndpointService(uuid);
            }
        };
    }

    private void deletePersistedClients() {
        List<String> persistedClientNames = siLAClientDAO.getConfiguration().getClientInformation().values().stream().map(SiLAClientInformation::getClientName).collect(Collectors.toList());
        persistedClientNames.forEach(siLAClientDAO::delete);
    }

    private void removeSiLAServerListener() {
        if(serverListener != null) {
            gatewayServices.getServerManagerService().removeServerListener(serverListener);
            serverListener = null;
        }
    }

    public void closeAndRemoveGatewayServers() {
        servers.forEach((k,v) -> v.stop());
        servers.clear();
    }
}

