package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class UploadChunkResponse {
    public static final String NAME = "UploadChunkResponse";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>BinaryTransferUUID</Identifier> <DisplayName>Binary Transfer UUID</DisplayName> <Description>Binary Transfer UUID refers to specific binary data.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}</Pattern> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>ChunkIndex</Identifier> <DisplayName>Chunk Index</DisplayName> <Description>The Binary Chunk Index.</Description> <DataType> <Basic>Integer</Basic> </DataType> </Element> <Element> <Identifier>LifetimeOfBinary</Identifier> <DisplayName>Lifetime Of Binary</DisplayName> <Description>Duration during which a Binary Transfer UUID is valid.</Description> <DataType> <DataTypeIdentifier>Duration</DataTypeIdentifier> </DataType> </Element> </Structure> </DataType>";
}
