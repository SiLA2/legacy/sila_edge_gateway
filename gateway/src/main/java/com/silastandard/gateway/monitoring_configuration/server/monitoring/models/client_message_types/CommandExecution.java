package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class CommandExecution {
    public static final String NAME = "CommandExecution";
    public static final String TYPE = "<DataType><Structure><Element><Identifier>CommandIdentifier</Identifier> <DisplayName>Command Identifier</DisplayName> <Description>The fully qualified identifier of the command.</Description> <DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><FullyQualifiedIdentifier>CommandIdentifier</FullyQualifiedIdentifier> </Constraints></Constrained></DataType></Element><Element><Identifier>CommandParameter</Identifier> <DisplayName>Command Parameter</DisplayName> <Description>The Parameter of the CommandExecution message.</Description> <DataType><List><DataType><Basic>Any</Basic></DataType></List></DataType></Element></Structure></DataType>";
}
