package com.silastandard.gateway.monitoring_configuration.server.monitoring;

import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.MonitoringMessage;
import lombok.NonNull;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The Monitoring Subscription Service notifies subscribers for various servers about client or server messages.
 */
public class MonitoringSubscriptionService {
    private final Map<String, List<MonitoringMessageListener>> messageListenerMap = new ConcurrentHashMap<>();

    /**
     * Notifies the observers on a client message.
     */
    public void notifyListenerOnClientMessage(MonitoringMessage monitoringMessage) {

        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses intermediateResponse =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.newBuilder()
                        .setClientRequestMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct
                                .newBuilder()
                                .setSiLAClient(SiLAString.from(monitoringMessage.getClientName()))
                                .setSiLAServer(SiLAString.from(monitoringMessage.getServerUUID()))
                                .setMessageType(SiLAString.from(monitoringMessage.getMessageType()))
                                .setMessage(monitoringMessage.getMessage())
                                .build()).build();

        if(messageListenerMap.containsKey(monitoringMessage.getServerUUID())) {
            List<MonitoringMessageListener> messageListenerList= messageListenerMap.get(monitoringMessage.getServerUUID());
            messageListenerList.forEach(m -> m.onMessage(intermediateResponse));
        }
    }

    /**
     * Notifies the observers on a server message.
     */
    public void notifyListenerOnServerMessage(MonitoringMessage monitoringMessage) {
        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses intermediateResponse =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.newBuilder()
                        .setServerResponseMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct
                                .newBuilder()
                                .setSiLAClient(SiLAString.from(monitoringMessage.getClientName()))
                                .setSiLAServer(SiLAString.from(monitoringMessage.getServerUUID()))
                                .setMessageType(SiLAString.from(monitoringMessage.getMessageType()))
                                .setMessage(monitoringMessage.getMessage())
                                .build()).build();

        if(messageListenerMap.containsKey(monitoringMessage.getServerUUID())) {
            List<MonitoringMessageListener> messageListenerList= messageListenerMap.get(monitoringMessage.getServerUUID());
            messageListenerList.forEach(m -> m.onMessage(intermediateResponse));
        }
    }

    /**
     *  Adds a listener for each server in the serverUUID list.
     */
    public void addListener(final @NonNull List<String> serverUUID, final @NonNull MonitoringMessageListener monitoringListener) {
        serverUUID.forEach(server -> addServerToListenTo(server, monitoringListener));
    }


    private void addServerToListenTo(final @NonNull String serverUUID, final @NonNull MonitoringMessageListener monitoringListener) {
        if(messageListenerMap.containsKey(serverUUID)) {
            messageListenerMap.get(serverUUID).add(monitoringListener);
        } else {
            List<MonitoringMessageListener> messageListenerList = new CopyOnWriteArrayList<>();
            messageListenerList.add(monitoringListener);
            messageListenerMap.put(serverUUID, messageListenerList);
        }
    }

    /**
     *  Removes the listener.
     */
    public void removeListener(final @NonNull MonitoringMessageListener monitoringListener) {
        messageListenerMap.forEach((key, value) -> value.remove(monitoringListener));
        messageListenerMap.entrySet().removeIf(e -> e.getValue().isEmpty());
    }

    /**
     * Clears the listener map. Used for testing.
     */
    public void clear() {
        messageListenerMap.clear();
    }

    /**
     * The interface for the monitoring message listener.
     */
    public interface MonitoringMessageListener {
        void onMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses intermediateResponse);
    }
}
