package com.silastandard.gateway.utils;

import lombok.NonNull;

/**
 * Helper class to get convert IDs of Commands and Properties
 */
public class FeatureIdConverter {

    /**
     * Converts a Fully Qualified Call Id to the respective Call Id.
     *
     * Example: converts "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee" to "MakeCoffee"
     */
    public static String getCallID(@NonNull final String fullyQualifiedCallId) {
        String[] splitString = fullyQualifiedCallId.split("/");
        return splitString[splitString.length - 1];
    }

    /**
     * Converts a Fully Qualified Feature Id to the respective Feature Id.
     *
     * Example: converts fullyQualifiedFeatureId "ch.unitelabs/test/UnobservableCommandTest/v1" to FeatureId "UnobservableCommandTest"
     */
    public static String getFeatureID(@NonNull final String fullyQualifiedFeatureId) {
        String[] splitString = fullyQualifiedFeatureId.split("/");
        return splitString[splitString.length - 2];
    }

    /**
     * Converts a Fully Qualified Call Id to the respective Fully Qualified Feature Id.
     *
     * Example: converts "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee" to
     *  "ch.unitelabs/test/UnobservableCommandTest/v1
     */
    public static String getFullyQualifiedFeatureID(@NonNull final String fullyQualifiedCallId) {
        final String outputTmp = fullyQualifiedCallId.substring(0, fullyQualifiedCallId.lastIndexOf("/"));
        return outputTmp.substring(0, outputTmp.lastIndexOf("/"));
    }

    /**
     * Converts Fully Qualified Metadata Id to the the respective valid Custom-Metadata key prefixed with 'sila-' and suffixed with '-bin'.
     *
     * Example: converts "org.silastandard/core/SiLAService/v1/Metadata/AuthorizationToken" to
     *  "sila-org.silastandard-core-silaservice-v1-metadata-authorizationtoken-bin"
     */
    public static String convertMetaDataId(@NonNull final String fullyQualifiedMetadataId) {
        String tmpId = fullyQualifiedMetadataId.replace("/", "-").toLowerCase();
        return "sila-" + tmpId + "-bin";
    }
}
