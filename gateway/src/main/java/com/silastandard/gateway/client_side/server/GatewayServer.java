package com.silastandard.gateway.client_side.server;

import com.silastandard.gateway.GatewayServices;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Opens the connection to a cloud endpoint.
 */
@Slf4j
public class GatewayServer {
    private final static int SHUTDOWN_TIMEOUT = 10; // [seconds]
    private final CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpointStub;
    private final List<GatewayServerEndpointService> cloudServerEndpointServiceList = new CopyOnWriteArrayList<>();
    private final List<ConnectionErrorObserver> connectionErrorObservers = new CopyOnWriteArrayList<>();
    private final ManagedChannel channel;

    public GatewayServer(Builder builder) {
        channel = ChannelFactory.withEncryption(builder.host, builder.port);
        ConnectivityState state = getState();
        while (state == ConnectivityState.IDLE || state == ConnectivityState.CONNECTING) {
            state = getState();
        }
        log.info("[start] GatewayServer started: " + getState());

        this.clientEndpointStub = CloudClientEndpointGrpc.newStub(channel);
    }

    /**
     * Starts and adds an new EndpointService to a connected client for a specific SiLA Server.
     */
    public void addAndStartCloudEndpointService(UUID silaServerUUID, String clientName, GatewayServices gatewayServices) {
        if(cloudServerEndpointServiceList.stream().noneMatch(es -> es.getConnectedSiLAServer().equals(silaServerUUID))) {
            ConnectivityState state = getState();
            log.info("[start EndpointService] tries to StartEndpoint with state - " + state.name());
            if(state == ConnectivityState.READY || state == ConnectivityState.IDLE) {
                GatewayServerEndpointService cloudServerEndpointService = new GatewayServerEndpointService(silaServerUUID, clientName, gatewayServices);
                cloudServerEndpointServiceList.add(cloudServerEndpointService);

                OnStreamErrorCallback onStreamErrorCallback = this::onStreamError;

                cloudServerEndpointService.start(clientEndpointStub, onStreamErrorCallback);

                log.info("[start EndpointService] new EndpointService started - for server: " + silaServerUUID);
            } else {
                log.info("[start EndpointService] ConnectivityState of client not ready - for server: " + silaServerUUID);
            }
        } else {
            log.info("[start EndpointService] endpoint for server already existing");
        }
    }

    /**
     * Adds a ConnectionErrorObserver to the Observer list.
     */
    public void addConnectionErrorObservers(ConnectionErrorObserver connectionErrorObserver) {
        connectionErrorObservers.add(connectionErrorObserver);
    }

    /**
     * Calls removeAndStopEndpointService method and informs subscribed Observers if last endpoint resulted in an error
     * and no endpoint is left.
     * It states that the connection to the Client is lost.
     */
    public void onStreamError(UUID silaServerUUID) {
        removeAndStopEndpointService(silaServerUUID);
        if(cloudServerEndpointServiceList.isEmpty()) {
            connectionErrorObservers.forEach(ConnectionErrorObserver::onConnectionError);
        }
    }

    /**
     * Stops and removes the EndpointService to a client for a connected SiLA Server.
     */
    public void removeAndStopEndpointService(UUID silaServerUUID) {
        Optional<GatewayServerEndpointService> endpointService = cloudServerEndpointServiceList.stream().filter(es -> es.getConnectedSiLAServer().equals(silaServerUUID)).findAny();
        if(endpointService.isPresent()) {
            log.info("[stop EndpointService] EndpointService stopped - for server: " + silaServerUUID);
            endpointService.get().stop();
            cloudServerEndpointServiceList.remove(endpointService.get());
        }
    }

    /**
     * Gets the state of the channel which is the virtual connection to the client.
     */
    public ConnectivityState getState() {
        return channel.getState(true);
    }

    /**
     * Terminates all open EndpointServices and shuts down the channel.
     */
    @SneakyThrows
    public void stop() {
        cloudServerEndpointServiceList.forEach(GatewayServerEndpointService::stop);
        try {
            log.info("[stop] stopping the GatewayServer ...");
            channel.shutdownNow().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
            log.info("[stop] the GatewayServer was stopped");
        } catch (InterruptedException e) {
            log.warn("[stop] could not shutdown the server within {} seconds", SHUTDOWN_TIMEOUT);
        }
    }

    /**
     * Callback Interface to inform observers on stream errors.
     */
    public interface OnStreamErrorCallback {
        void onError(UUID silaServerUUID);
    }

    /**
     * Interface to inform observers on connection errors.
     */
    public interface ConnectionErrorObserver {
        void onConnectionError();
    }

    public static class Builder {
        private Integer port = SILACloudClient.SILA_CLOUD_DEFAULT_PORT;
        private String host;

        /**
         * Define Specific Port for the services.
         *
         * @param port Port on which the gRCP service runs.
         */
        public Builder withPort(int port) {
            this.port = port;
            return this;
        }

        public Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public GatewayServer build() {
            return new GatewayServer(this);
        }
    }
}
