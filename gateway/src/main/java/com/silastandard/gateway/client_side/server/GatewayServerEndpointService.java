package com.silastandard.gateway.client_side.server;

import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.client_side.message_services.*;
import io.grpc.Context;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.UUID;

/**
 * Provides a stream and its message handler to the connected client.
 */
@Slf4j
public class GatewayServerEndpointService {
    private StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream;
    private Context.CancellableContext cancellableContext;
    private GatewayRequestHandler requestHandler;
    @Getter
    private final UUID connectedSiLAServer;
    private final CommandExecutionService commandExecutionService;
    private final ObservableCommandExecutionService observableCommandExecutionService;
    private final ObservablePropertyService observablePropertyService;
    private final UnobservablePropertyService unobservablePropertyService;
    private final BinaryTransferService binaryTransferService;


    public GatewayServerEndpointService(UUID connectedSiLAServer, String clientName, GatewayServices gatewayServices) {
        this.connectedSiLAServer = connectedSiLAServer;

        this.commandExecutionService = new CommandExecutionService(connectedSiLAServer, clientName, gatewayServices);
        this.observableCommandExecutionService = new ObservableCommandExecutionService(connectedSiLAServer, clientName, gatewayServices);
        this.observablePropertyService =  new ObservablePropertyService(connectedSiLAServer, clientName, gatewayServices);
        this.unobservablePropertyService =  new UnobservablePropertyService(connectedSiLAServer, clientName, gatewayServices);
        this.binaryTransferService = new BinaryTransferService(connectedSiLAServer, clientName, gatewayServices);
    }

    /**
     * Performs a gRPC call on the client connection that results in a new server message stream.
     * The call is started via a cancelable context to be able to end it again.
     */
    public void start(CloudClientEndpointGrpc.CloudClientEndpointStub cloudConnection, GatewayServer.OnStreamErrorCallback endpointErrorObserver) {
        cancellableContext = Context.current().withCancellation();
        cancellableContext.run(() -> {
            Context newContext = Context.current().fork();
            Context origContext = newContext.attach();
            try {
                StreamObserver<SiLACloudConnector.SILAClientMessage> requestObserver = new StreamObserver<SiLACloudConnector.SILAClientMessage>() {
                    @SneakyThrows
                    @Override
                    public void onNext(SiLACloudConnector.SILAClientMessage clientMessage) {
                        requestHandler.handleRequest(clientMessage);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable instanceof StatusRuntimeException) {
                            SiLAFramework.SiLAError siLAError = SiLAFramework.SiLAError.newBuilder()
                                    .setFrameworkError(SiLAFramework.FrameworkError.newBuilder().setMessage(throwable.getMessage()))
                                    .build();
                            log.warn("onError: {}", siLAError);
                            endpointErrorObserver.onError(connectedSiLAServer);
                        }
                    }

                    @Override
                    public void onCompleted() {
                        log.info("onCompleted");
                    }
                };

                this.serverMessageStream = cloudConnection.connectSILAServer(requestObserver);
                this.requestHandler = new GatewayRequestHandler(
                        serverMessageStream,
                        commandExecutionService,
                        unobservablePropertyService,
                        observablePropertyService,
                        observableCommandExecutionService,
                        binaryTransferService);
            } finally {
                newContext.detach(origContext);
            }
        });
    }

    /**
     * Cancels the stream via the context.
     */
    public void stop() {
        if (cancellableContext != null) {
            cancellableContext.cancel(null);
            cancellableContext = null;
        }
    }
}
