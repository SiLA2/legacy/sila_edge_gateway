package com.silastandard.gateway.client_side.message_services;


import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.server_side.call_services.GatewayCallService;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import io.grpc.StatusRuntimeException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.List;
import java.util.UUID;

/**
 * Handles Unobservable Commands
 */
@Slf4j
public class CommandExecutionService extends MessageService{
    
    public CommandExecutionService(@NonNull UUID silaServerUUID, @NonNull String clientName, @NonNull GatewayServices gatewayServices) {
        super(silaServerUUID, clientName, gatewayServices);
    }

    /**
     * Processes s Command Execution message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified on the request and response message.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeCommand(SiLACloudConnector.CommandExecution commandExecution) {
        String fullyQualifiedCommandId = commandExecution.getFullyQualifiedCommandId();
        log.info("Received clientMessage FullyQualifiedCommandId:{}", fullyQualifiedCommandId);
        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        ByteString parameterByteString = commandExecution.getCommandParameter().getParameters();
        List<SiLACloudConnector.Metadata> metadataList = commandExecution.getCommandParameter().getMetadataList();

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedCommandId, parameterByteString);
        final GatewayCallService gatewayCallService = gatewayServices.getServerManagerService().newCallService(siLAGatewayCall);

        try {

            // Request
            DynamicMessage requestMessage = gatewayCallService.getRequestMessage(siLAGatewayCall.getCallId());
            gatewayServices.getMonitoringClientMessageProvider().onCommandExecutionMessage(siLAGatewayCall, requestMessage);

            // Response
            DynamicMessage responseMessage = gatewayCallService.executeUnobservableCommand(requestMessage, metadataList);
            gatewayServices.getMonitoringServerMessageProvider().onCommandResponseMessage(siLAGatewayCall, responseMessage);

            ByteString commandResult = responseMessage.toByteString();
            SiLACloudConnector.CommandResponse response = SiLACloudConnector.CommandResponse.newBuilder()
                    .setResult(commandResult)
                    .build();
            return serverMessageBuilder
                    .setCommandResponse(response);
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, e);
            return serverMessageBuilder
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null));
        }
    }

    /**
     * Processes a Get FCP Affected By Metadata message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified on the request and response message.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeMetadataCommand(SiLACloudConnector.GetFCPAffectedByMetadataRequest metadataRequest) {
        String fullyQualifiedMetadataId = metadataRequest.getFullyQualifiedMetadataId();
        log.info("Received clientMessage FullyQualifiedCommandId:{}", fullyQualifiedMetadataId);
        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();

        ByteString parameterByteString = metadataRequest.toByteString();
        SiLAGatewayCall gatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedMetadataId,parameterByteString);

        try {
            gatewayServices.getMonitoringClientMessageProvider().onMetaDataRequestMessage(gatewayCall, fullyQualifiedMetadataId);
            DynamicMessage responseMessage = gatewayServices.getServerManagerService().newCallService(gatewayCall).executeGetFCPAffectedByMetadata();
            gatewayServices.getMonitoringServerMessageProvider().onMetaDataResponseMessage(gatewayCall, responseMessage);

            return serverMessageBuilder
                    .setMetadataResponse(SiLACloudConnector.GetFCPAffectedByMetadataResponse.newBuilder().mergeFrom(responseMessage).build());
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(gatewayCall, e);
            return serverMessageBuilder
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null));
        }
    }
}
