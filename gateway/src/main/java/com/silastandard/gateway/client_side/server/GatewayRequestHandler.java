package com.silastandard.gateway.client_side.server;

import com.silastandard.gateway.client_side.message_services.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;

/**
 * Handles all incoming requests on the stream.
 */
@Slf4j
public class GatewayRequestHandler {
    private final StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream;
    private final CommandExecutionService commandExecutionService;
    private final UnobservablePropertyService unobservablePropertyService;
    private final ObservablePropertyService observablePropertyService;
    private final ObservableCommandExecutionService observableCommandExecutionService;
    private final BinaryTransferService binaryTransferService;

    public GatewayRequestHandler(StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream, CommandExecutionService commandExecutionService, UnobservablePropertyService unobservablePropertyService, ObservablePropertyService observablePropertyService, ObservableCommandExecutionService observableCommandExecutionService, BinaryTransferService binaryTransferService) {
        this.serverMessageStream = serverMessageStream;
        this.commandExecutionService = commandExecutionService;
        this.unobservablePropertyService = unobservablePropertyService;
        this.observablePropertyService = observablePropertyService;
        this.observableCommandExecutionService = observableCommandExecutionService;
        this.binaryTransferService = binaryTransferService;
    }

    /**
     * Handles the incoming requests on the stream. The messages will be passed to the respective message services for further processing.
     */
    public void handleRequest(SiLACloudConnector.SILAClientMessage clientMessage) {
        String requestUUID = clientMessage.getRequestUUID();
        log.debug("onNext, message {}", clientMessage.getMessageCase());

        SiLACloudConnector.SILAClientMessage.MessageCase messageCase = clientMessage.getMessageCase();
        switch (messageCase) {
            case COMMANDEXECUTION:
                SiLACloudConnector.SILAServerMessage.Builder commandExecutionResponse = commandExecutionService.executeCommand(clientMessage.getCommandExecution());
                commandExecutionResponse.setRequestUUID(requestUUID);
                serverMessageStream.onNext(commandExecutionResponse.build());
                break;
            case COMMANDINITIATION:
                SiLACloudConnector.SILAServerMessage.Builder observableCommandResponse = observableCommandExecutionService.initiateCommand(clientMessage.getCommandInitiation());
                observableCommandResponse.setRequestUUID(requestUUID);
                serverMessageStream.onNext(observableCommandResponse.build());
                break;
            case COMMANDEXECUTIONINFOSUBSCRIPTION:
                observableCommandExecutionService.subscribeCommandInfo(clientMessage.getCommandExecutionInfoSubscription(), clientMessage.getRequestUUID(), serverMessageStream);
                break;
            case COMMANDINTERMEDIATERESPONSESUBSCRIPTION:
                observableCommandExecutionService.subscribeCommandIntermediateResult(clientMessage.getCommandIntermediateResponseSubscription(), clientMessage.getRequestUUID(), serverMessageStream);
                break;
            case COMMANDGETRESPONSE:
                SiLACloudConnector.SILAServerMessage.Builder observableCommandGetResponse = observableCommandExecutionService.executeCommandGetResponse(clientMessage.getCommandGetResponse());
                observableCommandGetResponse.setRequestUUID(requestUUID);
                serverMessageStream.onNext(observableCommandGetResponse.build());
                break;
            case METADATAREQUEST:
                SiLACloudConnector.SILAServerMessage.Builder metadataRequestMessage = commandExecutionService.executeMetadataCommand(clientMessage.getMetadataRequest());
                metadataRequestMessage.setRequestUUID(requestUUID);
                serverMessageStream.onNext(metadataRequestMessage.build());
                break;
            case PROPERTYREAD:
                SiLACloudConnector.SILAServerMessage.Builder propertyResponse = unobservablePropertyService.executeProperty(clientMessage.getPropertyRead());
                propertyResponse.setRequestUUID(requestUUID);
                serverMessageStream.onNext(propertyResponse.build());
                break;
            case PROPERTYSUBSCRIPTION:
                observablePropertyService.subscribeProperty(clientMessage.getPropertySubscription(), clientMessage.getRequestUUID(), serverMessageStream);
                break;
            case CANCELCOMMANDEXECUTIONINFOSUBSCRIPTION:
                observableCommandExecutionService.cancelExecutionInfoSubscription(clientMessage.getRequestUUID());
                break;
            case CANCELCOMMANDINTERMEDIATERESPONSESUBSCRIPTION:
                observableCommandExecutionService.cancelIntermediateResponseSubscription(clientMessage.getRequestUUID());
                break;
            case CANCELPROPERTYSUBSCRIPTION:
                observablePropertyService.cancelPropertySubscription(clientMessage.getRequestUUID());
                break;
            case CREATEBINARYUPLOADREQUEST:
                SiLACloudConnector.SILAServerMessage.Builder binaryUploadRequest = binaryTransferService.executeCreateBinaryUploadRequest(clientMessage.getCreateBinaryUploadRequest());
                binaryUploadRequest.setRequestUUID(requestUUID);
                serverMessageStream.onNext(binaryUploadRequest.build());
                break;
            case UPLOADCHUNKREQUEST:
                binaryTransferService.executeUploadChunkRequest(clientMessage, serverMessageStream);
                break;
            case DELETEUPLOADEDBINARYREQUEST:
                SiLACloudConnector.SILAServerMessage.Builder deleteUploadBinaryRequest = binaryTransferService.executeDeleteUploadedBinaryRequest(clientMessage.getDeleteUploadedBinaryRequest());
                deleteUploadBinaryRequest.setRequestUUID(requestUUID);
                serverMessageStream.onNext(deleteUploadBinaryRequest.build());
                break;
            case DELETEDOWNLOADEDBINARYREQUEST:
                SiLACloudConnector.SILAServerMessage.Builder deleteDownloadBinaryRequest = binaryTransferService.executeDeleteDownloadedBinaryRequest(clientMessage.getDeleteDownloadedBinaryRequest());
                deleteDownloadBinaryRequest.setRequestUUID(requestUUID);
                serverMessageStream.onNext(deleteDownloadBinaryRequest.build());
                break;
            case GETBINARYINFOREQUEST:
                SiLACloudConnector.SILAServerMessage.Builder getBinaryInfoRequest = binaryTransferService.executeGetBinaryInfoRequest(clientMessage);
                getBinaryInfoRequest.setRequestUUID(requestUUID);
                serverMessageStream.onNext(getBinaryInfoRequest.build());
                break;
            case GETCHUNKREQUEST:
                binaryTransferService.executeGetChunkRequest(clientMessage, serverMessageStream);
                break;
            case MESSAGE_NOT_SET:
                break;
        }
    }
}
