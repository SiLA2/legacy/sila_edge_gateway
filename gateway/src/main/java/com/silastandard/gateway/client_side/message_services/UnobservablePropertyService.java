package com.silastandard.gateway.client_side.message_services;

import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import io.grpc.StatusRuntimeException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.List;
import java.util.UUID;

/**
 * Handles Unobservable Properties
 */
@Slf4j
public class UnobservablePropertyService extends MessageService{
    public UnobservablePropertyService(@NonNull UUID silaServerUUID, @NonNull String clientName, @NonNull GatewayServices gatewayServices) {
        super(silaServerUUID, clientName, gatewayServices);
    }

    /**
     * Processes a Property Read message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified on the request and response message.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeProperty(SiLACloudConnector.PropertyRead propertyRequest)  {
        final String fullyQualifiedPropertyId = propertyRequest.getFullyQualifiedPropertyId();
        final List<SiLACloudConnector.Metadata> metadataList = propertyRequest.getMetadataList();
        log.info("Received clientMessage PropertyId:{}", fullyQualifiedPropertyId);

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        ByteString byteStringPayload = ByteString.EMPTY;
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedPropertyId, byteStringPayload);

        try {
            gatewayServices.getMonitoringClientMessageProvider().onPropertyReadMessage(siLAGatewayCall, fullyQualifiedPropertyId);
            DynamicMessage messageResponse = gatewayServices.getServerManagerService().newCallService(siLAGatewayCall).executeGetUnobservableProperty(metadataList);
            gatewayServices.getMonitoringServerMessageProvider().onPropertyValueMessage(siLAGatewayCall, messageResponse);

            ByteString propertyResult = messageResponse.toByteString();

            SiLACloudConnector.PropertyValue response = SiLACloudConnector.PropertyValue.newBuilder()
                    .setResult(propertyResult)
                    .build();
            return serverMessageBuilder
                    .setPropertyValue(response);
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, e);
            return serverMessageBuilder
                    .setPropertyError(SiLAErrors.retrieveSiLAError(e).orElse(null));
        }
    }
}
