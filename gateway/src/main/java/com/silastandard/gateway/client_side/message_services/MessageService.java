package com.silastandard.gateway.client_side.message_services;

import com.silastandard.gateway.GatewayServices;
import lombok.NonNull;

import java.util.UUID;

/**
 * MessageService is the abstract base class for all message services.
 * It contains the objects that all message services need for message execution.
 */
abstract class MessageService {
    protected final UUID silaServerUUID;
    protected final String clientName;
    protected final GatewayServices gatewayServices;

    protected MessageService(@NonNull UUID silaServerUUID,
                             @NonNull String clientName,
                             @NonNull GatewayServices gatewayServices) {
        this.silaServerUUID = silaServerUUID;
        this.clientName = clientName;
        this.gatewayServices = gatewayServices;
    }
}
