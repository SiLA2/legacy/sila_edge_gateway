package com.silastandard.gateway.client_side.message_services;

import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.server_side.models.SubscriptionStreamObserver;
import com.silastandard.gateway.server_side.call_services.GatewayCallService;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import io.grpc.Context;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Handles Unobservable Commands
 */
@Slf4j
public class ObservableCommandExecutionService extends MessageService {
    private final Map<String, Context.CancellableContext> infoSubscriptionContextMap = new ConcurrentHashMap<>();
    private final Map<String, Context.CancellableContext> intermediateResultSubscriptionContextMap = new ConcurrentHashMap<>();
    private final ExpiringMap<SiLAFramework.CommandExecutionUUID, String> fullyQualifiedCommandIDMap = ExpiringMap.builder().variableExpiration().build();

    public ObservableCommandExecutionService(@NonNull UUID silaServerUUID, @NonNull String clientName, @NonNull GatewayServices gatewayServices) {
        super(silaServerUUID, clientName, gatewayServices);
    }

    /**
     * Processes a Command Initiation message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message.
     * The CommandExecutionUUID is stored together with the fullyQualifiedCommandId for further calls that depend on the CommandExecutionUID.
     * The CommandExecutionUUID is valid and stored until the LifeTimeOfExecution expires. If no LifetimeOfExecution is specified, it will be stored until the server lifetime.
     */
    public SiLACloudConnector.SILAServerMessage.Builder initiateCommand(SiLACloudConnector.CommandInitiation commandInitiation) {
        String fullyQualifiedCommandId = commandInitiation.getFullyQualifiedCommandId();
        ByteString parameterByteString = commandInitiation.getCommandParameter().getParameters();
        List<SiLACloudConnector.Metadata> metadataList = commandInitiation.getCommandParameter().getMetadataList();
        log.info("Received Command Initiation message - FullyQualifiedCommandId: {}", fullyQualifiedCommandId);

        SiLAGatewayCall silaGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedCommandId, parameterByteString);
        GatewayCallService silaGatewayCallService = gatewayServices.getServerManagerService().newCallService(silaGatewayCall);

        try {
            DynamicMessage requestMessage = silaGatewayCallService.getRequestMessage(silaGatewayCall.getCallId());
            gatewayServices.getMonitoringClientMessageProvider().onCommandInitiationMessage(silaGatewayCall, requestMessage);

            DynamicMessage responseMessage = silaGatewayCallService.executeObservableCommandInitiation(requestMessage, metadataList);
            gatewayServices.getMonitoringServerMessageProvider().onCommandResponseMessage(silaGatewayCall, responseMessage);

            SiLACloudConnector.ObservableCommandConfirmation commandConfirmationResponse = SiLACloudConnector.ObservableCommandConfirmation.newBuilder()
                    .setCommandConfirmation(SiLAFramework.CommandConfirmation.newBuilder()
                            .mergeFrom(responseMessage)
                            .build())
                    .build();

            SiLAFramework.CommandExecutionUUID commandExecutionUUID = commandConfirmationResponse.getCommandConfirmation().getCommandExecutionUUID();
            if(commandConfirmationResponse.getCommandConfirmation().hasLifetimeOfExecution()) {
                long sec = commandConfirmationResponse.getCommandConfirmation().getLifetimeOfExecution().getSeconds();
                long secInNanos = TimeUnit.NANOSECONDS.convert(sec, TimeUnit.SECONDS);

                fullyQualifiedCommandIDMap.put(commandConfirmationResponse.getCommandConfirmation().getCommandExecutionUUID(),
                        fullyQualifiedCommandId,
                        ExpirationPolicy.CREATED,
                        secInNanos + commandConfirmationResponse.getCommandConfirmation().getLifetimeOfExecution().getNanos(),
                        TimeUnit.NANOSECONDS);
            } else {
                fullyQualifiedCommandIDMap.put(commandExecutionUUID, fullyQualifiedCommandId);
            }

            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setObservableCommandConfirmation(commandConfirmationResponse);
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(silaGatewayCall, e);
            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null));
        }
    }

    /**
     * Processes a Command Get Response message.
     * If the Command Execution UUID is no longer valid, the corresponding SiLA Error is returned.
     * Otherwise, it forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeCommandGetResponse(SiLACloudConnector.CommandGetResponse commandGetRequest) {
        SiLAFramework.CommandExecutionUUID executionUUID = commandGetRequest.getExecutionUUID();
        String fullyQualifiedCommandId = fullyQualifiedCommandIDMap.get(commandGetRequest.getExecutionUUID());
        log.info("Received Command Get Response message - FullyQualifiedCommandId: {}", fullyQualifiedCommandId);

        if (null == fullyQualifiedCommandId) {
            SiLAFramework.SiLAError frameworkError = SiLAFramework.SiLAError.newBuilder().setFrameworkError(SiLAFramework.FrameworkError
                    .newBuilder()
                    .setErrorType(SiLAFramework.FrameworkError.ErrorType.INVALID_COMMAND_EXECUTION_UUID).build()).build();
            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(frameworkError);
        }

        SiLAGatewayCall silaGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedCommandId, executionUUID.toByteString());

        try {
            gatewayServices.getMonitoringClientMessageProvider().onCommandGetResponseMessage(silaGatewayCall, executionUUID.getValue());
            DynamicMessage responseMessage = gatewayServices.getServerManagerService().newCallService(silaGatewayCall).executeObservableCommandGetResponse();
            gatewayServices.getMonitoringServerMessageProvider().onObservableCommandResponseMessage(silaGatewayCall, executionUUID.getValue(), responseMessage);

            return SiLACloudConnector.SILAServerMessage.newBuilder().setObservableCommandResponse(
                    SiLACloudConnector.ObservableCommandResponse.newBuilder()
                        .setResult(responseMessage.toByteString())
                        .build());
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(silaGatewayCall, e);
            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null));
        }
    }

    /**
     * Processes a Command Execution Info Subscription message.
     * If the Command Execution UUID is no longer valid, the corresponding SiLA Error is returned.
     * Otherwise, it forwards the message to the CallService for gRPC-execution and notifies the Monitoring Message Provider about the request and response messages.
     * A context in which the call is executed is created to be able to terminate the stream.
     * The termination of the stream is initiated by the CancelInfoSubscription message, for StreamComplete and StreamError.
     */
    public void subscribeCommandInfo(SiLACloudConnector.CommandExecutionInfoSubscription infoSubscription,
                                     String requestUUID,
                                     StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLAFramework.CommandExecutionUUID executionUUID = infoSubscription.getExecutionUUID();
        String fullyQualifiedCommandId = fullyQualifiedCommandIDMap.get(executionUUID);
        log.info("Received Command Execution Info Subscription message - FullyQualifiedCommandId: {}", fullyQualifiedCommandId);

        if (null == fullyQualifiedCommandId) {
            SiLAFramework.SiLAError frameworkError = SiLAFramework.SiLAError.newBuilder().setFrameworkError(SiLAFramework.FrameworkError
                    .newBuilder()
                    .setErrorType(SiLAFramework.FrameworkError.ErrorType.INVALID_COMMAND_EXECUTION_UUID).build()).build();
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder().setCommandError(frameworkError).build());
            return;
        }

        SiLAGatewayCall silaGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedCommandId, executionUUID.toByteString());

        try {
            gatewayServices.getMonitoringClientMessageProvider().onCommandExecutionInfoSubscriptionMessage(silaGatewayCall, executionUUID.getValue());

            Context.CancellableContext contextInfoSubscription = Context.current().withCancellation();
            infoSubscriptionContextMap.put(requestUUID, contextInfoSubscription);

            gatewayServices.getServerManagerService().newCallService(silaGatewayCall).executeObservableCommandInfoSubscription(
                    contextInfoSubscription,
                    new SubscriptionStreamObserver.SubscriptionStreamCallback() {
                        @Override
                        public boolean onNext(DynamicMessage responseMessage) {
                            try {
                                final SiLAFramework.ExecutionInfo.Builder executionInfo = SiLAFramework.ExecutionInfo.newBuilder().mergeFrom(responseMessage);
                                log.info("Received status for call " + silaGatewayCall.getCallId());
                                log.info(executionInfo.toString());

                                // Update new lifetime if Execution Info provides a new one
                                if (executionInfo.hasUpdatedLifetimeOfExecution()) {
                                    long newAbsolutLifetime = TimeUnit.SECONDS.toMillis(executionInfo.getUpdatedLifetimeOfExecution().getSeconds());
                                    long oldAbsolutLifetime = fullyQualifiedCommandIDMap.getExpiration(executionUUID);
                                    if (newAbsolutLifetime > oldAbsolutLifetime) {
                                        long pastTime = oldAbsolutLifetime - fullyQualifiedCommandIDMap.getExpectedExpiration(executionUUID);
                                        fullyQualifiedCommandIDMap.setExpiration(executionUUID, newAbsolutLifetime - pastTime, TimeUnit.MILLISECONDS);
                                    }
                                }

                                // build SeverMessage and send to serverMessageStream
                                SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                                        .setObservableCommandExecutionInfo(SiLACloudConnector.ObservableCommandExecutionInfo.newBuilder()
                                                .setExecutionInfo(executionInfo))
                                        .build();
                                serverMessageStream.onNext(serverMessage);
                                // monitoring: response
                                gatewayServices.getMonitoringServerMessageProvider().onObservableCommandExecutionInfoMessage(silaGatewayCall, executionUUID.getValue(), responseMessage);

                                final AtomicInteger commandStatus = new AtomicInteger(executionInfo.getCommandStatus().getNumber());
                                return (commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.running_VALUE ||
                                        commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.waiting_VALUE
                                );

                            } catch (final IllegalArgumentException e) {
                                log.warn("Received a malformed message: ", e);
                                return false;
                            }
                        }

                        @Override
                        public void onComplete() {
                            cancelExecutionInfoSubscription(requestUUID);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            handleThrowableError(throwable, silaGatewayCall, serverMessageStream);
                            cancelExecutionInfoSubscription(requestUUID);
                        }
                    }
            );
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(silaGatewayCall, e);
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null)).build());
        }
    }

    /**
     * Processes a Command Intermediate Response Subscription message.
     * If the Command Execution UUID is no longer valid, the corresponding SiLA Error is returned.
     * Otherwise, it forwards the message to the CallService for gRPC-execution and notifies the Monitoring Message Provider about the request and response messages.
     * A context in which the call is executed is created to be able to terminate the stream.
     * The termination of the stream is initiated by the CancelIntermediateResponseSubscription message, for StreamComplete and StreamError.
     */
    public void subscribeCommandIntermediateResult(SiLACloudConnector.CommandIntermediateResponseSubscription intermediateSubscription,
                                                   String requestUUID,
                                                   StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLAFramework.CommandExecutionUUID executionUUID = intermediateSubscription.getExecutionUUID();
        String fullyQualifiedCommandId = fullyQualifiedCommandIDMap.get(executionUUID);
        log.info("Received Command Intermediate Response Subscription messages - FullyQualifiedCommandId: {}", fullyQualifiedCommandId);

        if (null == fullyQualifiedCommandId) {
            SiLAFramework.SiLAError frameworkError = SiLAFramework.SiLAError.newBuilder().setFrameworkError(SiLAFramework.FrameworkError
                    .newBuilder()
                    .setErrorType(SiLAFramework.FrameworkError.ErrorType.INVALID_COMMAND_EXECUTION_UUID).build()).build();
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder().setCommandError(frameworkError).build());
            return;
        }
        SiLAGatewayCall silaGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedCommandId, executionUUID.toByteString());
        
        try {
            gatewayServices.getMonitoringClientMessageProvider().onCommandIntermediateResponseSubscriptionMessage(silaGatewayCall, executionUUID.getValue());

            Context.CancellableContext contextIntermediateSubscription = Context.current().withCancellation();
            intermediateResultSubscriptionContextMap.put(requestUUID, contextIntermediateSubscription);

            gatewayServices.getServerManagerService().newCallService(silaGatewayCall).executeIntermediateCommandSubscription(
                    contextIntermediateSubscription,
                    new SubscriptionStreamObserver.SubscriptionStreamCallback() {
                        @Override
                        public boolean onNext(DynamicMessage responseMessage) {
                            try {
                                // build SeverMessage and send to serverMessageStream
                                SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                                        .setObservableCommandIntermediateResponse(SiLACloudConnector.ObservableCommandIntermediateResponse.newBuilder()
                                                .setExecutionUUID(executionUUID)
                                                .setResult(responseMessage.toByteString())
                                                .build())
                                        .build();
                                serverMessageStream.onNext(serverMessage);
                                // monitoring: response
                                gatewayServices.getMonitoringServerMessageProvider().onObservableCommandIntermediateResponseMessage(silaGatewayCall, executionUUID.getValue(), responseMessage);
                                return true;

                            } catch (final Exception e) {
                                log.warn("Received a malformed message: ", e);
                                return false;
                            }
                        }

                        @Override
                        public void onComplete() {
                            cancelIntermediateResponseSubscription(requestUUID);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            handleThrowableError(throwable, silaGatewayCall, serverMessageStream);
                            cancelIntermediateResponseSubscription(requestUUID);
                        }
                    }
            );
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(silaGatewayCall, e);
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null)).build());
        }
    }

    /**
     * Processes a Cancel Command Execution Info Subscription message.
     * Cancels the stored context to the UUID of the request.
     */
    public void cancelExecutionInfoSubscription(String requestUUID) {
        log.info("Cancel Command Execution Info Subscription - Request UUID: {}", requestUUID);
        cancelContext(infoSubscriptionContextMap, requestUUID);
    }

    /**
     * Processes a Cancel Intermediate Response Subscription message.
     * Cancels the stored context to the UUID of the request.
     */
    public void cancelIntermediateResponseSubscription(String requestUUID) {
        log.info("Cancel Intermediate Response Subscription - Request UUID: {}", requestUUID);
        cancelContext(intermediateResultSubscriptionContextMap, requestUUID);
    }

    private void cancelContext(Map<String, Context.CancellableContext> contextMap, String requestUUID) {
        if(contextMap.containsKey(requestUUID)) {
            Context.CancellableContext context = contextMap.get(requestUUID);
            context.cancel(null);
            contextMap.remove(requestUUID);
        }
    }

    private void handleThrowableError(Throwable throwable, SiLAGatewayCall siLAGatewayCall, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        if (throwable instanceof StatusRuntimeException) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, (StatusRuntimeException) throwable);
            SiLACloudConnector.SILAServerMessage serverErrorMessage = SiLACloudConnector.SILAServerMessage
                    .newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError((StatusRuntimeException) throwable).orElse(null))
                    .build();
            serverMessageStream.onNext(serverErrorMessage);
        }
    }
}
