package com.silastandard.gateway;

import com.silastandard.gateway.monitoring_configuration.server.monitoring.MonitoringClientMessageProvider;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.MonitoringServerMessageProvider;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.MonitoringSubscriptionService;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import lombok.Getter;

/**
 * Contains services which are needed for server-side connections and monitoring
 */
@Getter
public class GatewayServices {
    private final ServerManagerService serverManagerService;
    private final MonitoringSubscriptionService monitoringSubscriptionService;
    private final MonitoringServerMessageProvider monitoringServerMessageProvider;
    private final MonitoringClientMessageProvider monitoringClientMessageProvider;

    public GatewayServices() {
        this.monitoringSubscriptionService = new MonitoringSubscriptionService();
        this.serverManagerService = new ServerManagerService();
        this.monitoringClientMessageProvider = new MonitoringClientMessageProvider(monitoringSubscriptionService, serverManagerService);
        this.monitoringServerMessageProvider = new MonitoringServerMessageProvider(monitoringSubscriptionService, serverManagerService);
    }

    public GatewayServices(MonitoringSubscriptionService monitoringSubscriptionService, ServerManagerService serverManagerService) {
        this.monitoringSubscriptionService = monitoringSubscriptionService;
        this.serverManagerService = serverManagerService;
        this.monitoringClientMessageProvider = new MonitoringClientMessageProvider(monitoringSubscriptionService, serverManagerService);
        this.monitoringServerMessageProvider = new MonitoringServerMessageProvider(monitoringSubscriptionService, serverManagerService);
    }

    public void clear() {
        serverManagerService.close();
        monitoringSubscriptionService.clear();
    }
}
