package com.silastandard.gateway.client_side.binary_test_server.test_server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.core.utils.Utils.blockUntilStop;

/**
 * SiLA Server for test purposes
 */
@Slf4j
public class BinaryTestServer implements AutoCloseable {
    static final String SERVER_TYPE = "Test Server";
    private final SiLAServer siLAServer;
    private static final int TIMEOUT = 60;

    public BinaryTestServer(@NonNull final ArgumentHelper argumentHelper) {
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "Server for test purposes",
                "www.unitelabs.ch",
                "v0.0"
        );

        try {
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            BinaryDatabase binaryTestDatabase = new TestBinaryDatabase();
            builder.withBinaryTransferSupport(binaryTestDatabase);

            this.siLAServer = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.siLAServer.close();
    }

    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        // Start Server
        try (final BinaryTestServer server = new BinaryTestServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(server::close));
            blockUntilStop();
        }
        System.out.println("Server closed.");
    }
}
