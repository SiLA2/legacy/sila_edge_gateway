package com.silastandard.gateway.client_side;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.silastandard.gateway.client_side.message_services.*;
import com.silastandard.gateway.client_side.server.GatewayRequestHandler;
import com.silastandard.gateway.GatewayServices;
import io.grpc.stub.StreamObserver;
import junit.framework.Assert;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;
import sila_java.examples.test_server.TestServer;
import sila_java.examples.test_server.impl.UnobservableCommand;
import sila_java.examples.test_server.impl.UnobservableProperty;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import client.messages.utils.ServerInfo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static junit.framework.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GatewayRequestHandlerTest {
    private static final int TIMEOUT = 60;
    private TestServer testServer;
    private UUID testServerUUID;
    private final String clientName = "Test Client";
    private GatewayServices gatewayServices = new GatewayServices();
    final ServerInfo serverInfo = new ServerInfo(Paths.get("./config/testserver1.cfg"), 50100);

    @BeforeEach
    void setup() throws IOException {
        gatewayServices = new GatewayServices();
        testServer = new TestServer(
                ArgumentHelperBuilder.build(serverInfo.getPort(), "Test server", serverInfo.getConfigFile().getPath(), "local"));
        testServerUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(gatewayServices.getServerManagerService(),testServerUUID, Server.Status.ONLINE, TIMEOUT);

    }

    @AfterEach
    void cleanup() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        testServer.close();
        waitServerState(gatewayServices.getServerManagerService(),testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        gatewayServices.getServerManagerService().close();
    }

    @Test
    void CommandExecutionRequestTest() throws IOException, InterruptedException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // CommandExecution
        UnobservableCommandTestOuterClass.MakeCoffee_Parameters parameters = UnobservableCommandTestOuterClass.MakeCoffee_Parameters.newBuilder()
                .setSugar(SiLABoolean.from(false)).build();
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecution(SiLACloudConnector.CommandExecution.newBuilder()
                        .setFullyQualifiedCommandId(fullyQualifiedCommandId)
                        .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder().setParameters(parameters.toByteString()).build())
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        // Test
        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        Parser<UnobservableCommandTestOuterClass.MakeCoffee_Responses> parser = UnobservableCommandTestOuterClass.MakeCoffee_Responses.parser();
        UnobservableCommandTestOuterClass.MakeCoffee_Responses response =
                parser.parseFrom(silaServerMessageResult.getCommandResponse().getResult().toByteArray());

        Assert.assertEquals(UnobservableCommand.MAKE_COFFEE_RESPONSE, response);
    }

    @Test
    void CommandExecutionRequestMultipleResponseTest() throws IOException, InterruptedException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/WhyMakeItSimpleWhenYouCanMakeItComplicated";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // CommandExecution
        UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters params= UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters.newBuilder()
                .addListStructList(UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters.ListStructList_Struct.newBuilder()
                        .addList(SiLAString.from("listValue"))
                        .build())
                .build();

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecution(SiLACloudConnector.CommandExecution.newBuilder()
                        .setFullyQualifiedCommandId(fullyQualifiedCommandId)
                        .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder().setParameters(params.toByteString()).build())
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        // Test
        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses response =
                UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses.parseFrom(silaServerMessageResult.getCommandResponse().getResult().toByteArray());

        Assert.assertEquals(UnobservableCommand.COMPLICATED_RESPONSES, response);
    }

    @Test
    void CommandExecutionWithMetadataRequestTest() throws IOException, InterruptedException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        String fullyQualifiedMetadataId = "org.silastandard/core/LockController/v1/Metadata/LockIdentifier";
        String lockId = UUID.randomUUID().toString();
        LockControllerOuterClass.Metadata_LockIdentifier metadata_lockIdentifier =
                LockControllerOuterClass.Metadata_LockIdentifier.newBuilder().setLockIdentifier(SiLAString.from(lockId)).build();

        SiLACloudConnector.Metadata metadata = SiLACloudConnector.Metadata.newBuilder()
                .setFullyQualifiedMetadataId(fullyQualifiedMetadataId)
                .setValue(metadata_lockIdentifier.toByteString())
                .build();

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // CommandExecution
        UnobservableCommandTestOuterClass.MakeCoffee_Parameters parameters = UnobservableCommandTestOuterClass.MakeCoffee_Parameters.newBuilder()
                .setSugar(SiLABoolean.from(false)).build();
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecution(SiLACloudConnector.CommandExecution.newBuilder()
                        .setFullyQualifiedCommandId(fullyQualifiedCommandId)
                        .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder()
                                .setParameters(parameters.toByteString())
                                .addMetadata(metadata)
                                .build())
                    .build());


        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        // Test
        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        Parser<UnobservableCommandTestOuterClass.MakeCoffee_Responses> parser = UnobservableCommandTestOuterClass.MakeCoffee_Responses.parser();
        UnobservableCommandTestOuterClass.MakeCoffee_Responses response =
                parser.parseFrom(silaServerMessageResult.getCommandResponse().getResult().toByteArray());

        Assert.assertEquals(UnobservableCommand.MAKE_COFFEE_RESPONSE, response);
    }

    @Test
    void observableCommandTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // observableCommand
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();

        // Tests
        assertTrue(commandConfirmation.hasCommandExecutionUUID());
        assertTrue(commandConfirmation.hasLifetimeOfExecution());
        assertEquals(commandConfirmation.getLifetimeOfExecution().getSeconds(), 25);
    }

    @Test
    void observableCommandResponseErrorTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // Command Initiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService,binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();

        // Test commandGetResponse: do not wait until command finished
        serverMessageResult = commandGetResponse(commandConfirmation, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        assertTrue(serverMessageResult.hasCommandError());
    }

    @Test
    void observableCommandResponseTest() throws InterruptedException, InvalidProtocolBufferException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // Command Initiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();

        // Test commandGetResponse: wait until command finished
        TimeUnit.SECONDS.sleep(20); // Command runs with setUpdateDevice=false for ~10 seconds until response is ready

        serverMessageResult = commandGetResponse(commandConfirmation, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        Assertions.assertFalse(serverMessageResult.hasCommandError());

        SiLACloudConnector.ObservableCommandResponse observableCommandResponse = serverMessageResult.getObservableCommandResponse();

        Parser<ObservableCommandTestOuterClass.RestartDevice_Responses> parser = ObservableCommandTestOuterClass.RestartDevice_Responses.parser();
        ObservableCommandTestOuterClass.RestartDevice_Responses result =
                parser.parseFrom(observableCommandResponse.getResult().toByteArray());

        ObservableCommandTestOuterClass.RestartDevice_Responses expected_result = ObservableCommandTestOuterClass.RestartDevice_Responses.getDefaultInstance();
        assertEquals(expected_result, result);
    }

    @Test
    void commandLifeTimeTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // Command Initiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();

        // Tests
        assertEquals(25, commandConfirmation.getLifetimeOfExecution().getSeconds());
        TimeUnit.SECONDS.sleep(25);

        serverMessageResult = commandGetResponse(commandConfirmation, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        Assertions.assertTrue(serverMessageResult.hasCommandError());
    }

    @Test
    void commandInfoSubscriptionTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // Command Initiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();
        String commandExecutionUUID = commandConfirmation.getCommandExecutionUUID().getValue();

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(2);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        // InfoSubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecutionInfoSubscription(SiLACloudConnector.CommandExecutionInfoSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID).build())
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());

        // Tests
        assertTrue(latch.await(20, TimeUnit.SECONDS));
        SiLACloudConnector.SILAServerMessage serverMessage_1 = result.get(0);
        SiLAFramework.ExecutionInfo executionInfo_1 = serverMessage_1.getObservableCommandExecutionInfo().getExecutionInfo();
        assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running_VALUE, executionInfo_1.getCommandStatus().getNumber());

        SiLACloudConnector.SILAServerMessage serverMessage_2 = result.get(1);
        SiLAFramework.ExecutionInfo executionInfo_2 = serverMessage_2.getObservableCommandExecutionInfo().getExecutionInfo();
        assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully_VALUE, executionInfo_2.getCommandStatus().getNumber());
    }

    @Test
    void CancelCommandSubscriptionTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // Command Initiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();
        String commandExecutionUUID = commandConfirmation.getCommandExecutionUUID().getValue();

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(2);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        // InfoSubscription
        String infoSubscriptionRequestUUID = UUID.randomUUID().toString();
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecutionInfoSubscription(SiLACloudConnector.CommandExecutionInfoSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID).build())
                        .build())
                .setRequestUUID(infoSubscriptionRequestUUID);
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // CancelInfoSubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessageCancelSubscription = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCancelCommandExecutionInfoSubscription(SiLACloudConnector.CancelCommandExecutionInfoSubscription.newBuilder().build())
                .setRequestUUID(infoSubscriptionRequestUUID);
        gatewayRequestHandler.handleRequest(clientMessageCancelSubscription.build());

        // Tests
        assertFalse(latch.await(6, TimeUnit.SECONDS));
    }

    @Test
    void subscribeIntermediateResultTest() throws InterruptedException, InvalidProtocolBufferException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // CommandInitiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();
        String commandExecutionUUID = commandConfirmation.getCommandExecutionUUID().getValue();

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(15);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        // CommandIntermediateResponseSubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandIntermediateResponseSubscription(SiLACloudConnector.CommandIntermediateResponseSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID).build())
                        .build());
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // Tests
        assertTrue(latch.await(20, TimeUnit.SECONDS));

        for (SiLACloudConnector.SILAServerMessage serverMessage : result) {
            Parser<ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses> parser = ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses.parser();
            ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses response = parser.parseFrom(serverMessage.getObservableCommandIntermediateResponse().getResult().toByteArray());

            ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses expectedResult =
                    ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses.newBuilder()
                            .setFileBeingUpdated(SiLAString.from("Matrix.exe"))
                            .build();

            assertEquals(expectedResult, response);
        }
    }

    @Test
    void cancelIntermediateResponseSubscriptionTest() throws InterruptedException {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        // CommandInitiation
        SiLACloudConnector.SILAServerMessage serverMessageResult = commandInitiation(commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        SiLAFramework.CommandConfirmation commandConfirmation = serverMessageResult.getObservableCommandConfirmation().getCommandConfirmation();
        String commandExecutionUUID = commandConfirmation.getCommandExecutionUUID().getValue();

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(20);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        // CommandIntermediateResponseSubscription
        String intermediateResponseSubscriptionRequestUUID = UUID.randomUUID().toString();
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandIntermediateResponseSubscription(SiLACloudConnector.CommandIntermediateResponseSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID).build())
                        .build())
                .setRequestUUID(intermediateResponseSubscriptionRequestUUID);
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // CancelInfoSubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessageCancelSubscription = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCancelCommandIntermediateResponseSubscription(SiLACloudConnector.CancelCommandIntermediateResponseSubscription.newBuilder()
                        .build())
                .setRequestUUID(intermediateResponseSubscriptionRequestUUID);
        gatewayRequestHandler.handleRequest(clientMessageCancelSubscription.build());

        // Tests
        assertFalse(latch.await(16, TimeUnit.SECONDS));
    }

    @Test
    void unobservablePropertyReadTest() throws IOException, InterruptedException {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // unobservablePropertyRead
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertyRead(SiLACloudConnector.PropertyRead.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build());
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // Tests
        assertTrue(latch.await(1, TimeUnit.SECONDS));
        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        Parser<UnobservablePropertyTestOuterClass.Get_ListString_Responses> parser = UnobservablePropertyTestOuterClass.Get_ListString_Responses.parser();

        UnobservablePropertyTestOuterClass.Get_ListString_Responses response =
                parser.parseFrom(silaServerMessageResult.getPropertyValue().getResult().toByteArray());

        Assertions.assertEquals(UnobservableProperty.LIST_STRING_RESPONSE, response);
    }

    @Test
    void observablePropertySubscriptionTest() throws InterruptedException, InvalidProtocolBufferException {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(10);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // observablePropertySubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessage  = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertySubscription(SiLACloudConnector.PropertySubscription.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build());
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // Tests
        assertTrue(latch.await(12, TimeUnit.SECONDS));

        Parser<ObservablePropertyTestOuterClass.Subscribe_ListString_Responses> parser = ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.parser();
        ArrayList<SiLAFramework.String> list = new ArrayList<SiLAFramework.String>() {
            {
                this.add(SiLAString.from("Element 0"));
                this.add(SiLAString.from("Element 1"));
                this.add(SiLAString.from("Element 2"));
            }
        };
        for (SiLACloudConnector.SILAServerMessage serverMessage : result) {
            final ObservablePropertyTestOuterClass.Subscribe_ListString_Responses response = parser.parseFrom(serverMessage.getObservablePropertyValue().getResult().toByteArray());

            list.add(SiLAString.from("Element " + list.size()));
            ObservablePropertyTestOuterClass.Subscribe_ListString_Responses expectedResponse = ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.newBuilder().addAllListString(list).build();

            assertEquals(expectedResponse, response);
        }
    }

    @Test
    void cancelObservablePropertySubscriptionTest() throws InterruptedException {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(10);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // observablePropertySubscription
        String requestUUID = UUID.randomUUID().toString();
        SiLACloudConnector.SILAClientMessage.Builder clientMessage  = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertySubscription(SiLACloudConnector.PropertySubscription.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build())
                .setRequestUUID(requestUUID);
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // cancelObservablePropertySubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessageCancel  = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCancelPropertySubscription(SiLACloudConnector.CancelPropertySubscription.newBuilder().build())
                .setRequestUUID(requestUUID);
        gatewayRequestHandler.handleRequest(clientMessageCancel.build());

        //Tests
        assertFalse(latch.await(12, TimeUnit.SECONDS));
    }

    @Test
    void errorResponseTest() throws InterruptedException {
        final String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String errorIdentifier = "TestException";
        final String errorMessage = "Nothing to do, this will always throw an exception";

        // Get RequestHandler
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        // unobservablePropertyRead
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertyRead(SiLACloudConnector.PropertyRead.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build());
        gatewayRequestHandler.handleRequest(clientMessage.build());

        // Tests
        assertTrue(latch.await(1, TimeUnit.SECONDS));
        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        assertTrue(silaServerMessageResult.hasPropertyError());
        SiLAFramework.DefinedExecutionError actualError = silaServerMessageResult.getPropertyError().getDefinedExecutionError();
        assertEquals(errorIdentifier, actualError.getErrorIdentifier());
        assertEquals(errorMessage, actualError.getMessage());
    }

    private SiLACloudConnector.SILAServerMessage commandInitiation(CommandExecutionService commandExecutionService,
                                                                   UnobservablePropertyService unobservablePropertyService,
                                                                   ObservableCommandExecutionService observableCommandExecutionService,
                                                                   ObservablePropertyService observablePropertyService,
                                                                    BinaryTransferService binaryTransferService) throws InterruptedException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";

        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);

        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        ObservableCommandTestOuterClass.RestartDevice_Parameters parameters = ObservableCommandTestOuterClass.RestartDevice_Parameters
                .newBuilder()
                .setUpdateDevice(SiLABoolean.from(true))
                .build();

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandInitiation(SiLACloudConnector.CommandInitiation.newBuilder()
                        .setFullyQualifiedCommandId(fullyQualifiedCommandId)
                        .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder().setParameters(parameters.toByteString()).build())
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());

        assertTrue(latch.await(1, TimeUnit.SECONDS));
        return result.get(0);
    }

    private SiLACloudConnector.SILAServerMessage commandGetResponse(SiLAFramework.CommandConfirmation commandConfirmation,
                                                                    CommandExecutionService commandExecutionService,
                                                                    UnobservablePropertyService unobservablePropertyService,
                                                                    ObservableCommandExecutionService observableCommandExecutionService,
                                                                    ObservablePropertyService observablePropertyService,
                                                                    BinaryTransferService binaryTransferService) throws InterruptedException {
        String commandExecutionUUID = commandConfirmation.getCommandExecutionUUID().getValue();
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);

        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage.Builder clientMessageGetResponse = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandGetResponse(SiLACloudConnector.CommandGetResponse.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID).build())
                        .build());

        gatewayRequestHandler.handleRequest(clientMessageGetResponse.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        return result.get(0);
    }

    private GatewayRequestHandler getRequestHandler(CountDownLatch latch,
                                                     List<SiLACloudConnector.SILAServerMessage> result,
                                                     CommandExecutionService commandExecutionService,
                                                     UnobservablePropertyService unobservablePropertyService,
                                                     ObservableCommandExecutionService observableCommandExecutionService,
                                                     ObservablePropertyService observablePropertyService,
                                                     BinaryTransferService binaryTransferService) {
        return new GatewayRequestHandler(
                getMessageStreamObserver(latch, result),
                commandExecutionService,
                unobservablePropertyService,
                observablePropertyService,
                observableCommandExecutionService,
                binaryTransferService);
    }

    public GatewayRequestHandler getRequestHandler(CountDownLatch latch,
                                                    List<SiLACloudConnector.SILAServerMessage> result) {
        return new GatewayRequestHandler(getMessageStreamObserver(latch, result),
                new CommandExecutionService(testServerUUID, clientName, gatewayServices),
                new UnobservablePropertyService(testServerUUID, clientName, gatewayServices),
                new ObservablePropertyService(testServerUUID, clientName, gatewayServices),
                new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices),
                new BinaryTransferService(testServerUUID, clientName, gatewayServices));
    }

    public static StreamObserver<SiLACloudConnector.SILAServerMessage> getMessageStreamObserver(CountDownLatch latch,
                                                                                         List<SiLACloudConnector.SILAServerMessage> result) {
        return  new StreamObserver<SiLACloudConnector.SILAServerMessage>() {
            @SneakyThrows
            @Override
            public void onNext(SiLACloudConnector.SILAServerMessage clientMessage) {
                result.add(clientMessage);
                latch.countDown();
            }

            @Override
            public void onError(Throwable throwable) {
                fail();
            }

            @Override
            public void onCompleted() {
                latch.countDown();
            }
        };
    }
}