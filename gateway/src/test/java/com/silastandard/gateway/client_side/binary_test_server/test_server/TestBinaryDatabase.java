package com.silastandard.gateway.client_side.binary_test_server.test_server;

import com.silastandard.gateway.utils.BytesGenerator;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.UUID;

public class TestBinaryDatabase implements BinaryDatabase {
    private static final Duration BINARY_EXTEND_DURATION = Duration.ofMinutes(10);
    @Override
    public Binary getBinary(UUID uuid) {
        return new Binary(new Blob() {
            @Override
            public long length() {
                return 100;
            }

            @Override
            public byte[] getBytes(long pos, int length) {
                return new byte[0];
            }

            @Override
            public InputStream getBinaryStream() {
                return null;
            }

            @Override
            public long position(byte[] pattern, long start) {
                return 0;
            }

            @Override
            public long position(Blob pattern, long start) {
                return 0;
            }

            @Override
            public int setBytes(long pos, byte[] bytes) {
                return 0;
            }

            @Override
            public int setBytes(long pos, byte[] bytes, int offset, int len) {
                return 0;
            }

            @Override
            public OutputStream setBinaryStream(long pos) {
                return null;
            }

            @Override
            public void truncate(long len) {

            }

            @Override
            public void free() {

            }

            @Override
            public InputStream getBinaryStream(long pos, long length)  {
                byte[] source = BytesGenerator.getRandomBytes((int) length);
                return new ByteArrayInputStream(source);
            }
        }, new BinaryInfo(uuid, OffsetDateTime.now(), 100));
    }

    @Override
    public BinaryInfo getBinaryInfo(UUID uuid) throws BinaryDatabaseException {
        return new BinaryInfo(uuid, OffsetDateTime.now(), 100);
    }

    @Override
    public Duration addBinary(UUID uuid, InputStream inputStream) throws BinaryDatabaseException {
        final OffsetDateTime expirationDate = OffsetDateTime.now().plus(BINARY_EXTEND_DURATION);
        return Duration.between(OffsetDateTime.now(), expirationDate);
    }

    @Override
    public Duration extendBinaryExpiration(UUID uuid) throws BinaryDatabaseException {
        final OffsetDateTime newExpiration = getBinaryInfo(uuid).getExpiration().plus(BINARY_EXTEND_DURATION);
        return Duration.between(OffsetDateTime.now(), newExpiration);
    }

    @Override
    public void removeAllBinaries() throws BinaryDatabaseException {

    }

    @Override
    public void removeBinary(UUID uuid) throws BinaryDatabaseException {

    }

    @Override
    public void reserveBytes(long l) throws BinaryDatabaseException {

    }

    @Override
    public void close() throws Exception {

    }
}
