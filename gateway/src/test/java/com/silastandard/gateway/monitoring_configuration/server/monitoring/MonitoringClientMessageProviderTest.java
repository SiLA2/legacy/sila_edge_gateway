package com.silastandard.gateway.monitoring_configuration.server.monitoring;

import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.*;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.utils.BytesGenerator;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.examples.test_server.TestServer;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAInteger;

import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import client.messages.utils.ServerInfo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.*;
import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MonitoringClientMessageProviderTest {

    private GatewayServices gatewayServices;
    final String clientName = "Client name";

    private static final int TIMEOUT = 60;
    private final int testServerPort = 50100;
    private final ServerInfo serverInfo = new ServerInfo(Paths.get("./config/testserver1.cfg"), testServerPort);
    private UUID testServerUUID;
    private TestServer testServer;

    @BeforeAll
    void beforeAll() throws IOException {
        log.info("Start set up test server");
        gatewayServices = new GatewayServices();
        testServer = new TestServer(
                ArgumentHelperBuilder.build(testServerPort, "TestServerTest",serverInfo.getConfigFile().getPath(), "local"));
        testServerUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(gatewayServices.getServerManagerService(), testServerUUID, Server.Status.ONLINE, TIMEOUT);
        log.info("Set up test server");
    }

    @AfterAll
    void afterAll() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        testServer.close();
        waitServerState(gatewayServices.getServerManagerService(), testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        gatewayServices.getServerManagerService().close();
    }

    @BeforeEach
    void beforeEach() {
        gatewayServices.getMonitoringSubscriptionService().clear();
    }

    @Test
    void onCommandExecutionMessageTest() throws InvalidProtocolBufferException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/ListProvider";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CommandExecution.NAME);
            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage> executionMessageParser = GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage commandExecution = executionMessageParser.parseFrom(message.getMessage().getPayload());

                assertEquals(fullyQualifiedCommandId, commandExecution.getCommandExecutionMessage().getCommandIdentifier().getValue());

                List<GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct> parameterList =
                        commandExecution.getCommandExecutionMessage().getCommandParametersList();

                assertEquals(2, parameterList.size());

                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct param_1 = parameterList.get(0);
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct param_2 = parameterList.get(1);
                assertEquals("ListLength", param_1.getName().getValue());
                assertEquals("ElementLength", param_2.getName().getValue());
                assertEquals(new JsonParser().parse("{\"value\":\"10\"}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(param_1.getValue()))));
                assertEquals(new JsonParser().parse("{\"value\":\"8\"}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(param_2.getValue()))));
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CommandExecutionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for CommandExecution
        UnobservableCommandTestOuterClass.ListProvider_Parameters parameters = UnobservableCommandTestOuterClass.ListProvider_Parameters.newBuilder()
                .setElementLength(SiLAInteger.from(8))
                .setListLength(SiLAInteger.from(10))
                .build();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId ,parameters.toByteString());

        DynamicMessage requestMessage = DynamicMessage.parseFrom(parameters.getDescriptorForType(), parameters.toByteString());

        gatewayServices.getMonitoringClientMessageProvider().onCommandExecutionMessage(siLAGatewayCall, requestMessage);
    }

    @Test
    void onCommandInitiationMessageTest() throws InvalidProtocolBufferException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CommandInitiation.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage> initiationMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage commandInitiationMessage = initiationMessageParser.parseFrom(message.getMessage().getPayload());
                assertEquals(fullyQualifiedCommandId, commandInitiationMessage.getCommandInitiationMessage().getCommandIdentifier().getValue());

                List<GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct.CommandParameters_Struct> parameterList =
                        commandInitiationMessage.getCommandInitiationMessage().getCommandParametersList();
                assertEquals(1, parameterList.size());

                GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct.CommandParameters_Struct parameter = parameterList.get(0);

                assertEquals("UpdateDevice", parameter.getName().getValue());
                assertEquals(new JsonParser().parse("{\"value\":true}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(parameter.getValue()))));
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CommandInitiationMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for CommandInitiation
        ObservableCommandTestOuterClass.RestartDevice_Parameters parameters = ObservableCommandTestOuterClass.RestartDevice_Parameters
                .newBuilder()
                .setUpdateDevice(SiLABoolean.from(true)).build();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId ,parameters.toByteString());

        DynamicMessage requestMessage = DynamicMessage.parseFrom(parameters.getDescriptorForType(), parameters.toByteString());
        gatewayServices.getMonitoringClientMessageProvider().onCommandInitiationMessage(siLAGatewayCall, requestMessage);
    }

    @Test
    void onCommandExecutionInfoSubscriptionMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";
        UUID commandExecutionUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CommandExecutionInfoSubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage> infoSubscriptionMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage commandExecutionInfoSubscriptionMessage =
                        infoSubscriptionMessageParser.parseFrom(message.getMessage().getPayload());
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage.CommandExecutionInfoSubscriptionMessage_Struct actualMessage =
                        commandExecutionInfoSubscriptionMessage.getCommandExecutionInfoSubscriptionMessage();
                assertEquals(commandExecutionUUID.toString(), actualMessage.getCommandExecutionUUID().getValue());
                assertEquals(fullyQualifiedCommandId, actualMessage.getCommandIdentifier().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CommandExecutionInfoSubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for ExecutionInfoSubscription
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandExecutionInfoSubscription(SiLACloudConnector.CommandExecutionInfoSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID.toString()).build())
                        .build());
        SiLAFramework.CommandExecutionUUID executionUUID = clientMessage.getCommandExecutionInfoSubscription().getExecutionUUID();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, executionUUID.getValueBytes());
        gatewayServices.getMonitoringClientMessageProvider().onCommandExecutionInfoSubscriptionMessage(siLAGatewayCall, executionUUID.getValue());
    }

    @Test
    void onCommandIntermediateResponseSubscriptionMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";
        final UUID commandExecutionUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CommandIntermediateResponseSubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage> infoSubscriptionMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage commandIntermediateResponseSubscriptionMessage =
                        infoSubscriptionMessageParser.parseFrom(message.getMessage().getPayload());
                GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage.CommandIntermediateResponseSubscriptionMessage_Struct actualMessage =
                        commandIntermediateResponseSubscriptionMessage.getCommandIntermediateResponseSubscriptionMessage();
                assertEquals(commandExecutionUUID.toString(), actualMessage.getCommandExecutionUUID().getValue());
                assertEquals(fullyQualifiedCommandId, actualMessage.getCommandIdentifier().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse commandIntermediateResponseSubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.emptyList(), monitoringMessageListener);

        // Build SILAClientMessage for CommandIntermediateResponseSubscriptionMessage
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandIntermediateResponseSubscription(SiLACloudConnector.CommandIntermediateResponseSubscription.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID.toString()).build())
                        .build());
        SiLAFramework.CommandExecutionUUID executionUUID = clientMessage.getCommandIntermediateResponseSubscription().getExecutionUUID();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, executionUUID.getValueBytes());
        gatewayServices.getMonitoringClientMessageProvider().onCommandIntermediateResponseSubscriptionMessage(siLAGatewayCall, executionUUID.getValue());
    }

    @Test
    void onCommandGetResponseMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";
        final UUID commandExecutionUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener =                                                                                                                                                                      intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CommandGetResponse.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage> commandGetResponseMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage commandGetResponseMessage =
                        commandGetResponseMessageParser.parseFrom(message.getMessage().getPayload());
                GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.CommandGetResponseMessage_Struct actualResponse =
                        commandGetResponseMessage.getCommandGetResponseMessage();
                assertEquals(commandExecutionUUID.toString(), actualResponse.getCommandExecutionUUID().getValue());
                assertEquals(fullyQualifiedCommandId, actualResponse.getCommandIdentifier().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse commandIntermediateResponseSubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for CommandGetResponseMessage
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCommandGetResponse(SiLACloudConnector.CommandGetResponse.newBuilder()
                        .setExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID.toString()).build())
                        .build());
        SiLAFramework.CommandExecutionUUID executionUUID = clientMessage.getCommandGetResponse().getExecutionUUID();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, executionUUID.getValueBytes());
        gatewayServices.getMonitoringClientMessageProvider().onCommandGetResponseMessage(siLAGatewayCall, executionUUID.getValue());
    }

    @Test
    void onMetaDataRequestMessageTest() {
        String fullyQualifiedMetadataId = "ch.unitelabs/test/UnobservableCommandTest/v1/Metadata/User";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, MetaDataRequest.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage> metadataRequestMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage metadataRequestMessage =
                        metadataRequestMessageParser.parseFrom(message.getMessage().getPayload());
                assertEquals(fullyQualifiedMetadataId, metadataRequestMessage.getGetFCPAffectedByMetadataRequestMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse MetaDataRequestMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for MetaDataRequestMessage
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setMetadataRequest(SiLACloudConnector.GetFCPAffectedByMetadataRequest.newBuilder()
                        .setFullyQualifiedMetadataId(fullyQualifiedMetadataId)
                        .build());
        SiLACloudConnector.GetFCPAffectedByMetadataRequest metadataRequest = clientMessage.getMetadataRequest();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedMetadataId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onMetaDataRequestMessage(siLAGatewayCall, metadataRequest.getFullyQualifiedMetadataId());
    }

    @Test
    void onPropertyReadMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, PropertyRead.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage> propertyReadMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage propertyReadMessage =
                        propertyReadMessageParser.parseFrom(message.getMessage().getPayload());
                assertEquals(fullyQualifiedPropertyId, propertyReadMessage.getPropertyReadMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse PropertyReadMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for PropertyReadMessage
        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertyRead(SiLACloudConnector.PropertyRead.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build());
        SiLACloudConnector.PropertyRead propertyRead = clientMessage.getPropertyRead();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onPropertyReadMessage(siLAGatewayCall, propertyRead.getFullyQualifiedPropertyId());
    }

    @Test
    void onPropertySubscriptionMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, PropertySubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage> propertySubscriptionMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage propertySubscriptionMessage =
                        propertySubscriptionMessageParser.parseFrom(message.getMessage().getPayload());
                assertEquals(fullyQualifiedPropertyId, propertySubscriptionMessage.getPropertySubscriptionMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse PropertySubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        // Build SILAClientMessage for PropertySubscriptionMessage
        SiLACloudConnector.SILAClientMessage.Builder clientMessage  = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setPropertySubscription(SiLACloudConnector.PropertySubscription.newBuilder()
                        .setFullyQualifiedPropertyId(fullyQualifiedPropertyId)
                        .build());
        SiLACloudConnector.PropertySubscription propertySubscription = clientMessage.getPropertySubscription();

        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onPropertySubscriptionMessage(siLAGatewayCall, propertySubscription.getFullyQualifiedPropertyId());
    }

    @Test
    void onCancelCommandExecutionInfoSubscriptionMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";
        final UUID requestUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CancelCommandExecutionInfoSubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage> parser =
                        GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage cancelCommandExecutionInfoSubscriptionMessage =
                        parser.parseFrom(message.getMessage().getPayload());
                assertEquals(requestUUID.toString(), cancelCommandExecutionInfoSubscriptionMessage.getCancelCommandExecutionInfoSubscriptionMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CancelCommandExecutionInfoSubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onCancelCommandExecutionInfoSubscriptionMessage(siLAGatewayCall, requestUUID.toString());
    }

    @Test
    void onCancelCommandIntermediateResponseSubscriptionMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";
        final UUID requestUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CancelCommandIntermediateResponseSubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage> parser =
                        GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage cancelCommandIntermediateResponseSubscriptionMessage =
                        parser.parseFrom(message.getMessage().getPayload());
                assertEquals(requestUUID.toString(), cancelCommandIntermediateResponseSubscriptionMessage.getCancelCommandIntermediateResponseSubscriptionMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CancelCommandIntermediateResponseSubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onCancelCommandIntermediateResponseSubscriptionMessage(siLAGatewayCall, requestUUID.toString());
    }

    @Test
    void onCancelPropertySubscriptionMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString";
        final UUID requestUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CancelPropertySubscription.NAME);

            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage> cancelPropertySubscriptionMessageParser =
                        GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage cancelPropertySubscriptionMessage =
                        cancelPropertySubscriptionMessageParser.parseFrom(message.getMessage().getPayload());
                assertEquals(requestUUID.toString(), cancelPropertySubscriptionMessage.getCancelPropertySubscriptionMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CancelPropertySubscriptionMessage");
            }
        };

        // add Listener to monitor messages
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        // Call MonitoringMessageProvider
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringClientMessageProvider().onCancelPropertySubscriptionMessage(siLAGatewayCall, requestUUID.toString());
    }


    @Test
    void onCreateBinaryUploadRequestTest() {
        final String fullyQualifiedCommandParameterId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee/Parameter/Sugar";
        final long binarySize = 200;
        final int chunkCount = 20;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, CreateBinaryUploadRequest.NAME);

            try {
                GatewayMonitoringServiceOuterClass.DataType_CreateBinaryUploadRequestMessage createBinaryUploadRequestMessage =
                        GatewayMonitoringServiceOuterClass.DataType_CreateBinaryUploadRequestMessage.parseFrom(message.getMessage().getPayload());
                GatewayMonitoringServiceOuterClass.DataType_CreateBinaryUploadRequestMessage.CreateBinaryUploadRequestMessage_Struct message_struct = createBinaryUploadRequestMessage.getCreateBinaryUploadRequestMessage();

                assertEquals(fullyQualifiedCommandParameterId, message_struct.getParameterIdentifier().getValue());
                assertEquals(binarySize, message_struct.getBinarySize().getValue());
                assertEquals(chunkCount, message_struct.getChunkCount().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse CreateBinaryUploadRequestMessage");
            }
        };
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.CreateBinaryRequest createBinaryRequest = SiLABinaryTransfer.CreateBinaryRequest.newBuilder()
                .setBinarySize(binarySize)
                .setChunkCount(chunkCount)
                .setParameterIdentifier(fullyQualifiedCommandParameterId)
                .build();
        gatewayServices.getMonitoringClientMessageProvider().onCreateBinaryUploadRequest(siLAGatewayCall, createBinaryRequest);
    }

    @Test
    void onUploadChunkRequestTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();
        final int chunkIndex = 3;
        final ByteString payload = ByteString.copyFrom(BytesGenerator.getRandomBytes(5));


        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, UploadChunkRequest.NAME);

            try {
                GatewayMonitoringServiceOuterClass.DataType_UploadChunkRequestMessage uploadChunkRequestMessage =
                        GatewayMonitoringServiceOuterClass.DataType_UploadChunkRequestMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_UploadChunkRequestMessage.UploadChunkRequestMessage_Struct message_struct = uploadChunkRequestMessage.getUploadChunkRequestMessage();
                assertEquals(binaryTransferUUID.toString(), message_struct.getBinaryTransferUUID().getValue());
                assertEquals(chunkIndex, message_struct.getChunkIndex().getValue());
                assertEquals(payload, message_struct.getPayload().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse UploadChunkRequestMessage");
            }
        };
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.UploadChunkRequest uploadChunkRequest = SiLABinaryTransfer.UploadChunkRequest.newBuilder()
                .setBinaryTransferUUID(binaryTransferUUID.toString())
                .setChunkIndex(chunkIndex)
                .setPayload(payload)
                .build();
        gatewayServices.getMonitoringClientMessageProvider().onUploadChunkRequest(siLAGatewayCall, uploadChunkRequest);
    }

    @Test
    void onDeleteBinaryRequestTest() {
        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, DeleteUploadedBinaryRequest.NAME);

            try {
                GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryRequestMessage deleteBinaryRequestMessage =
                        GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryRequestMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryRequestMessage.newBuilder().setDeleteBinaryRequestMessage(SiLAString.from("")).build(), deleteBinaryRequestMessage);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse DeleteBinaryRequestMessage");
            }
        };
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        gatewayServices.getMonitoringClientMessageProvider().onDeleteUploadedBinaryRequest(siLAGatewayCall, SiLABinaryTransfer.DeleteBinaryRequest.getDefaultInstance());
    }

    @Test
    void onGetBinaryInfoRequestTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, GetBinaryInfoRequest.NAME);

            try {
                GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoRequestMessage getBinaryInfoRequestMessage =
                        GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoRequestMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(binaryTransferUUID.toString(), getBinaryInfoRequestMessage.getGetBinaryInfoRequestMessage().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse GetBinaryInfoRequestMessage");
            }
        };
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.GetBinaryInfoRequest getBinaryInfoRequest = SiLABinaryTransfer.GetBinaryInfoRequest.newBuilder().setBinaryTransferUUID(binaryTransferUUID.toString()).build();
        gatewayServices.getMonitoringClientMessageProvider().onGetBinaryInfoRequest(siLAGatewayCall, getBinaryInfoRequest);
    }

    @Test
    void onGetChunkRequestTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();
        final int offset = 3;
        final int length = 5;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                    checkClientRequestMessage(intermediateResponse, GetChunkRequest.NAME);

            GatewayMonitoringServiceOuterClass.DataType_GetChunkRequestMessage getChunkRequestMessage;
            try {
                getChunkRequestMessage = GatewayMonitoringServiceOuterClass.DataType_GetChunkRequestMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_GetChunkRequestMessage.GetChunkRequestMessage_Struct message_struct = getChunkRequestMessage.getGetChunkRequestMessage();
                assertEquals(binaryTransferUUID.toString(), message_struct.getBinaryTransferUUID().getValue());
                assertEquals(offset, message_struct.getOffset().getValue());
                assertEquals(length, message_struct.getLength().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse GetChunkRequestMessage");
            }
        };
        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.GetChunkRequest uploadChunkRequest = SiLABinaryTransfer.GetChunkRequest.newBuilder()
                .setBinaryTransferUUID(binaryTransferUUID.toString())
                .setOffset(offset)
                .setLength(length)
                .build();
        gatewayServices.getMonitoringClientMessageProvider().onGetChunkRequest(siLAGatewayCall, uploadChunkRequest);
    }

    private GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct checkClientRequestMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses intermediateResponse, String expectedMessageType) {
        assertTrue(intermediateResponse.hasClientRequestMessage());
        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                intermediateResponse.getClientRequestMessage();
        assertEquals(expectedMessageType, message.getMessageType().getValue());
        assertEquals(clientName, message.getSiLAClient().getValue());
        assertEquals(testServerUUID.toString(), message.getSiLAServer().getValue());
        return message;
    }
}
