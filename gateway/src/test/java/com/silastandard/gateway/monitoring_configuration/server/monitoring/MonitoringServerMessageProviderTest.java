package com.silastandard.gateway.monitoring_configuration.server.monitoring;

import client.messages.utils.ServerInfo;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types.*;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.utils.BytesGenerator;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila_java.examples.test_server.TestServer;
import sila_java.examples.test_server.impl.UnobservableProperty;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.*;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MonitoringServerMessageProviderTest {

    private GatewayServices gatewayServices;
    final String clientName = "Client name";

    private static final int TIMEOUT = 60;
    private final int testServerPort = 50100;
    private final ServerInfo serverInfo = new ServerInfo(Paths.get("./config/testserver1.cfg"), testServerPort);
    private UUID testServerUUID;
    private TestServer testServer;

    @BeforeAll
    void beforeAll() throws IOException {
        log.info("Start set up test server");
        gatewayServices = new GatewayServices();
        testServer = new TestServer(
                ArgumentHelperBuilder.build(testServerPort, "TestServerTest",serverInfo.getConfigFile().getPath(), "local"));
        testServerUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(gatewayServices.getServerManagerService(), testServerUUID, Server.Status.ONLINE, TIMEOUT);
        log.info("Set up test server");
    }

    @AfterAll
    void afterAll() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        testServer.close();
        waitServerState(gatewayServices.getServerManagerService(), testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        gatewayServices.getServerManagerService().close();
    }

    @BeforeEach
    void beforeEach() {
        gatewayServices.getMonitoringSubscriptionService().clear();
    }

    @Test
    void onCommandResponseMessageTest() {
        final String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, CommandResponse.NAME);
            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage> parser = GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage commandResponse = parser.parseFrom(message.getMessage().getPayload());

                assertEquals(fullyQualifiedCommandId, commandResponse.getCommandResponseMessage().getCommandIdentifier().getValue());

                SiLAFramework.Any anyResponse = commandResponse.getCommandResponseMessage().getCommandResponseList().get(0).getValue();

                assertEquals("Result", commandResponse.getCommandResponseMessage().getCommandResponseList().get(0).getIdentifier().getValue());
                assertEquals( "{\"value\":\"Done\"}",  new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(anyResponse))).toString());
            } catch (Exception e) {
                fail("Couldn't parse CommandResponseMessage");
            }
        };

        UnobservableCommandTestOuterClass.MakeCoffee_Responses response = UnobservableCommandTestOuterClass.MakeCoffee_Responses.newBuilder()
                .setResult(SiLAString.from("Done"))
                .build();

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        try {
            DynamicMessage responseMessage = DynamicMessage.parseFrom(response.getDescriptorForType(), response.toByteString());
            SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, response.toByteString());

            gatewayServices.getMonitoringServerMessageProvider().onCommandResponseMessage(siLAGatewayCall, responseMessage);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse MakeCoffee_Responses to DynamicMessage");
        }
    }

    @Test
    void onObservableCommandConfirmationMessageTest() {
        final String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        final UUID commandExecutionUUID = UUID.randomUUID();
        final long seconds = 10;
        final long nanos = 1;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ObservableCommandConfirmation.NAME);
            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage> parser = GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage commandConfirmation = parser.parseFrom(message.getMessage().getPayload());

                assertEquals(commandExecutionUUID.toString(), commandConfirmation.getObservableCommandConfirmationMessage().getCommandExecutionUUID().getValue());
                assertEquals(seconds, commandConfirmation.getObservableCommandConfirmationMessage().getLifetimeOfExecution().getDuration().getSeconds().getValue());
                assertEquals(nanos, commandConfirmation.getObservableCommandConfirmationMessage().getLifetimeOfExecution().getDuration().getNanos().getValue());
            } catch (Exception e) {
                fail("Couldn't parse into ObservableCommandConfirmationMessage");
            }
        };

        SiLAFramework.CommandConfirmation commandConfirmation = SiLAFramework.CommandConfirmation.newBuilder()
                .setCommandExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(commandExecutionUUID.toString()).build())
                .setLifetimeOfExecution(SiLAFramework.Duration.newBuilder()
                        .setSeconds(seconds)
                        .setNanos((int)nanos)
                        .build())
                .build();

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onObservableCommandConfirmationMessage(siLAGatewayCall, commandConfirmation.toByteString());
    }

    @Test
    void onObservableCommandExecutionInfoMessage() throws InvalidProtocolBufferException {
        final String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        final UUID executionUUID = UUID.randomUUID();
        final long lifetimeSeconds = 20;
        final String expectedCommandStatus = "finishedSuccessfully";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ObservableCommandExecutionInfo.NAME);
            try {
                Parser<GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage> parser = GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.parser();
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage commandExecutionInfo = parser.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.ObservableCommandExecutionInfoMessage_Struct infoMessage =
                        commandExecutionInfo.getObservableCommandExecutionInfoMessage();

                assertEquals(executionUUID.toString(), infoMessage.getCommandExecutionUUID().getValue());
                assertEquals(expectedCommandStatus, infoMessage.getExecutionInfo().getCommandExecutionStatus().getValue());
                assertEquals(lifetimeSeconds, infoMessage.getExecutionInfo().getUpdatedLifetimeOfExecution().getDuration().getSeconds().getValue());
                assertEquals(0, infoMessage.getExecutionInfo().getUpdatedLifetimeOfExecution().getDuration().getNanos().getValue());
                assertEquals(2, infoMessage.getExecutionInfo().getProgressInfo().getValue());

            } catch (Exception e) {
                fail("Couldn't parse ObservableCommandConfirmationMessage");
            }
        };

        final SiLAFramework.ExecutionInfo executionInfo = SiLAFramework.ExecutionInfo.newBuilder()
                .setCommandStatus(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully)
                .setUpdatedLifetimeOfExecution(SiLAFramework.Duration.newBuilder().setSeconds(lifetimeSeconds).build())
                .setEstimatedRemainingTime(SiLAFramework.Duration.newBuilder().setSeconds(10).setNanos(1).build()) // optional
                .setProgressInfo(SiLAReal.from(2)) //optional
                .build();

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
        DynamicMessage responseMessage = DynamicMessage.parseFrom(executionInfo.getDescriptorForType(),executionInfo.toByteString());
        gatewayServices.getMonitoringServerMessageProvider().onObservableCommandExecutionInfoMessage(siLAGatewayCall, executionUUID.toString(), responseMessage);
    }

    @Test
    void onObservableCommandIntermediateResponseMessage() {
        final UUID executionUUID = UUID.randomUUID();
        final String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";

        ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses expectedResponse = ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses.newBuilder()
                .setFileBeingUpdated(SiLAString.from("Done"))
                .build();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ObservableCommandIntermediateResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.parseFrom(message.getMessage().getPayload());

                String commandExecutionID = response.getObservableCommandIntermediateResponseMessage().getCommandExecutionUUID().getValue();
                assertEquals(executionUUID.toString(), commandExecutionID);

                SiLAFramework.Any result = response.getObservableCommandIntermediateResponseMessage().getResult(0).getValue();

                assertEquals("FileBeingUpdated", response.getObservableCommandIntermediateResponseMessage().getResultList().get(0).getIdentifier().getValue());
                assertEquals( "{\"value\":\"Done\"}",  new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(result))).toString());

            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse ObservableCommandIntermediateResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        try {
            DynamicMessage messageResponse = DynamicMessage.parseFrom(expectedResponse.getDescriptorForType(), expectedResponse.toByteString());
            SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
            gatewayServices.getMonitoringServerMessageProvider().onObservableCommandIntermediateResponseMessage(siLAGatewayCall, executionUUID.toString(), messageResponse);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse RestartDevice_IntermediateResponses to DynamicMessage");
        }
    }

    @Test
    void onObservableCommandResponseMessage() {
        final UUID executionUUID = UUID.randomUUID();
        final String fullyQualifiedCommandId = "ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice";

        ObservableCommandTestOuterClass.RestartDevice_Responses expectedResponse = ObservableCommandTestOuterClass.RestartDevice_Responses.getDefaultInstance();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ObservableCommandResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.parseFrom(message.getMessage().getPayload());

                String commandExecutionID = response.getObservableCommandResponseMessage().getCommandExecutionUUID().getValue();
                assertEquals(executionUUID.toString(), commandExecutionID);

                GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.ObservableCommandResponseMessage_Struct s = response.getObservableCommandResponseMessage();
                assertEquals(0, s.getResultCount());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse ObservableCommandResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        try {
            DynamicMessage messageResponse = DynamicMessage.parseFrom(expectedResponse.getDescriptorForType(), expectedResponse.toByteString());
            SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedCommandId, ByteString.EMPTY);
            gatewayServices.getMonitoringServerMessageProvider().onObservableCommandResponseMessage(siLAGatewayCall, executionUUID.toString(), messageResponse);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse RestartDevice_IntermediateResponses to DynamicMessage");
        }
    }

    @Test
    void onMetaDataResponseMessageTest() throws InvalidProtocolBufferException {
        String fullyQualifiedMetadataId = "ch.unitelabs/test/UnobservableCommandTest/v1/Metadata/User";
        final String metadataAffectedFullyId_1 = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        final String metadataAffectedFullyId_2 = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/DrinkCoffee";
        List<String> expectedList = Arrays.asList(metadataAffectedFullyId_1, metadataAffectedFullyId_2);

        UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses expectedResponse =
                UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses.newBuilder()
                        .addAffectedCalls(SiLAString.from(metadataAffectedFullyId_1))
                        .addAffectedCalls(SiLAString.from(metadataAffectedFullyId_2))
                .build();
        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, GetFCPAffectedByMetadataResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage.parseFrom(message.getMessage().getPayload());
                List<SiLAFramework.String> responseList = response.getGetFCPAffectedByMetadataResponseMessageList();
                List<String> actualList = responseList.stream().map(SiLAFramework.String::getValue).collect(Collectors.toList());
                assertEquals(expectedList, actualList);

            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse MetaDataResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedMetadataId, ByteString.EMPTY);
        DynamicMessage responseMessage = DynamicMessage.parseFrom(expectedResponse.getDescriptorForType(), expectedResponse.toByteString());
        gatewayServices.getMonitoringServerMessageProvider().onMetaDataResponseMessage(siLAGatewayCall, responseMessage);
    }

    @Test
    void onPropertyValueMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, PropertyValue.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(fullyQualifiedPropertyId, response.getPropertyValueMessage().getPropertyIdentifier().getValue());

                SiLAFramework.Any responseAny = response.getPropertyValueMessage().getPropertyValue();

                new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(responseAny)));

                UnobservablePropertyTestOuterClass.Get_ListString_Responses actualResponse =
                       UnobservablePropertyTestOuterClass.Get_ListString_Responses.parseFrom(responseAny.getPayload());
                assertEquals(UnobservableProperty.LIST_STRING_RESPONSE, actualResponse);

                assertEquals(new JsonParser().parse("{\"value\":[{\"value\":\"White\"},{\"value\":\"Rabbit\"}]}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(responseAny))));
            } catch (Exception e) {
                fail("Couldn't parse ValueMessageProperty");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        try {
            DynamicMessage responseMessage = DynamicMessage.parseFrom(UnobservableProperty.LIST_STRING_RESPONSE.getDescriptorForType(), UnobservableProperty.LIST_STRING_RESPONSE.toByteString());
            gatewayServices.getMonitoringServerMessageProvider().onPropertyValueMessage(siLAGatewayCall, responseMessage);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse Get_ListString_Responses to DynamicMessage");
        }
    }


    @Test
    void onPropertyBooleanValueMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/Boolean";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, PropertyValue.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(fullyQualifiedPropertyId, response.getPropertyValueMessage().getPropertyIdentifier().getValue());

                SiLAFramework.Any responseAny = response.getPropertyValueMessage().getPropertyValue();

                assertEquals(new JsonParser().parse("{\"value\":false}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(responseAny))));
            } catch (Exception e) {
                fail("Couldn't parse ValueMessageProperty");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        try {
            DynamicMessage responseMessage = DynamicMessage.parseFrom(UnobservablePropertyTestOuterClass.Get_Boolean_Responses.getDescriptor(),UnobservablePropertyTestOuterClass.Get_Boolean_Responses.newBuilder().setBoolean(SiLABoolean.from(false)).build().toByteString());
            gatewayServices.getMonitoringServerMessageProvider().onPropertyValueMessage(siLAGatewayCall, responseMessage);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse Get_Boolean_Responses to DynamicMessage");
        }
    }

    @Test
    void onObservablePropertyValueMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString";

        List<SiLAFramework.String> list = Arrays.asList(SiLAString.from("Element 0"), SiLAString.from("Element 1"), SiLAString.from("Element 3"));
        ObservablePropertyTestOuterClass.Subscribe_ListString_Responses expectedResponse = ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.newBuilder().addAllListString(list).build();

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ObservablePropertyValue.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage response = GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage.parser().parseFrom(message.getMessage().getPayload());
                assertEquals(fullyQualifiedPropertyId, response.getObservablePropertyValueMessage().getPropertyIdentifier().getValue());
                SiLAFramework.Any responseAny = response.getObservablePropertyValueMessage().getPropertyValue();
                ObservablePropertyTestOuterClass.Subscribe_ListString_Responses actualResponse = ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.parseFrom(responseAny.getPayload());
                assertEquals(expectedResponse, actualResponse);
                assertEquals(new JsonParser().parse("{\"value\":[{\"value\":\"Element 0\"},{\"value\":\"Element 1\"},{\"value\":\"Element 3\"}]}"), new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(responseAny))));
            } catch (Exception e) {
                fail("Couldn't parse ObservablePropertyValueMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        try {
            DynamicMessage responseMessage = DynamicMessage.parseFrom(expectedResponse.getDescriptorForType(),expectedResponse.toByteString());
            gatewayServices.getMonitoringServerMessageProvider().onObservablePropertyValueMessage(siLAGatewayCall, responseMessage);
        } catch (InvalidProtocolBufferException e) {
            fail("Couldn't parse Get_ListString_Responses to DynamicMessage");
        }
    }

    @Test
    void onValidationErrorTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String errorParameter = "FeatureIdentifier";
        final String errorMessage = "FeatureIdentifier parameter was not set.";
        StatusRuntimeException validationError = SiLAErrors.generateValidationError(errorParameter, errorMessage);

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ErrorResponse.VALIDATION_ERROR_NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage actualResponse =
                        GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage.parseFrom(message.getMessage().getPayload());


                assertEquals(errorParameter, actualResponse.getValidationErrorMessage().getParameter().getValue());
                assertEquals(errorMessage, actualResponse.getValidationErrorMessage().getMessage().getValue());

            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse ValidationError");
            }

        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, validationError);
    }

    @Test
    void onUndefinedExecutionErrorTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String errorMessage = "Test Error Message";
        StatusRuntimeException undefinedExecutionError = SiLAErrors.generateUndefinedExecutionError(errorMessage);

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ErrorResponse.UNDEFINED_ERROR_NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(errorMessage, response.getUndefinedExecutionErrorMessage().getValue());
            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse UndefinedExecutionError");
            }

        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, undefinedExecutionError);
    }

    @Test
    void onDefinedExecutionErrorTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String errorId = "TestException";
        final String errorMessage = "Nothing to do, this will always throw an exception";
        StatusRuntimeException definedExecutionError = SiLAErrors.generateDefinedExecutionError(errorId, errorMessage);

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ErrorResponse.DEFINED_ERROR_NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(errorId, response.getDefinedExecutionErrorMessage().getErrorIdentifier().getValue());
                assertEquals(errorMessage, response.getDefinedExecutionErrorMessage().getMessage().getValue());

            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse DefinedExecutionError");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, definedExecutionError);
    }

    @Test
    void onFrameworkErrorTest() {

        final String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String expectedErrorType = "COMMAND_EXECUTION_NOT_ACCEPTED";
        final String expectedErrorMessage = "Test error message";
        StatusRuntimeException frameworkError = SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.COMMAND_EXECUTION_NOT_ACCEPTED, expectedErrorMessage);

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ErrorResponse.FRAMEWORK_ERROR_NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(expectedErrorType, response.getFrameworkErrorMessage().getErrorType().getValue());
                assertEquals(expectedErrorMessage, response.getFrameworkErrorMessage().getMessage().getValue());
            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse FrameworkError");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Arrays.asList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, frameworkError);

    }

    @Test
    void onConnectionErrorTest() {
        final String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException";
        final String expectedCauseMessage = Status.Code.CANCELLED.name();
        StatusRuntimeException connectionException = new StatusRuntimeException(Status.fromCode(Status.Code.CANCELLED));

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, ErrorResponse.CONNECTION_ERROR_NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(expectedCauseMessage, response.getConnectionErrorMessage().getValue());
            } catch (InvalidProtocolBufferException e) {
                fail("Couldn't parse ConnectionError");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName, fullyQualifiedPropertyId, ByteString.EMPTY);
        gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, connectionException);
    }

    @Test
    void onCreateBinaryResponseTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();
        final int seconds = 50;
        final int nanos = 0;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, CreateBinaryResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_CreateBinaryResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_CreateBinaryResponseMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_CreateBinaryResponseMessage.CreateBinaryResponseMessage_Struct message_struct = response.getCreateBinaryResponseMessage();
                assertEquals(binaryTransferUUID.toString(), message_struct.getBinaryTransferUUID().getValue());
                assertEquals(seconds, message_struct.getLifetimeOfBinary().getDuration().getSeconds().getValue());
                assertEquals(nanos, message_struct.getLifetimeOfBinary().getDuration().getNanos().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse ObservableCommandResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = SiLABinaryTransfer.CreateBinaryResponse.newBuilder()
                .setBinaryTransferUUID(binaryTransferUUID.toString())
                .setLifetimeOfBinary(SiLAFramework.Duration.newBuilder().setSeconds(seconds).setNanos(nanos).build())
                .build();
        gatewayServices.getMonitoringServerMessageProvider().onCreateBinaryResponse(siLAGatewayCall, createBinaryResponse);
    }

    @Test
    void onUploadChunkResponseTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();
        final int seconds = 50;
        final int nanos = 0;
        final int chunkIndex = 2;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, UploadChunkResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_UploadChunkResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_UploadChunkResponseMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_UploadChunkResponseMessage.UploadChunkResponseMessage_Struct message_struct = response.getUploadChunkResponseMessage();
                assertEquals(binaryTransferUUID.toString(), message_struct.getBinaryTransferUUID().getValue());
                assertEquals(seconds, message_struct.getLifetimeOfBinary().getDuration().getSeconds().getValue());
                assertEquals(nanos, message_struct.getLifetimeOfBinary().getDuration().getNanos().getValue());
                assertEquals(chunkIndex, message_struct.getChunkIndex().getValue());
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse UploadChunkResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse = SiLABinaryTransfer.UploadChunkResponse.newBuilder()
                .setBinaryTransferUUID(binaryTransferUUID.toString())
                .setLifetimeOfBinary(SiLAFramework.Duration.newBuilder().setSeconds(seconds).setNanos(nanos).build())
                .setChunkIndex(chunkIndex)
                .build();
        gatewayServices.getMonitoringServerMessageProvider().onUploadChunkResponse(siLAGatewayCall, uploadChunkResponse);
    }

    @Test
    void onDeleteBinaryResponseTest() {
        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, DeleteBinaryResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryResponseMessage.parseFrom(message.getMessage().getPayload());

                assertEquals(GatewayMonitoringServiceOuterClass.DataType_DeleteBinaryResponseMessage.newBuilder().setDeleteBinaryResponseMessage(SiLAString.from("")).build(), response);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse DeleteBinaryResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        gatewayServices.getMonitoringServerMessageProvider().onDeleteBinaryResponse(siLAGatewayCall);
    }

    @Test
    void onGetBinaryInfoResponseTest() {
        final int binarySize = 20;
        final int seconds = 50;
        final int nanos = 0;

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, GetBinaryInfoResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage expectedMessage = GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage.newBuilder()
                        .setGetBinaryInfoResponseMessage(GatewayMonitoringServiceOuterClass.DataType_GetBinaryInfoResponseMessage.GetBinaryInfoResponseMessage_Struct.newBuilder()
                                .setBinarySize(SiLAInteger.from(binarySize))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(seconds))
                                                .setNanos(SiLAInteger.from(nanos))
                                                .build())
                                        .build())
                                .build())
                        .build();


                assertEquals(expectedMessage,response);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse GetBinaryInfoResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.GetBinaryInfoResponse getBinaryInfoResponse = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder()
                .setBinarySize(binarySize)
                .setLifetimeOfBinary(SiLAFramework.Duration.newBuilder().setSeconds(seconds).setNanos(nanos).build())
                .build();
        gatewayServices.getMonitoringServerMessageProvider().onGetBinaryInfoResponse(siLAGatewayCall, getBinaryInfoResponse);
    }

    @Test
    void onGetChunkResponseTest() {
        final UUID binaryTransferUUID = UUID.randomUUID();
        final int seconds = 50;
        final int nanos = 0;
        final long offset = 0;
        final byte[] bytes = BytesGenerator.getRandomBytes(5);
        final ByteString payload = ByteString.copyFrom(bytes);

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, GetChunkResponse.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage expectedMessage = GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage.newBuilder()
                        .setGetChunkResponseMessage(GatewayMonitoringServiceOuterClass.DataType_GetChunkResponseMessage.GetChunkResponseMessage_Struct.newBuilder()
                                .setBinaryTransferUUID(SiLAString.from(binaryTransferUUID.toString()))
                                .setLifetimeOfBinary(GatewayMonitoringServiceOuterClass.DataType_Duration.newBuilder()
                                        .setDuration(GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct.newBuilder()
                                                .setSeconds(SiLAInteger.from(seconds))
                                                .setNanos(SiLAInteger.from(nanos))
                                                .build())
                                        .build())
                                .setOffset(SiLAInteger.from(offset))
                                .setPayload(SiLABinary.fromBytes(bytes))
                                .build())
                        .build();

                assertEquals(expectedMessage,response);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse GetChunkResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.GetChunkResponse getChunkResponse = SiLABinaryTransfer.GetChunkResponse.newBuilder()
                .setBinaryTransferUUID(binaryTransferUUID.toString())
                .setLifetimeOfBinary(SiLAFramework.Duration.newBuilder().setNanos(nanos).setSeconds(seconds).build())
                .setOffset(offset)
                .setPayload(payload)
                .build();
        gatewayServices.getMonitoringServerMessageProvider().onGetChunkResponse(siLAGatewayCall, getChunkResponse);
    }

    @Test
    void invalidBinaryTransferErrorTest() {
        final SiLABinaryTransfer.BinaryTransferError.ErrorType errorType = SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID;
        final String errorMessage = "test message";

        MonitoringSubscriptionService.MonitoringMessageListener monitoringMessageListener = intermediateResponse -> {
            GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                    checkServerRequestMessage(intermediateResponse, BinaryTransferError.NAME);
            try {
                GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage response =
                        GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage.parseFrom(message.getMessage().getPayload());

                GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage expectedMessage = GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage.newBuilder()
                        .setBinaryTransferErrorMessage(GatewayMonitoringServiceOuterClass.DataType_BinaryTransferErrorMessage.BinaryTransferErrorMessage_Struct.newBuilder()
                                .setErrorType(SiLAString.from(errorType.name()))
                                .setMessage(SiLAString.from(errorMessage))
                                .build())
                        .build();

                assertEquals(expectedMessage,response);
            } catch (Exception e) {
                e.printStackTrace();
                fail("Couldn't parse GetBinaryInfoResponseMessage");
            }
        };

        gatewayServices.getMonitoringSubscriptionService().addListener(Collections.singletonList(testServerUUID.toString()), monitoringMessageListener);
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientName);
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = SiLABinaryTransfer.BinaryTransferError.newBuilder()
                .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID)
                .setMessage(errorMessage)
                .build();
        gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError);
    }

    private GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct checkServerRequestMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses intermediateResponse, String expectedMessageType) {
        assertTrue(intermediateResponse.hasServerResponseMessage());
        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                intermediateResponse.getServerResponseMessage();
        assertEquals(expectedMessageType, message.getMessageType().getValue());
        assertEquals(clientName, message.getSiLAClient().getValue());
        assertEquals(testServerUUID.toString(), message.getSiLAServer().getValue());
        return message;
    }
}