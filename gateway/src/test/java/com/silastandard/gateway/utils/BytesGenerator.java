package com.silastandard.gateway.utils;

import java.util.Random;

public class BytesGenerator {
    /**
     * Allocate a byte array with random bytes
     * @param nbBytes The number of random bytes to allocate
     * @return a new byte array containing random bytes
     * @throws IllegalArgumentException if the number of bytes is below 1
     */
    public static byte[] getRandomBytes(final int nbBytes) {
        if (nbBytes < 1)
            throw new IllegalArgumentException("Number of bytes must be greater than or equal to 1");
        final byte[] random = new byte[nbBytes];
        new Random().nextBytes(random);
        return random;
    }
}
