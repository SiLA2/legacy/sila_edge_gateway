package com.silastandard.gateway.server_side.executor;

import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila_java.examples.test_server.TestServer;
import sila_java.examples.test_server.impl.UnobservableCommand;
import sila_java.examples.test_server.impl.UnobservableProperty;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import client.messages.utils.ServerInfo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GatewayRequestMessageTest {
    private static final int TIMEOUT = 60;
    private final int testServerPort = 50100;
    private final ServerInfo serverInfo = new ServerInfo(Paths.get("./config/testserver1.cfg"), 50100);
    private final String clientNameId = "client name";
    private final ServerManagerService serverManager = new ServerManagerService();
    private UUID testServerUUID;
    private TestServer testServer;


    @BeforeAll
    void beforeAll() throws IOException {
        log.info("Start set up test server");
        testServer = new TestServer(
                ArgumentHelperBuilder.build(testServerPort, "TestServerTest",serverInfo.getConfigFile().getPath(), "local"));
        testServerUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(serverManager, testServerUUID, Server.Status.ONLINE, TIMEOUT);

        log.info("Set up test server");
    }

    @AfterAll
    void cleanup() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        testServer.close();
        waitServerState(serverManager, testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        serverManager.close();
    }

    @Test
    void unobservablePropertyTest() throws InvalidProtocolBufferException {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString";
        final SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientNameId, fullyQualifiedPropertyId, ByteString.EMPTY);
        DynamicMessage result = serverManager.newCallService(siLAGatewayCall).executeGetUnobservableProperty(Collections.emptyList());
        final UnobservablePropertyTestOuterClass.Get_ListString_Responses.Builder builder = UnobservablePropertyTestOuterClass.Get_ListString_Responses.newBuilder();
        JsonFormat.parser().merge(ProtoMapper.serializeToJson(result), builder);
        final UnobservablePropertyTestOuterClass.Get_ListString_Responses response = builder.build();
        Assertions.assertEquals(UnobservableProperty.LIST_STRING_RESPONSE, response);
    }

    @Test
    void unobservableCommandTest() throws InvalidProtocolBufferException {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        UnobservableCommandTestOuterClass.MakeCoffee_Parameters parameters = UnobservableCommandTestOuterClass.MakeCoffee_Parameters.newBuilder()
                .setSugar(SiLABoolean.from(true))
                .build();

        final SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(testServerUUID, clientNameId, fullyQualifiedCommandId, ByteString.EMPTY);
        DynamicMessage request = serverManager.newCallService(siLAGatewayCall).getRequestMessage(siLAGatewayCall.getCallId());
        DynamicMessage result = serverManager.newCallService(siLAGatewayCall).executeUnobservableCommand(request, Collections.emptyList());

        final UnobservableCommandTestOuterClass.MakeCoffee_Responses.Builder builder = UnobservableCommandTestOuterClass.MakeCoffee_Responses.newBuilder();
        JsonFormat.parser().merge(ProtoMapper.serializeToJson(result), builder);
        final UnobservableCommandTestOuterClass.MakeCoffee_Responses response = builder.build();
        Assertions.assertEquals(UnobservableCommand.MAKE_COFFEE_RESPONSE, response);
    }
}
