package client.messages.utils;

import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila_java.library.core.asynchronous.MethodPoller;
import sila_java.library.manager.models.Server;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ScanUtilsForTest {
    public static void scanAndWaitServerState(
            @NonNull final ServerManagerService serverManagerService,
            @NonNull final UUID serverId,
            @NonNull final Server.Status status,
            final int timeout
    ) {
        ServerFinderGateway
                .filterBy(serverManagerService,ServerFinderGateway.Filter.uuid(serverId), ServerFinderGateway.Filter.status(status))
                .scanAndFindOne(Duration.ofSeconds(timeout))
                .orElseThrow(RuntimeException::new);
    }

    public static void waitServerState(
            @NonNull final ServerManagerService serverManagerService,
            @NonNull final UUID serverId,
            @NonNull final Server.Status status,
            final int timeout
    ) throws TimeoutException, ExecutionException {
        MethodPoller
                .await()
                .atMost(Duration.ofSeconds(timeout))
                .withInterval(Duration.ofMillis(100))
                .until(() -> ServerFinderGateway
                        .filterBy(serverManagerService, ServerFinderGateway.Filter.uuid(serverId), ServerFinderGateway.Filter.status(status))
                        .findOne()
                        .isPresent());
    }
}
