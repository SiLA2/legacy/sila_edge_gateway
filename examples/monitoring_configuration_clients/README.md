## Example monitoring and configuration clients
The clients are used to test and demonstrate the monitoring and configuration functions of the gateway.
The executable jars can be launched from the root directory of the project.

&nbsp;

#### Configuration Client
A client to test the GatewayConfigurationService Feature.

Start the configuration client:
- `java -jar examples/monitoring_configuration_clients/target/ConfigurationClient-exec.jar`

---

#### Monitoring Client
A client to test the GatewayMonitoringService Feature. This client just monitors the messages. 
To see messages, a server-initiated Client must communicate with SiLA servers, all of which are connected to the gateway.
The example [Multiple Server Client ](../test_implementations/src/main/java/client/TestMultipleServerClient.java)  with the corresponding SiLA Servers can be used for this scenario.

Start the monitoring client:
- `java -jar examples/monitoring_configuration_clients/target/MonitoringClient-exec.jar`

---

#### TextUserInterface Client
A text user interface client for testing the configuration and monitoring functionalities. It is not complete for all possible calls.
Use [ConnectionConfigurationProvider](src/main/java/connection_configurattion/ConnectionConfigurationProvider.java) to change the default connection settings.

Start the TUITest client:
- `java -jar examples/monitoring_configuration_clients/target/TUITestClient-exec.jar`