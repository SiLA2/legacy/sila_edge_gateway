package monitoring_client;

import io.grpc.ManagedChannel;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceGrpc;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;

import java.util.Iterator;
import java.util.List;

public class MonitoringProvider {
    
    private final GatewayMonitoringServiceGrpc.GatewayMonitoringServiceBlockingStub blockingStub;

    public MonitoringProvider(final ManagedChannel channel) {
        this.blockingStub = GatewayMonitoringServiceGrpc.newBlockingStub(channel);
    }

    public SiLAFramework.CommandConfirmation observerCurrentMessagesCommand(List<SiLAFramework.String> uuidList) {
        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters.Builder parameters =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Parameters.newBuilder();
        parameters.addAllServerUUIDList(uuidList);
        return blockingStub.observeCurrentMessages(parameters.build());
    }

    public Iterator<SiLAFramework.ExecutionInfo> currentMessageInfo(SiLAFramework.CommandExecutionUUID commandExecutionUUID) {
        return blockingStub.observeCurrentMessagesInfo(commandExecutionUUID);
    }

    public Iterator<GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses> observeCurrentMessagesIntermediate(SiLAFramework.CommandExecutionUUID commandExecutionUUID) {
        return blockingStub.observeCurrentMessagesIntermediate(commandExecutionUUID);
    }

    public GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_Responses observeCurrentMessagesResult(SiLAFramework.CommandExecutionUUID commandExecutionUUID) {
        return blockingStub.observeCurrentMessagesResult(commandExecutionUUID);
    }
}
