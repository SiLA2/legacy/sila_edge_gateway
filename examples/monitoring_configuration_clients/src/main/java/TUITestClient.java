import configuration_client.ConfigurationProvider;
import connection_configurattion.ConnectionConfigurationProvider;
import io.grpc.ManagedChannel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import monitoring_client.MonitoringClientExample;
import monitoring_client.MonitoringProvider;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * A text user interface client for testing the configuration and monitoring functionalities.
 */
@Slf4j
public class TUITestClient {
    private ConnectionConfigurationProvider connectionConfigurationProvider;
    private MonitoringProvider monitoringProvider;
    private ConfigurationProvider configurationProvider;

    private final String gateway_host = "localhost";
    private final int gateway_port = 50400;
    private static final String NEWLINE = System.getProperty("line.separator");

    public static void main(String[] args) {
        TUITestClient testClient = new TUITestClient();
        testClient.connect();
    }

    @SneakyThrows
    public void connect() {
        final ManagedChannel serviceChannel = ChannelFactory.withEncryption(gateway_host, gateway_port);

        try {
            final SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc.newBlockingStub(serviceChannel);

            log.info("Found Features:");
            final List<SiLAFramework.String> featureIdentifierList = serviceStub
                    .getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.newBuilder().build())
                    .getImplementedFeaturesList();

            featureIdentifierList.forEach(featureIdentifier ->
                    log.info("\t" + featureIdentifier.getValue())
            );

            connectionConfigurationProvider = new ConnectionConfigurationProvider(serviceChannel);
            monitoringProvider = new MonitoringProvider(serviceChannel);
            configurationProvider = new ConfigurationProvider(serviceChannel);

            handleInput();

        } finally {
            serviceChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }

    @SneakyThrows
    private void handleInput() {
        // reads input on the tui continuously until user quits
        Thread.sleep(2000);
        boolean go = true;
        Scanner scanner = new Scanner(System.in);
        while (go) {
            printTUI();
            String next = scanner.next();
            switch (next) {
                case "q":
                    go = false;
                    break;
                case "t":
                    configurationProvider.triggerServerDiscovery();
                    break;
                case "s":
                    configurationProvider.getServers();
                    break;
                case "i":
                    configurationProvider.getServers().forEach(configurationProvider::getServerInformation);
                    break;
                case "f":
                    configurationProvider.getServers().forEach(configurationProvider::getImplementedFeature);
                    break;
                case "c":
                    connectionConfigurationProvider.connectUpstream();
                    break;
                case "g":
                    connectionConfigurationProvider.getConfiguredClients();
                    break;
                case "d":
                    connectionConfigurationProvider.disconnectClient();
                    break;
                case "dis":
                    connectionConfigurationProvider.disableServerInitiatedConnectionMode();
                    break;
                case "en":
                    connectionConfigurationProvider.enableServerInitiatedConnectionMode();
                    break;
                case "m":
                    List<SiLAFramework.String> serverUUIDs = configurationProvider.getServers();
                    if(serverUUIDs.isEmpty()) {
                        System.out.println("No Server found in network!");
                        return;
                    }
                    MonitoringClientExample.monitorMessageInGateway(serverUUIDs, monitoringProvider);
                    break;
            }
        }
    }

    public void printTUI() {
        log.info(NEWLINE + NEWLINE +
                "Server Configuration calls:" + NEWLINE +
                "t - trigger server discovery" + NEWLINE +
                "s - get servers" + NEWLINE +
                "i - get server information" + NEWLINE +
                "f - get implemented features" + NEWLINE + NEWLINE +
                "Client Connection Configuration calls:" + NEWLINE +
                "c - connect gateway to client" + NEWLINE +
                "d - disconnect client from gateway" + NEWLINE +
                "g - get connected client" + NEWLINE +
                "dis - disable Server Initiated Connection Mode" + NEWLINE +
                "en - enable Server Initiated Connection Mode" + NEWLINE + NEWLINE +
                "Monitoring:" + NEWLINE +
                "m - monitor messages" + NEWLINE + NEWLINE +
                "Quit:"+ NEWLINE +
                "q - quit" + NEWLINE
        );
    }
}