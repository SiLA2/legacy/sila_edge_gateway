package configuration_client;

import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.gateway.gateway.gatewayconfigurationservice.v1.GatewayConfigurationServiceGrpc;
import sila2.org.gateway.gateway.gatewayconfigurationservice.v1.GatewayConfigurationServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.ExceptionGeneration;
import sila_java.library.core.sila.types.SiLAString;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ConfigurationProvider {
    private final GatewayConfigurationServiceGrpc.GatewayConfigurationServiceBlockingStub blockingStub;

    public ConfigurationProvider(final ManagedChannel channel) {
        this.blockingStub = GatewayConfigurationServiceGrpc.newBlockingStub(channel);
    }

    public void triggerServerDiscovery() {
        System.out.println("Will try to scan network..");

        blockingStub.triggerServerDiscovery(GatewayConfigurationServiceOuterClass.TriggerServerDiscovery_Parameters.newBuilder().build());

        System.out.println("Scanned network successful: ");
    }

    public List<SiLAFramework.String> getServers() {
        GatewayConfigurationServiceOuterClass.Get_Servers_Parameters.Builder parameter = GatewayConfigurationServiceOuterClass.Get_Servers_Parameters.newBuilder();
        GatewayConfigurationServiceOuterClass.Get_Servers_Responses response;

        try {
            response = blockingStub.getServers(parameter.build());
        } catch (StatusRuntimeException e) {
            log.error(ExceptionGeneration.generateMessage(e));
            return null;
        }
        List<SiLAFramework.String> servers = response.getServersList();
        System.out.println("Servers Response:\n"  + servers);
        return servers;
    }

    public GatewayConfigurationServiceOuterClass.GetServerInformation_Responses.ServerInformation_Struct getServerInformation(SiLAFramework.String serverUUID) {
        GatewayConfigurationServiceOuterClass.GetServerInformation_Parameters parameter = GatewayConfigurationServiceOuterClass.GetServerInformation_Parameters.newBuilder()
                .setServerUUID(serverUUID)
                .build();
        GatewayConfigurationServiceOuterClass.GetServerInformation_Responses response = blockingStub.getServerInformation(parameter);
        System.out.println("Server Information Response:\n" + response.getServerInformation().toString());
        return response.getServerInformation();
    }

    public List<String> getImplementedFeature(SiLAFramework.String serverUUID) {
        GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Parameters parameter = GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Parameters.newBuilder()
                .setServerUUID(serverUUID)
                .build();

        GatewayConfigurationServiceOuterClass.GetImplementedFeatures_Responses response = blockingStub.getImplementedFeatures(parameter);
        List<String> implFeatureList = response.getImplementedFeaturesList().stream().map(SiLAFramework.String::getValue).collect(Collectors.toList());
        System.out.println("Implemented Feature Response:\n" + implFeatureList);
        return implFeatureList;
    }

    public String getFeatureDefinition(String featureIdentifier) {
        GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Parameters parameter = GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Parameters.newBuilder()
                .setFeatureIdentifier(SiLAString.from(featureIdentifier))
                .build();
        GatewayConfigurationServiceOuterClass.GetFeatureDefinition_Responses response = blockingStub.getFeatureDefinition(parameter);
        return response.getFeatureDefinition().getValue();
    }
}
