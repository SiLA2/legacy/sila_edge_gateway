package client.messages;

import client.messages.models.client_message_types.*;
import client.messages.models.server_messages_types.*;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.*;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types.*;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class MessageParserService {
    public ClientRequestMessage parseSiLAClientMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message_struct) {
        ClientRequestMessage.ClientRequestMessageBuilder clientRequestMessage = ClientRequestMessage.builder();
        clientRequestMessage.fromSiLAClient(message_struct.getSiLAClient().getValue());
        clientRequestMessage.toSiLAServer(message_struct.getSiLAServer().getValue());
        clientRequestMessage.messageType(message_struct.getMessageType().getValue());

        String messageName = message_struct.getMessageType().getValue();
        ByteString payload = message_struct.getMessage().getPayload();

        try {
            switch (messageName) {
                case CommandExecution.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage commandExecution =
                            GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CommandExecutionMessage(commandExecution));
                    break;
                case PropertyRead.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage propertyRead =
                            GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage.parseFrom(payload);
                    clientRequestMessage.message(new PropertyReadMessage(propertyRead));
                    break;
                case PropertySubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage propertySubscription =
                            GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new PropertySubscriptionMessage(propertySubscription));
                    break;
                case MetaDataRequest.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage metadataRequest =
                            GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage.parseFrom(payload);
                    clientRequestMessage.message(new MetaDataRequestMessage(metadataRequest));
                    break;
                case CommandIntermediateResponseSubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage intermediateResponseSubscription =
                            GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CommandIntermediateResponseSubscriptionMessage(intermediateResponseSubscription));
                    break;
                case CommandInitiation.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage commandInitiation =
                            GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.parseFrom(payload);
                    clientRequestMessage.message(new CommandInitiationMessage(commandInitiation));
                    break;
                case CommandGetResponse.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage commandGetResponse =
                            GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.parseFrom(payload);
                    clientRequestMessage.message(new CommandGetResponseMessage(commandGetResponse));
                    break;
                case CommandExecutionInfoSubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage commandInfoSubscription =
                            GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CommandExecutionInfoSubscriptionMessage(commandInfoSubscription));
                    break;
                case CancelPropertySubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage cancelPropertySubscriptionMessage =
                            GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CancelPropertySubscriptionMessage(cancelPropertySubscriptionMessage));
                    break;
                case CancelCommandIntermediateResponseSubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage cancelCommandIntermediate =
                            GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CancelCommandIntermediateResponseSubscriptionMessage(cancelCommandIntermediate));
                    break;
                case CancelCommandExecutionInfoSubscription.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage cancelCommandInfo =
                            GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage.parseFrom(payload);
                    clientRequestMessage.message(new CancelCommandExecutionInfoSubscriptionMessage(cancelCommandInfo));
                    break;
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return clientRequestMessage.build();
    }

    public ServerResponseMessage parseSiLAServerMessage(GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message_struct) {

        ServerResponseMessage.ServerResponseMessageBuilder serverResponseMessage = ServerResponseMessage.builder();
        serverResponseMessage.fromSiLAServer(message_struct.getSiLAServer().getValue());
        serverResponseMessage.toSiLAClient(message_struct.getSiLAClient().getValue());

        String messageType = message_struct.getMessageType().getValue();
        ByteString payload = message_struct.getMessage().getPayload();
        try {
            switch (messageType) {
                case CommandResponse.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage responseMessage =
                            GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.parseFrom(payload);
                    serverResponseMessage.message(new CommandResponseMessage(responseMessage));
                    break;
                case ObservableCommandConfirmation.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage confirmationMessage =
                            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage.parseFrom(payload);
                    serverResponseMessage.message(new ObservableCommandConfirmationMessage(confirmationMessage));
                    break;
                case ObservableCommandExecutionInfo.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage commandExecutionInfoMessage =
                            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.parseFrom(payload);
                    serverResponseMessage.message(new ObservableCommandExecutionInfoMessage(commandExecutionInfoMessage));
                    break;
                case ObservableCommandIntermediateResponse.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage commandIntermediateResponseMessage =
                            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.parseFrom(payload);
                    serverResponseMessage.message(new ObservableCommandIntermediateResponseMessage(commandIntermediateResponseMessage));
                    break;
                case ObservableCommandResponse.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage observableCommandResponse =
                            GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.parseFrom(payload);
                    serverResponseMessage.message(new ObservableCommandResponseMessage(observableCommandResponse));
                    break;
                case GetFCPAffectedByMetadataResponse.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage getFCPAffectedByMetadata =
                            GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage.parseFrom(payload);
                    serverResponseMessage.message(new GetFCPAffectedByMetadataResponseMessage(getFCPAffectedByMetadata));
                    break;
                case PropertyValue.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage propertyValueMessage = GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage.parseFrom(payload);
                    serverResponseMessage.message(new PropertyValueMessage(propertyValueMessage));
                    break;
                case ObservablePropertyValue.NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage observablePropertyValue =
                            GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage.parseFrom(payload);
                    serverResponseMessage.message(new ObservablePropertyValueMessage(observablePropertyValue));
                    break;
                case ErrorResponse.VALIDATION_ERROR_NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage validationErrorMessage =
                            GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage.parseFrom(payload);
                    serverResponseMessage.message(new ValidationErrorMessage(validationErrorMessage));
                    break;
                case ErrorResponse.DEFINED_ERROR_NAME:
                    GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage definedExecutionErrorMessage =
                            GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.parseFrom(payload);
                    serverResponseMessage.message(new DefinedExecutionErrorMessage(definedExecutionErrorMessage));
                    break;
                case ErrorResponse.UNDEFINED_ERROR_NAME:
                    GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage undefinedExecutionErrorMessage =
                            GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage.parseFrom(payload);
                    serverResponseMessage.message(new UndefinedExecutionErrorMessage(undefinedExecutionErrorMessage));
                    break;
                case ErrorResponse.FRAMEWORK_ERROR_NAME:
                    GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage frameworkErrorMessage =
                            GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage.parseFrom(payload);
                    serverResponseMessage.message(new FrameworkErrorMessage(frameworkErrorMessage));
                    break;
                case ErrorResponse.CONNECTION_ERROR_NAME:
                    GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage connectionErrorMessage =
                            GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage.parseFrom(payload);
                    serverResponseMessage.message(new ConnectionErrorMessage(connectionErrorMessage));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serverResponseMessage.build();
    }
}
