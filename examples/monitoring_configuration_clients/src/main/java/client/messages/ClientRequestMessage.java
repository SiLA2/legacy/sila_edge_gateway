package client.messages;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;


@Builder
@Getter
public class ClientRequestMessage {
    private final String fromSiLAClient;
    private final String toSiLAServer;
    private final String messageType;
    private final ClientRequestMessageTypes message;

    public ClientRequestMessage(@NonNull String fromSiLAClient, @NonNull String toSiLAServer, @NonNull String messageType, @NonNull ClientRequestMessageTypes message) {
        this.fromSiLAClient = fromSiLAClient;
        this.toSiLAServer = toSiLAServer;
        this.messageType = messageType;
        this.message = message;
    }

    public interface ClientRequestMessageTypes {
        String toJson();
    }

    @Override
    public String toString() {
        return String.format("From SiLA Client: %s\nTo SiLA Server: %s\nMessage:%s\n",
                fromSiLAClient,
                toSiLAServer,
                message);
    }
}
