package client.messages.utils;

import com.google.gson.JsonParser;
import lombok.SneakyThrows;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.SiLAAny;

/**
 * Utility Functions for Files including Resources and Configurations
 */
public class ParserUtils {
    /**
     * Parse SiLAFramework.Any datatype to Json String
     *
     * @param any Datatype Any which will be parsed into json
     */
    @SneakyThrows
    public static String getJsonFromAny(SiLAFramework.Any any) {
        return new JsonParser().parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(any))).toString();
    }
}
