package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class FrameworkErrorMessage extends ServerMessage {
    private final String errorType;
    private final String errorMessage;

    public FrameworkErrorMessage(GatewayMonitoringServiceOuterClass.DataType_FrameworkErrorMessage message) {
        this.errorType = message.getFrameworkErrorMessage().getErrorType().getValue();
        this.errorMessage = message.getFrameworkErrorMessage().getMessage().getValue();
    }

    @Override
    public String toString() {
        return "FrameworkErrorMessage{" +
                "\nError Type : " + errorType +
                "\nError Message : " + errorMessage +
                "\n}";
    }
}