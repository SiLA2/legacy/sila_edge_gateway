package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;

public class PropertyValueMessage extends ServerMessage {
    private final String fullyQualifiedPropertyIdentifier;
    private final String propertyValue;

    public PropertyValueMessage(GatewayMonitoringServiceOuterClass.DataType_PropertyValueMessage message) {
        this.fullyQualifiedPropertyIdentifier = message.getPropertyValueMessage().getPropertyIdentifier().getValue();
        this.propertyValue = ParserUtils.getJsonFromAny(message.getPropertyValueMessage().getPropertyValue());
    }

    @Override
    public String toString() {
        return "ObservablePropertyValueMessage{" +
                "\nPropertyIdentifier : " + fullyQualifiedPropertyIdentifier +
                "\nPropertyValue : " + propertyValue +
                "\n}";
    }
}