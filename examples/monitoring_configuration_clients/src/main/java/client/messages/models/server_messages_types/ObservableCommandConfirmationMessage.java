package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class ObservableCommandConfirmationMessage extends ServerMessage{
    private final String commandExecutionUUID;
    private final GatewayMonitoringServiceOuterClass.DataType_Duration.Duration_Struct lifetimeOfExecution;

    public ObservableCommandConfirmationMessage(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandConfirmationMessage message) {
        this.commandExecutionUUID = message.getObservableCommandConfirmationMessage().getCommandExecutionUUID().getValue();
        this.lifetimeOfExecution = message.getObservableCommandConfirmationMessage().getLifetimeOfExecution().getDuration();
    }

    @Override
    public String toString() {
        return "ObservableCommandConfirmationMessage{" +
                "\nCommandExecutionUUID : " + commandExecutionUUID +
                "\nLifetimeOfExecution : " + lifetimeOfExecution +
                "\n}";
    }
}
