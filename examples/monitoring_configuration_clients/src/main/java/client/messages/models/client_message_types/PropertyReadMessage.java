package client.messages.models.client_message_types;

import lombok.NonNull;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class PropertyReadMessage extends ClientMessage {
    private final String propertyIdentifier;

    public PropertyReadMessage(@NonNull GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage message) {
        this.propertyIdentifier = message.getPropertyReadMessage().getValue();
    }

    @Override
    public String toString() {
        return "PropertyReadMessage{" +
                "\nProperty Identifier : " +  propertyIdentifier +
                "\n}";
    }
}