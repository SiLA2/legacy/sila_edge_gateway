package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CancelCommandExecutionInfoSubscriptionMessage extends ClientMessage {
    private final String requestUUID;

    public CancelCommandExecutionInfoSubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CancelCommandExecutionInfoSubscriptionMessage message) {
        this.requestUUID = message.getCancelCommandExecutionInfoSubscriptionMessage().getValue();
    }
    
    @Override
    public String toString() {
        return "CancelCommandExecutionInfoSubscriptionMessage{" +
                "\nRequestUUID : " + requestUUID +
                "\n}";
    }
}
