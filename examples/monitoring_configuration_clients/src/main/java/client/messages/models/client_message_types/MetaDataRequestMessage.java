package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class MetaDataRequestMessage extends ClientMessage {
    private final String fullyQualifiedMetadataIdentifier;

    public MetaDataRequestMessage(GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataRequestMessage message) {
        this.fullyQualifiedMetadataIdentifier = message.getGetFCPAffectedByMetadataRequestMessage().getValue();
    }

    public String toString() {
        return "MetaDataRequestMessage{" +
                "\nMetadataIdentifier : " + fullyQualifiedMetadataIdentifier +
                "\n}";
    }
}
