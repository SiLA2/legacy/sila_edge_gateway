package client.messages.models.server_messages_types;

import lombok.SneakyThrows;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;
import client.messages.utils.ParserUtils;

import java.util.Map;
import java.util.stream.Collectors;

public class CommandResponseMessage extends ServerMessage {
    private final String fullyQualifiedCommandIdentifier;
    private final Map<String, Object> response;

    public CommandResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage message) {
        GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct payload= message.getCommandResponseMessage();
        this.fullyQualifiedCommandIdentifier = payload.getCommandIdentifier().getValue();
        response = payload.getCommandResponseList().stream().collect(Collectors.toMap(r -> r.getIdentifier().getValue(), r-> ParserUtils.getJsonFromAny(r.getValue())));
    }

    @SneakyThrows
    @Override
    public String toString() {
        return "CommandResponseMessage{" +
                "\nCommand Identifier: " + fullyQualifiedCommandIdentifier +
                "\n Command Response:\n" + response +
                "\n}";
    }
}