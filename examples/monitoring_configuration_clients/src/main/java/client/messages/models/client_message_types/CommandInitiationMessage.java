package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CommandInitiationMessage extends ClientMessage {
    private final String commandIdentifier;
    private final List<String> parameters;

    public CommandInitiationMessage(GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage message) {
        GatewayMonitoringServiceOuterClass.DataType_CommandInitiationMessage.CommandInitiationMessage_Struct payload = message.getCommandInitiationMessage();
        this.commandIdentifier = payload.getCommandIdentifier().getValue();
        this.parameters = payload.getCommandParametersList().stream().map(param -> param.getName().getValue() + ParserUtils.getJsonFromAny(param.getValue())).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "CommandInitiationMessage{" +
                "\nCommandIdentifier : " + commandIdentifier +
                "\nParameter : " + parameters +
                "\n}";
    }
}
