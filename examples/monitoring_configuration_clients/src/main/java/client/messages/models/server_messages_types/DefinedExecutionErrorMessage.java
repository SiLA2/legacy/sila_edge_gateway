package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class DefinedExecutionErrorMessage extends ServerMessage {
    private final String errorMessage;
    private final String errorIdentifier;

    public DefinedExecutionErrorMessage(GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage message) {
        this.errorIdentifier = message.getDefinedExecutionErrorMessage().getErrorIdentifier().getValue();
        this.errorMessage = message.getDefinedExecutionErrorMessage().getMessage().getValue();
    }

    @Override
    public String toString() {
        return "DefinedExecutionErrorMessage{" +
                "\nError Identifier : " + errorIdentifier +
                "\nError Message : " + errorMessage +
                "\n}";
    }
}