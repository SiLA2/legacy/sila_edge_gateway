package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class ConnectionErrorMessage extends ServerMessage {
    private final String errorMessage;

    public ConnectionErrorMessage(GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage message) {
        this.errorMessage = message.getConnectionErrorMessage().getValue();
    }

    @Override
    public String toString() {
        return "ConnectionErrorMessage{" +
                "\nMessage : " + errorMessage +
                "\n}";
    }
}