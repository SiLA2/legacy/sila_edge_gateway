package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;

import java.util.Map;
import java.util.stream.Collectors;

public class ObservableCommandIntermediateResponseMessage extends ServerMessage{
   private final String commandExecutionUUID;
   private final Map<String, Object> response;

   public ObservableCommandIntermediateResponseMessage(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage message) {
      GatewayMonitoringServiceOuterClass.DataType_ObservableCommandIntermediateResponseMessage.ObservableCommandIntermediateResponseMessage_Struct payload= message.getObservableCommandIntermediateResponseMessage();
      this.commandExecutionUUID = message.getObservableCommandIntermediateResponseMessage().getCommandExecutionUUID().getValue();
      response = payload.getResultList().stream().collect(Collectors.toMap(r -> r.getIdentifier().getValue(), r-> ParserUtils.getJsonFromAny(r.getValue())));
   }

   @Override
   public String toString() {
      return "ObservableCommandIntermediateResponseMessage{" +
              "\nCommandExecutionUUID : " + commandExecutionUUID +
              "\nResponse:\n" + response +
              "\n}";
   }
}
