package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CommandIntermediateResponseSubscriptionMessage extends ClientMessage{
    private final String commandIdentifier;
    private final String commandExecutionUUID;

    public CommandIntermediateResponseSubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CommandIntermediateResponseSubscriptionMessage message) {
        this.commandIdentifier = message.getCommandIntermediateResponseSubscriptionMessage().getCommandIdentifier().getValue();
        this.commandExecutionUUID = message.getCommandIntermediateResponseSubscriptionMessage().getCommandExecutionUUID().getValue();
    }

    @Override
    public String toString() {
        return "CommandIntermediateResponseSubscriptionMessage{" +
                "\nCommandIdentifier : " + commandIdentifier +
                "\nCommandExecutionUUID : " +  commandExecutionUUID +
                "\n}";
    }
}