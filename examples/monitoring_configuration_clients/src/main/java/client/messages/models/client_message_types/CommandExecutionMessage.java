package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandExecutionMessage extends ClientMessage {
    private final String commandIdentifier;
    private final Map<Object, Object> parameters;


    public CommandExecutionMessage(GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage message) {
        GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct payload = message.getCommandExecutionMessage();
        this.commandIdentifier = payload.getCommandIdentifier().getValue();
        this.parameters = payload.getCommandParametersList().stream().collect(Collectors.toMap(p -> p.getName().getValue(), p-> ParserUtils.getJsonFromAny(p.getValue())));
    }

    @Override
    public String toString() {
        return "CommandExecutionMessage{" +
                "\nCommand Identifier : " + commandIdentifier +
                "\nCommand Parameter :\n " + parameters +
                "\n}";
    }
}