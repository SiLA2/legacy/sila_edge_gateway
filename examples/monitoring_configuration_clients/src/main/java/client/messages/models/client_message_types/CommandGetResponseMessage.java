package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CommandGetResponseMessage extends ClientMessage {
    private final String commandIdentifier;
    private final String commandExecutionUUID;

    public CommandGetResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage message) {
        this.commandIdentifier = message.getCommandGetResponseMessage().getCommandIdentifier().getValue();
        this.commandExecutionUUID = message.getCommandGetResponseMessage().getCommandExecutionUUID().getValue();
    }

    @Override
    public String toString() {
        return "CommandGetResponseMessage{" +
                "\nCommandIdentifier : " + commandIdentifier +
                "\nCommandExecutionUUID : " +  commandExecutionUUID +
                "\n}";
    }
}