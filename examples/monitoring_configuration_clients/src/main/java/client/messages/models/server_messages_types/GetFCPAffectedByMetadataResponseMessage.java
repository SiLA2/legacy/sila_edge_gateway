package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import java.util.List;
import java.util.stream.Collectors;

public class GetFCPAffectedByMetadataResponseMessage extends ServerMessage {
    private final List<String> fullyQualifiedIdentifierList;

    public GetFCPAffectedByMetadataResponseMessage(GatewayMonitoringServiceOuterClass.DataType_GetFCPAffectedByMetadataResponseMessage message) {
        this.fullyQualifiedIdentifierList = message.getGetFCPAffectedByMetadataResponseMessageList().stream().map(s -> s.getValue()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "GetFCPAffectedByMetadataResponseMessage{" +
                "\nFullyQualifiedList : \n" + fullyQualifiedIdentifierList +
                "\n}";
    }
}
