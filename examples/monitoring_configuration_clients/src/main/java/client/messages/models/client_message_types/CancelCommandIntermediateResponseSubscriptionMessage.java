package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CancelCommandIntermediateResponseSubscriptionMessage extends ClientMessage {
    private final String requestUUID;

    public CancelCommandIntermediateResponseSubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CancelCommandIntermediateResponseSubscriptionMessage message) {
        this.requestUUID = message.getCancelCommandIntermediateResponseSubscriptionMessage().getValue();
    }

    @Override
    public String toString() {
        return "CancelCommandIntermediateResponseSubscriptionMessage{" +
                "\nRequestUUID : " + requestUUID +
                "\n}";
    }
}

