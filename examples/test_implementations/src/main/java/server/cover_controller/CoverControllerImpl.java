package server.cover_controller;

import io.grpc.stub.StreamObserver;

import sila2.org.silastandard.instruments.covercontroller.v1.CoverControllerGrpc;
import sila2.org.silastandard.instruments.covercontroller.v1.CoverControllerOuterClass;
import sila_java.library.core.sila.types.SiLAString;

public class CoverControllerImpl extends CoverControllerGrpc.CoverControllerImplBase {
    String state = "CLOSED";
    @Override
    public void openCover(CoverControllerOuterClass.OpenCover_Parameters request,
                          StreamObserver<CoverControllerOuterClass.OpenCover_Responses> responseObserver) {
        CoverControllerOuterClass.OpenCover_Responses result =
                CoverControllerOuterClass.OpenCover_Responses
                        .newBuilder()
                        .build();
        state = "OPEN";

        responseObserver.onNext(result);
        responseObserver.onCompleted();
    }

    @Override
    public void closeCover(CoverControllerOuterClass.CloseCover_Parameters request,
                           StreamObserver<CoverControllerOuterClass.CloseCover_Responses> responseObserver) {
        CoverControllerOuterClass.CloseCover_Responses result =
                CoverControllerOuterClass.CloseCover_Responses
                        .newBuilder()
                        .build();
        state = "CLOSED";

        responseObserver.onNext(result);
        responseObserver.onCompleted();
    }

    @Override
    public void getCoverState(CoverControllerOuterClass.Get_CoverState_Parameters request, StreamObserver<CoverControllerOuterClass.Get_CoverState_Responses> responseObserver) {
        responseObserver.onNext(
                CoverControllerOuterClass.Get_CoverState_Responses
                        .newBuilder()
                        .setCoverState(SiLAString.from(state))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
