package server.timer;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.core.utils.FileUtils.getResourceContent;
import static sila_java.library.core.utils.Utils.blockUntilStop;

/**
 * Example of a minimal SiLA Server
 */
@Slf4j
public class TimerServer implements AutoCloseable {
    // Every SiLA Server needs to define a type
    public static final String SERVER_TYPE = "Timer Server";

    // SiLA Server constructed in constructor
    private final SiLAServer server;

    /**
     * Application Class using command line arguments
     *
     * @param argumentHelper Custom Argument Helper
     */
    TimerServer(@NonNull final ArgumentHelper argumentHelper) {
        /*
        Start the minimum SiLA Server with the Meta Information
        and the gRPC Server Implementations (found below)
          */
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "TimeServer Example for testing the Gateway",
                "www.sila-standard.org",
                "v0.0"
        );

        try {
            /*
            A configuration file has to be given if the developer wants to persist server configurations
            (such as the generated UUID)
             */
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            /*
            Additional optional arguments are used, if no port is given it's automatically chosen, if
            no network interface is given, discovery is not enabled.
             */
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);


            builder.addFeature(
                    getResourceContent("TimerService.sila.xml"),
                    new TimerImpl()
            );

            this.server = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            log.error("Something went wrong when building / starting server", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.server.close();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        try (final TimerServer helloSiLAServer = new TimerServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(helloSiLAServer::close));
            blockUntilStop();
        }

        log.info("termination complete.");
    }
}
