package server.timer;


import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceGrpc;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

public class TimerImpl extends SilaTimerServiceGrpc.SilaTimerServiceImplBase {
    TimerService timerService = new TimerService();

    @Override
    public void startTimer(SilaTimerServiceOuterClass.StartTimer_Parameters request,
                           StreamObserver<SilaTimerServiceOuterClass.StartTimer_Responses> responseObserver) {
        SilaTimerServiceOuterClass.StartTimer_Responses response =  SilaTimerServiceOuterClass.StartTimer_Responses.newBuilder()
                .setTimerValue(SiLAInteger.from(timerService.getTimerValue()))
                .build();
        timerService.startTimer();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void stopTimer(SilaTimerServiceOuterClass.StopTimer_Parameters request,
                          StreamObserver<SilaTimerServiceOuterClass.StopTimer_Responses> responseObserver) {
        SilaTimerServiceOuterClass.StopTimer_Responses response = SilaTimerServiceOuterClass.StopTimer_Responses.newBuilder()
                .setTimerValue(SiLAInteger.from(timerService.getTimerValue()))
                .build();
        timerService.stopTimer();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void resetTimer(SilaTimerServiceOuterClass.ResetTimer_Parameters request,
                           StreamObserver<SilaTimerServiceOuterClass.ResetTimer_Responses> responseObserver) {
        timerService.resetTimer();
        SilaTimerServiceOuterClass.ResetTimer_Responses response = SilaTimerServiceOuterClass.ResetTimer_Responses.newBuilder()
                .setTimerValue(SiLAInteger.from(timerService.getTimerValue()))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getValue(SilaTimerServiceOuterClass.Get_Value_Parameters request,
                         StreamObserver<SilaTimerServiceOuterClass.Get_Value_Responses> responseObserver) {
        SilaTimerServiceOuterClass.Get_Value_Responses response = SilaTimerServiceOuterClass.Get_Value_Responses.newBuilder()
                .setValue(SiLAInteger.from(timerService.getTimerValue()))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getStartTime(SilaTimerServiceOuterClass.Get_StartTime_Parameters request,
                             StreamObserver<SilaTimerServiceOuterClass.Get_StartTime_Responses> responseObserver) {

        SilaTimerServiceOuterClass.Get_StartTime_Responses response = SilaTimerServiceOuterClass.Get_StartTime_Responses.newBuilder()
                .setStartTime(SiLAString.from(timerService.getStartTime()))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
