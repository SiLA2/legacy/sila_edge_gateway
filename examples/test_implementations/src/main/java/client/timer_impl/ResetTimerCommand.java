package client.timer_impl;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.command.UnobservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
@Getter
public class ResetTimerCommand extends UnobservableCommandImpl<SilaTimerServiceOuterClass.ResetTimer_Parameters, SilaTimerServiceOuterClass.ResetTimer_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/SilaTimerService/v1/Command/ResetTimer";

    @Builder
    public ResetTimerCommand(SilaTimerServiceOuterClass.ResetTimer_Parameters parameters, Consumer<SilaTimerServiceOuterClass.ResetTimer_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, resultCallback, errorCallback, SilaTimerServiceOuterClass.ResetTimer_Responses.parser());
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.ResetTimer_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError siLAError) {
            log.info("Error received: {}", siLAError);
    }
}
