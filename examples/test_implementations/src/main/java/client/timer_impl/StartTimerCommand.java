package client.timer_impl;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.command.UnobservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
public class StartTimerCommand extends UnobservableCommandImpl<SilaTimerServiceOuterClass.StartTimer_Parameters, SilaTimerServiceOuterClass.StartTimer_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/SilaTimerService/v1/Command/StartTimer";

    @Builder
    public StartTimerCommand(SilaTimerServiceOuterClass.StartTimer_Parameters parameters, Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, resultCallback, errorCallback, SilaTimerServiceOuterClass.StartTimer_Responses.parser());
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.StartTimer_Responses responses) {
        log.info("onResponse {}", responses.getTimerValue());
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.info("onError {}", error);
    }
}
