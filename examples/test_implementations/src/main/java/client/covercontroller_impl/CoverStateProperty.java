package client.covercontroller_impl;

import com.google.protobuf.ByteString;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.instruments.covercontroller.v1.CoverControllerOuterClass;
import sila_java.library.cloud.client.property.UnobservablePropertyImpl;

import java.util.function.Consumer;

@Slf4j
public class CoverStateProperty extends UnobservablePropertyImpl<CoverControllerOuterClass.Get_CoverState_Parameters, CoverControllerOuterClass.Get_CoverState_Responses> {
    private static final String PROPERTY_ID = "org.silastandard/instruments/CoverController/v1/Property/CoverState";

    @Builder
    public CoverStateProperty(CoverControllerOuterClass.Get_CoverState_Parameters parameters, Consumer<CoverControllerOuterClass.Get_CoverState_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, CoverControllerOuterClass.Get_CoverState_Responses.parser(), errorCallback);
    }

    @Override
    public void onResponse(CoverControllerOuterClass.Get_CoverState_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError siLAError)  {
        log.warn("error received: {}", siLAError);
    }

    @Override
    public ByteString getPayload() {
        CoverControllerOuterClass.Get_CoverState_Responses response = CoverControllerOuterClass.Get_CoverState_Responses.newBuilder().build();
        return response.toByteString();
    }
}
