package client.covercontroller_impl;

import sila2.org.silastandard.instruments.covercontroller.v1.CoverControllerOuterClass;
import sila_java.library.cloud.client.CloudConnectedServer;

import java.util.function.Consumer;

public class CoverControllerClient {
    private final CloudConnectedServer server;

    public CoverControllerClient(CloudConnectedServer server) {
        this.server = server;
    }

    public void openCover(Consumer<String> resultCallback) {
        OpenCoverCommand openCoverCommand = OpenCoverCommand.builder().parameters(CoverControllerOuterClass.CloseCover_Parameters.getDefaultInstance()).resultCallback(r -> resultCallback.accept("done")).build();
        server.getCommandExecutionService().executeCommand(openCoverCommand);
    }

    public void closeCover(Consumer<String> resultCallback) {
        CloseCoverCommand closeCoverCommand = CloseCoverCommand.builder().parameters(CoverControllerOuterClass.CloseCover_Parameters.newBuilder().getDefaultInstanceForType()).resultCallback(r -> resultCallback.accept("done")).build();
        server.getCommandExecutionService().executeCommand(closeCoverCommand);
    }

    public void getCoverState(Consumer<String> resultCallback) {
        CoverStateProperty coverStateProperty = CoverStateProperty.builder().resultCallback(r -> resultCallback.accept(r.getCoverState().getValue())).build();
        server.getUnobservablePropertyService().readProperty(coverStateProperty);
    }
}
