package client;

import client.covercontroller_impl.CoverControllerClient;
import client.hellosilaserver_impl.HelloSiLAClient;
import client.timer_impl.TimerClient;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.standard_features.core.silaservice.SILAServiceClient;
import sila_java.library.core.encryption.SelfSignedCertificate;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A server-initiated client that communicates over the Gateway with the three test servers Hello SiLA Server, Cover Controller Server and Timer Server with randomly repeated calls.
 * The Gateway and these three SiLA servers must be started beforehand.
 */
@Slf4j
public class TestMultipleServerClient {
    final static Random rand = new Random();
    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException, SelfSignedCertificate.CertificateGenerationException {
        long stayAliveSeconds = 1000;
        log.info("Launching cloud client for {} seconds.", stayAliveSeconds);

        TestMultipleServerClient helloClient = new TestMultipleServerClient();

        SILACloudClient cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(helloClient::serverHasConnected)
                .withSelfSignedCertificate()
                .start();

        log.info("client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 1000);
        log.info("# No connectedServers before client ends: {}", cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }

    void serverHasConnected(CloudConnectedServer cloudConnectedServer) {
        SILAServiceClient silaServiceClient = new SILAServiceClient(cloudConnectedServer);
        silaServiceClient.getServerType(serverType->
        {
            switch (serverType) {
                    case "Hello SiLA Server":
                        executeHelloSiLACalls(new HelloSiLAClient(cloudConnectedServer));
                        break;
                    case "Cover Controller Server":
                        executeCoverControllerCalls(new CoverControllerClient(cloudConnectedServer));
                        break;
                    case "Timer Server":
                        executeTimerServer(new TimerClient(cloudConnectedServer));
                        break;
                }
        });
    }

    void executeHelloSiLACalls(HelloSiLAClient helloSiLAClient) {
        repeatRandom(10, 20, 50, () -> helloSiLAClient.getYear(r -> {}));
        repeatRandom(10, 20, 50, () -> helloSiLAClient.greeting("Bernd", r -> {}));
    }

    void executeCoverControllerCalls(CoverControllerClient coverControllerClient) {
        repeatRandom(10, 20, 50, () -> coverControllerClient.openCover(r -> {}));
        repeatRandom(10, 20, 50, () -> coverControllerClient.closeCover(r -> {}));
        repeatRandom(5, 10, 50, () -> coverControllerClient.getCoverState(r -> {}));
    }

    void executeTimerServer(TimerClient timerClient) {
        timerClient.startTimer("My timer", (r -> {}));
        repeatRandom(5, 10, 50, () -> timerClient.getTimerValue(r -> {}));
        repeatRandom(10, 30, 50, () ->
        {
            timerClient.resetTimer(r -> {});
            timerClient.startTimer("My timer", (r -> {}));
        });
    }

    static void repeatRandom(long min, long max, int count, Runnable r) {
        Timer timer = new Timer();
        repeatRandom(timer, min*1000, max*1000, count, r);
    }

    private static void repeatRandom(Timer timer, long min, long max, int count, Runnable r) {
        if(count < 1) {
            timer.cancel();
            return;
        }
        long delay = (long)(rand.nextDouble() * (max - min)) + min;
        timer.schedule(new TimerTask() {
            public void run() {
            r.run();
            repeatRandom(timer, min, max, count - 1, r);
            }
        }, delay);
    }
}
