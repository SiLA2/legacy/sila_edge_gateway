package client;

import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.standard_features.core.silaservice.SILAServiceClient;
import sila_java.library.core.encryption.SelfSignedCertificate;

import java.io.IOException;


/**
 * A server-initiated client which uses the SiLA Service.
 * The goal is to show that the gateway can handle the used message types specified in the SiLA Service.
 */
@Slf4j
public class LogServerInformationClient {
    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException, SelfSignedCertificate.CertificateGenerationException {
        long stayAliveSeconds = 1000;
        log.info("Launching cloud client for {} seconds.", stayAliveSeconds);

        LogServerInformationClient logServerInformationClient = new LogServerInformationClient();

        SILACloudClient cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(logServerInformationClient::serverHasConnected)
                .withSelfSignedCertificate()
                .start();

        log.info("Client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 1000);
        log.info("# No connectedServers before client ends: {}", cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }

    void serverHasConnected(CloudConnectedServer cloudConnectedServer) {
        SILAServiceClient silaServiceClient = new SILAServiceClient(cloudConnectedServer);
        log.info("Server has connected. Client can send commands to it now");
        log.info(cloudConnectedServer.toString());

        // Test SiLAService
        silaServiceClient.getServerName(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerType(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerUUID(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerDescription(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerVendorURL(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerVersion(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getImplementedFeatures(response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.setServerName("New Server Name", response -> log.info("Got serverNameResponse: {}", response));
        silaServiceClient.getServerName(response -> log.info("Got serverNameResponse: {}", response));
    }
}
