package client;

import client.covercontroller_impl.CoverControllerClient;
import client.hellosilaserver_impl.HelloSiLAClient;
import client.timer_impl.TimerClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.standard_features.core.silaservice.SILAServiceClient;
import sila_java.library.core.encryption.SelfSignedCertificate;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A server-initiated client that issues repeated calls to the gateway after opening the streams for the test servers.
 * It records how many RPCs have been completed after a certain time.
 */
@Slf4j
public class LoadTestClient {
    private static final long DURATION_SECONDS = 60;
    private static final AtomicLong rpcCount = new AtomicLong();
    private final static Semaphore limiter = new Semaphore(300);
    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException, SelfSignedCertificate.CertificateGenerationException {
        long stayAliveSeconds = 1000;
        log.info("Launching cloud client for {} seconds.", stayAliveSeconds);

        LoadTestClient clientToTestMultipleServers = new LoadTestClient();

        SILACloudClient cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(clientToTestMultipleServers::serverHasConnected)
                .withSelfSignedCertificate()
                .withPlainText()
                .start();

        log.info("client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 1000);
        log.info("# No connectedServers before client ends: {}", cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }

    void serverHasConnected(CloudConnectedServer cloudConnectedServer) {
        log.info("Gateway connected to ClientToTestMultipleServers");

        SILAServiceClient silaServiceClient = new SILAServiceClient(cloudConnectedServer);
        silaServiceClient.getServerType(serverType->
        {
            switch (serverType) {
                case "Hello SiLA Server":
                    executeHelloSiLACalls(new HelloSiLAClient(cloudConnectedServer));
                    break;
                case "Cover Controller Server":
                    executeCoverControllerCalls(new CoverControllerClient(cloudConnectedServer));
                    break;
                case "Timer Server":
                    executeTimerServer(new TimerClient(cloudConnectedServer));
                    break;
            }
        });
    }

    void executeHelloSiLACalls(HelloSiLAClient helloSiLAClient) {
        repeatCall(() -> helloSiLAClient.greeting("Bernd", r -> onResponse()));
    }


    void executeCoverControllerCalls(CoverControllerClient coverControllerClient) {
        repeatCall(() -> coverControllerClient.openCover(r -> onResponse()));
    }

    void executeTimerServer(TimerClient timerClient) {
        repeatCall(() -> timerClient.startTime(r -> onResponse()));
    }

    void onResponse() {
        rpcCount.incrementAndGet();
        limiter.release();
    }

    static void repeatCall(Runnable r) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        AtomicBoolean done = new AtomicBoolean();
        final long DURATION_SECONDS = 30;
        scheduler.schedule(() -> done.set(true), DURATION_SECONDS, TimeUnit.SECONDS);
        Timer timer = new Timer();
        repeatCall(timer, done, r);
        scheduler.shutdown();
    }

    @SneakyThrows
    private static void repeatCall(Timer timer, AtomicBoolean done, Runnable r) {
        if(done.get()) {
            timer.cancel();
            log.info("Did " + rpcCount.get() + " RPCs/" + DURATION_SECONDS +" secondes");
            return;
        }
        limiter.acquire();
        long delay = 0;
        timer.schedule(new TimerTask() {
            public void run() {
                r.run();
                repeatCall(timer, done, r);
            }
        }, delay);
    }
}
